package com.sdpgroup6.helps;

import android.util.Log;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
        System.out.println(2+2);
    }

    static String TAG = "log";
    @Test
    public void blah() throws Exception{
        Calendar currentCalendar, workshopCal;
        currentCalendar = Calendar.getInstance();
        Date date = new Date();
        date.setTime(2*3600001);
        currentCalendar.setTime(date);
        workshopCal = Calendar.getInstance();
        date.setTime(4*3600000);
        workshopCal.setTime(date);
        System.out.println(isLessThanTwoHours(currentCalendar, workshopCal));
    }


    public static Boolean isLessThanTwoHours(Calendar currentCalendar, Calendar workshopTime){
        Calendar temp = currentCalendar;
        temp.add(Calendar.HOUR,2);
        long difference = temp.compareTo(workshopTime);
        System.out.println(difference);
        if(difference < 0){
            System.out.println("current time is earlier than workshop time");
            return true;
        }
        else return false;
    }
}