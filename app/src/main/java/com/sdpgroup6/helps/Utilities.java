package com.sdpgroup6.helps;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.TimeController.TimeSyncHandler;
import com.sdpgroup6.helps.TimeController.TimeTask;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by kiman on 3/09/2016.
 * takes care of small trivial functions
 */
public class Utilities {

    // private Context mContext;
    public final static String TAG = "Log";
    public final static String RESPONSETAG = "RestLog";
    public final static String ERRORTAG = "ErrorLog";
    private final static DateFormat SQL_DATE_SENDING_FROMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private final static DateFormat SQL_RETURNED_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private final static DateFormat INTERNAL_DISPLAY_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private final static DateFormat DOB_FORMAT_CHECK = new SimpleDateFormat("dd/MM/yyyy");
    private final static Calendar CURRENT_TIME = Calendar.getInstance();
    public static final String TIME_NIST_GOV = "time.nist.gov";
    public static final String TIME_NW_NIST_GOV = "time-nw.nist.gov";
    public static final String WWW_NIST_GOV = "www.nist.gov";

    //return null if date is null
    public static String getSQLDateString(Calendar date) {
        if (date == null)
            return null;
        else
            return SQL_DATE_SENDING_FROMAT.format(date.getTime());
    }

    //check if the string is a valid DOB
    //check if the string is a valid DOB
    public static Boolean DOBFormatChecker(String stringDOB) {
        Boolean valid = true;
        Calendar birthday = Calendar.getInstance();
        try {
            birthday.setTime(DOB_FORMAT_CHECK.parse(stringDOB));
        } catch (ParseException e) {
            valid = false;
        }
        //check age
        int age = getAppTime().get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
        if (age > 100 || age < 16) {       //nah, you are too old or too young for Uni
            valid = false;
        }
        return valid;
    }

    //set The App Time, make sure month is between 0 to 11
    public static void setAppTime(int year, int month, int day, int hour, int minutes) {
        CURRENT_TIME.set(year, month, day, hour, minutes);

    }

    public static void setAppTime(Date time) {
        CURRENT_TIME.setTime(time);


    }

    //call this if want a get time from a time server, and  update the currentAppTime.
    // Used to prevent student fake their time
    public static void initiateSyncTimeTask(@Nullable TimeSyncHandler handler) {
        new TimeTask(handler).execute(TIME_NIST_GOV, TIME_NW_NIST_GOV, WWW_NIST_GOV);//add more servers
    }

    //call this to get Application's Time,
    //note in real application, simply return CURRENT_TIME instead of mocktime.
    public static Calendar getAppTime() {
        Date temp = new Date();
        temp.setTime((long) (CURRENT_TIME.getTimeInMillis() - 3 * (3600 * 24 * 365.25 * 1000)));
        Calendar mockCalender = Calendar.getInstance();
        mockCalender.setTime(temp);
        //testing start
        // mockCalender.set(Calendar.YEAR,2013);
        //  mockCalender.set(Calendar.MONTH,3);
        // mockCalender.set(Calendar.DAY_OF_MONTH,25);
        //testing end
        Log.i(RESPONSETAG, "getAppTime: " + INTERNAL_DISPLAY_FORMAT.format(mockCalender.getTime()));
        return mockCalender;
    }

    //for waiting time Updates
    public static void getAppTime(TimeSyncHandler handler) {
        initiateSyncTimeTask(handler);
    }

    //return a String that contains current App Time in the format specified at INTERNAL_DISPLAY_FORMAT
    public static String getAppTimeString() {
        return INTERNAL_DISPLAY_FORMAT.format(getAppTime().getTime());
    }

    public static String getDOBString(Calendar DOB) throws NullPointerException {
        return DOB_FORMAT_CHECK.format(DOB.getTime());
    }

    //return a Date that can be used by Calander.initiateSyncTimeTask(), make sure the format is correct,
    public static Date getDatefromString(String date) throws ParseException {
        return SQL_RETURNED_FORMAT.parse(date);

    }

    public static Calendar getCalendarByString(String date) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(DOB_FORMAT_CHECK.parse(date));
        } catch (ParseException e) {
            return null;
        }
        return calendar;
    }

    /**
     * Display a toast on the screen with the message
     *
     * @param context
     * @param msg
     */

    public static void displayToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void displayError(Context context, String errorMsg) {
        Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();
    }

    public static void setStudentIdOnNav(Context context, View header, String emailSuffix) {
        TextView studentIdTV = (TextView) header.findViewById(R.id.nav_header_student_id_textview);
        TextView studentEmailTV = (TextView) header.findViewById(R.id.nav_header_student_email_textView);
        String id = AppPreferences.getInstance(context).getStudentId();
        if (id != null) {
            studentIdTV.setText(id);
            studentEmailTV.setText(id + emailSuffix);
        }
    }


    //this method returns the time of calendar object in a HH:MM:SS format
    public static String getAppTime(Calendar calendar) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        String hourStr = String.format(Locale.UK, "%02d", hour);
        String minStr = String.format(Locale.UK, "%02d", minute);
        String secStr = String.format(Locale.UK, "%02d", seconds);
        return hourStr + ":" + minStr + ":" + secStr;
    }

    //format time based on day, month and year
    public static String getFormattedDate(int day, int month, int year) {
        String yearStr = String.format(Locale.UK, "%04d", year);
        String monthStr = String.format(Locale.UK, "%02d", month);
        String dayStr = String.format(Locale.UK, "%02d", day);
        return dayStr + "/" + monthStr + "/" + yearStr;
    }

    ////this method returns the Date of calendar object in YYYY/MM/DD format
    public static String getFormattedDate(Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String yearStr = String.format(Locale.UK, "%04d", year);
        String monthStr = String.format(Locale.UK, "%02d", month);
        String dayStr = String.format(Locale.UK, "%02d", day);
        return dayStr + "/" + monthStr + "/" + yearStr;
    }

    /**
     * return true if the current time is less than 2 hour before the workshopTime
     */
    public static Boolean isLessThanTwoHours(Calendar workshopTime) {
        Calendar temp = getAppTime();
        temp.add(Calendar.HOUR, 2);
        long difference = temp.compareTo(workshopTime);
        System.out.println(difference);
        if (difference < 0) {
            return false;
        } else return true;
    }

    /**
     * returns the difference between two dates.
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

}
