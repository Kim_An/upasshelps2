package com.sdpgroup6.helps.Controller;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

/**
 * Takes care of the content within a card of program workshop,
 * no interaction involved because this is only for viewing purpose
 */
public class ProgramWorkshopAdapter extends RecyclerView.Adapter<ProgramWorkshopAdapter.recyclerViewHolder> {

    private List<Workshop> mWorkshops;

    public ProgramWorkshopAdapter(List<Workshop> workshops) {
        mWorkshops = workshops;
    }

    /**
     * Inflate the adapter view and return a view holder with the layout
     */
    @Override
    public ProgramWorkshopAdapter.recyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_single_workshop, parent, false);
        return new recyclerViewHolder(v, mWorkshops);
    }

    /**
     * set text in the view holder
     */
    @Override
    public void onBindViewHolder(ProgramWorkshopAdapter.recyclerViewHolder holder, int position) { //Change from JH
        Workshop workshop = mWorkshops.get(position);
        Calendar workshopCal = null;
        holder.mWorkshopTitle.setText(workshop.getTopic());

        try {
            workshopCal = workshop.getStartDate();
            holder.mWorkshopDate.setText(Utilities.getFormattedDate(workshopCal));
            holder.mWorkshopTimeTv.setText(Utilities.getAppTime(workshopCal));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.mWorkshopDate.setText(Constants.GENERIC_ERROR);
        }
        holder.mWorkshopLocationTv.setText(workshop.getCampus());
    }

    @Override
    public int getItemCount() {
        return mWorkshops.size();
    }

    /**
     * recyclerview viewholder class
     * linked to the adapter and attach java object with the xml view
     */
    public class recyclerViewHolder extends RecyclerView.ViewHolder {

        private List<Workshop> mWorkshopList;
        private TextView mWorkshopTitle;
        private TextView mWorkshopDate;
        private TextView mWorkshopTimeTv;
        private TextView mWorkshopLocationTv;
        private Button mBookBtn;

        public recyclerViewHolder(View itemView, List<Workshop> WorkshopList) {
            super(itemView);
            this.mWorkshopList = WorkshopList;
            mWorkshopTitle = (TextView) itemView.findViewById(R.id.adapter_single_workshop_title_textview);
            mWorkshopDate = (TextView) itemView.findViewById(R.id.adapter_single_workshop_start_date_textview);
            mWorkshopTimeTv = (TextView) itemView.findViewById(R.id.adapter_single_workshop_start_time_textview);
            mWorkshopLocationTv = (TextView) itemView.findViewById(R.id.adapter_single_workshop_location_textview);
            mBookBtn = (Button) itemView.findViewById(R.id.adapter_single_workshop_book_button);
            mBookBtn.setVisibility(View.GONE);
        }
    }


}
