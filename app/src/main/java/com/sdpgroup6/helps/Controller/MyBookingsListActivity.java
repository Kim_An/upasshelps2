package com.sdpgroup6.helps.Controller;

import android.Manifest;
import android.accounts.NetworkErrorException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.sdpgroup6.helps.CalendarHelper.CalendarChooser;
import com.sdpgroup6.helps.CalendarHelper.CalendarDialogHandler;
import com.sdpgroup6.helps.CalendarHelper.CalendarEventReminder;
import com.sdpgroup6.helps.CalendarHelper.EventStorage;
import com.sdpgroup6.helps.CalendarHelper.ReminderDialogHandler;
import com.sdpgroup6.helps.CalendarHelper.ReminderHandler;
import com.sdpgroup6.helps.CalendarHelper.UpcomingWorkshopReminderChooser;
import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.BookedWorkshop;
import com.sdpgroup6.helps.Model.BookedWorkshopResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.SessionBooking;
import com.sdpgroup6.helps.Model.SessionBookingResponse;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;
import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Is the class for booking list activity
 * takes care of canceling workshop, making reminders, and
 * sets up the recyclerview for upcoming bookings, past bookings, and past sessions
 */
public class MyBookingsListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        RestResponseHandler, View.OnClickListener, ReminderHandler, CalendarDialogHandler, ReminderDialogHandler {

    private ProgressBar mProgressBar;
    private Toolbar mToolbar;
    private List<BookedWorkshop> mPastWorkshops = new ArrayList<>();
    private List<BookedWorkshop> mUpcomingWorkshops = new ArrayList<>();
    private List<SessionBooking> mSessionBooking = new ArrayList<>();
    private DrawerLayout mDrawer;
    private FloatingActionButton mFab;
    private long mCalendarId;
    private String mBookingId;
    private Workshop mBookedWorkshop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_my_bookings);

        mToolbar = (Toolbar) findViewById(R.id.activity_my_bookings_list_toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.nav_my_bookings_drawer_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.activity_my_bookings_list_progressbar);
        mFab = (FloatingActionButton) findViewById(R.id.activity_my_bookings_fab);
        ScrollView rootView = (ScrollView) findViewById(R.id.activity_my_bookings_list_root_view);

        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);
        setSupportActionBar(mToolbar);
        mProgressBar.setVisibility(View.VISIBLE);
        mFab.setOnClickListener(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setUpNavigationDrawer();

    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    @Override
    public void onRestResponse(RequestResponse requestResponse) {
        if(requestResponse instanceof BookedWorkshopResponse){
            setUpBookedworkshopRecyclerViews((BookedWorkshopResponse) requestResponse);
        }else if(requestResponse instanceof SessionBookingResponse){
            setUpSessionBookingRecyclerViews((SessionBookingResponse) requestResponse);
        }
    }

    @Override
    public void onResponseFailed(String errorMsg) {
        Utilities.displayError(this, errorMsg);
        mProgressBar.setVisibility(View.GONE);
    }

    private void setUpBookedworkshopRecyclerViews(BookedWorkshopResponse response){
        mPastWorkshops = response.getPastWorkshops();
        Log.i(Utilities.RESPONSETAG, "Got " + mPastWorkshops.size() + " Past workshops");

        mUpcomingWorkshops = response.getFutureWorkshops();
        Log.i(Utilities.RESPONSETAG, "Got " + mUpcomingWorkshops.size() + " Future workshops");

        mProgressBar.setVisibility(View.GONE);

        setUpUpcomingRecyclerViews(mUpcomingWorkshops);
        setUpPastBookingRecyclerViews(mPastWorkshops);
    }

    private void setUpSessionBookingRecyclerViews(SessionBookingResponse response){
        mSessionBooking = response.getResults();
        Log.i(Utilities.RESPONSETAG, "Got " + mSessionBooking.size() + " session bookings");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_my_bookings_list_session_booking_recyclerview);
        SessionAdapter adapter = new SessionAdapter(this, this, MyBookingsListActivity.this, mSessionBooking);

        if(recyclerView!=null){
            recyclerView.setLayoutManager(new LinearLayoutManager(MyBookingsListActivity.this));
            recyclerView.setAdapter(adapter);
        }
    }

    private void setUpUpcomingRecyclerViews(List<BookedWorkshop> workshops) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_my_bookings_list_upcoming_workshop_recyclerview);
        UpcomingBookingsAdapter bookingsAdapter = new UpcomingBookingsAdapter(this, this, getApplicationContext(), workshops);

        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(MyBookingsListActivity.this));
            recyclerView.setAdapter(bookingsAdapter);
        }
    }

    private void setUpPastBookingRecyclerViews(List<BookedWorkshop> workshops){
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_my_bookings_list_past_workshop_recyclerview);
        PastBookingsAdapter pastBookingsAdapter = new PastBookingsAdapter(this, workshops);

        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(MyBookingsListActivity.this));
            recyclerView.setAdapter(pastBookingsAdapter);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent = ActivityManager.getSwitchActivities(this, item.getItemId());

        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }

        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }

    /**
     * set up navigation drawer
     * set student id on the nav header, and the profile picture
     */
    private void setUpNavigationDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_my_bookings_nav_view);
        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            ImageView profilePicture = (ImageView) header.findViewById(R.id.nav_header_profile_picture_imageview);

            Utilities.setStudentIdOnNav(getApplicationContext(), header, getString(R.string.student_email_suffix));
            navigationView.setCheckedItem(R.id.nav_my_booking_item);
            navigationView.setNavigationItemSelectedListener(this);
            profilePicture.setOnClickListener(this);
        }
    }

    @Override
    protected void onResume() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_my_bookings_nav_view);
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();

        if (navigationView != null) {
            navigationView.setCheckedItem(R.id.nav_my_booking_item);
        }

        startDate.set(2010, 0, 1);
        endDate.set(2015, 11, 31);

        getWorkshopBookings(startDate, endDate);
        getSessionBookings(startDate, endDate);
        super.onResume();
    }

    /**
     * send a request to get workshop bookings
     * @param startDate is the start of the range to search for
     * @param endDate is the start of the range to search for
     */
    private void getWorkshopBookings(Calendar startDate, Calendar endDate){
        try {
            MobileRESTClient.getInstance(this).getWorkshopController().getWorkshopBooking(startDate, endDate, null, null, null,
                    true, null, 100, this);
        } catch (NetworkErrorException e) {
            e.printStackTrace();

            Utilities.displayError(this, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    /**
     * send a request to get session bookings
     * @param startDate is the start of the range to search for
     * @param endDate is the start of the range to search for
     */
    private void getSessionBookings(Calendar startDate, Calendar endDate){
        try {
            MobileRESTClient.getInstance(this).getmSessionController().getSessionBooking(startDate, endDate, null, null, null,
                    null, null, true, null, null, this);
        } catch (NetworkErrorException e) {
            e.printStackTrace();

            Utilities.displayError(this, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onPause() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.nav_header_profile_picture_imageview) {
            startActivity(ActivityManager.intentToRegisterActivity(this));
        } else if (id == R.id.activity_my_bookings_fab) {
            startActivity(ActivityManager.intentToTabbedMakeBookingActivity(MyBookingsListActivity.this));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.READ_CALENDAR_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.displayToast(this, getString(R.string.permission_granted));
                    showCalendarEventDialog();
                } else {
                    Utilities.displayToast(this, getString(R.string.permission_denied));
                }
                break;
            }
        }
    }

    @Override
    public void onRemindClicked(BookedWorkshop bookedWorkshop) {
        mBookingId = String.valueOf(bookedWorkshop.getBookingId());
        mBookedWorkshop = bookedWorkshop.getWorkshop();

        //checks for read calendar permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Constants.READ_CALENDAR_PERMISSION}, Constants.READ_CALENDAR_PERMISSION_REQUEST);
        } else {
            showCalendarEventDialog();
        }
    }

    private void showCalendarEventDialog() {
        CalendarChooser calendarChooser = new CalendarChooser(this, this, getContentResolver(), this);
        calendarChooser.show();
    }

    @Override
    public void onCalendarDialogResponse(boolean positive, long calId) {
        mCalendarId = calId;

        UpcomingWorkshopReminderChooser reminderChooser = new UpcomingWorkshopReminderChooser(this, this, getContentResolver(), this);
        reminderChooser.show();
    }

    @Override
    public void onReminderChosen(boolean[] selectedReminder, int arrayResId) {
        try {
            //checks for existing event in the calendar using app preferences
            if (EventStorage.getInstance(this).getEventId(mBookingId) != -1) {
                removeEvent(mBookingId);
            }
            makeEvent(mCalendarId, selectedReminder, arrayResId);
        } catch (ParseException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }

    /**
     * makes event and fill with topic/title, description, location and time of the event i.e. workshop
     * @param calId is the id of the calendar that user wants to use
     * @param selectedReminders is the sequence of the reminder that user wanted
     * @param arrayResId is the reminder array that is shown
     * @throws ParseException
     */
    private void makeEvent(long calId, boolean[] selectedReminders, int arrayResId) throws ParseException {
        CalendarEventReminder event = new CalendarEventReminder(this, this, getContentResolver(), mBookingId);

        String title = mBookedWorkshop.getTopic();
        String description = mBookedWorkshop.getDescription();
        String location = mBookedWorkshop.getCampus();
        Calendar beginTime = mBookedWorkshop.getStartDate();
        Calendar endTime = mBookedWorkshop.getEndDate();

        event.makeEvent(calId, title, description, location, beginTime, endTime);
        event.insertEventAndReminders(selectedReminders, arrayResId);
    }

    /**
     * removes event from the app preference and from the user's calendar
     * @param workshopBookingId
     */
    public void removeEvent(String workshopBookingId) {
        long eventId = -1;

        if (workshopBookingId != null) {
            eventId = EventStorage.getInstance(getApplicationContext()).getEventId(workshopBookingId);
        }

        //checks if event is existed
        // -1 means no
        if (eventId != -1) {
            CalendarEventReminder event = new CalendarEventReminder(getContentResolver(), this, this);
            event.deleteEventWithoutToast(eventId, workshopBookingId);
        } else {
            Utilities.displayError(this, Constants.NO_REMINDER_RELATED_TO_THIS_BOOKING);
        }
    }
}
