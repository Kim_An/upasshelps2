package com.sdpgroup6.helps.Controller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.Program;
import com.sdpgroup6.helps.R;

import java.util.List;

/**
 * Takes care of the content within a card of program,
 * clicking on layout will go to detail page
 */
public class TabbedProgramAdapter extends RecyclerView.Adapter<TabbedProgramAdapter.TabbedProgramViewHolder>{

    private Context mContext;
    private List<Program> mProgramList;

    public TabbedProgramAdapter(Context mContext, List<Program> mProgramList) {
        this.mContext = mContext;
        this.mProgramList = mProgramList;
    }

    @Override
    public TabbedProgramViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_single_program, parent, false);
        return new TabbedProgramViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(TabbedProgramViewHolder holder, int position) {
        Program program = mProgramList.get(position);

        holder.mProgramTv.setText(program.getName());
    }

    @Override
    public int getItemCount() {
        return mProgramList.size();
    }

    /**
     * references view from xml using the itemView
     */
    public class TabbedProgramViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private LinearLayout mProgramLayout;
        private TextView mProgramTv;

        public TabbedProgramViewHolder(View itemView) {
            super(itemView);
            mProgramLayout = (LinearLayout) itemView.findViewById(R.id.adapter_program_name_textview_layout);
            mProgramTv = (TextView) itemView.findViewById(R.id.adapter_program_name_textview);
            mProgramLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int selectedPos = getAdapterPosition();
            Program program = mProgramList.get(selectedPos);

            Intent intent = new Intent(mContext, TabbedProgramDetailActivity.class);
            intent.putExtra(Constants.PROGRAM_ID, String.valueOf(program.getId()));
            intent.putExtra(Constants.PROGRAM_NAME, String.valueOf(program.getName()));

            Log.i(Constants.LOG_TAG, "Program ID: " + String.valueOf(program.getId()) + "\n"
                    + "Program name: " + program.getName());

            mContext.startActivity(intent);
        }
    }


}
