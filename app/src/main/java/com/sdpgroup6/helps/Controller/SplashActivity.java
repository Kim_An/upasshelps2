package com.sdpgroup6.helps.Controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.Constants;

/**
 * decides which activity to go while the app is loading
 */
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String id = AppPreferences.getInstance(getApplicationContext()).getStudentId();
        if (!id.equals(Constants.NULL)) {
            startActivity(ActivityManager.login(getApplicationContext(), this, id));
        } else {
            startActivity(ActivityManager.intentToLoginActivity(this));
        }
        finish();
    }

}
