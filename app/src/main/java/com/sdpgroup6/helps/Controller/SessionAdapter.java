package com.sdpgroup6.helps.Controller;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sdpgroup6.helps.CalendarHelper.ReminderHandler;
import com.sdpgroup6.helps.Model.SessionBooking;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

/**
 * Takes care of the content within a card of session,
 * no interaction involved because session that is passed can't be cancelled or set reminder
 */
public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.recyclerViewHolder> {

    List<SessionBooking> mBookedSession;
    Context mContext;
    ReminderHandler mHandler;
    Activity mActivity;

    public SessionAdapter(Activity activity, ReminderHandler handler, Context context, List<SessionBooking> bookedSession) {
        mActivity = activity;
        mHandler = handler;
        mContext = context;
        mBookedSession = bookedSession;
    }

    /**
     * Inflate the adapter view and return a view holder with the layout
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public SessionAdapter.recyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_bookings, parent, false);

        return new SessionAdapter.recyclerViewHolder(v);
    }

    /**
     * set text in the view holder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(SessionAdapter.recyclerViewHolder holder, int position) {
        SessionBooking bookedSession = mBookedSession.get(position);
        String attended = String.valueOf(bookedSession.getAttended());

        holder.mWorkshopTitleTv.setText(bookedSession.getAppointmentType());
        holder.mLocationTv.setText(bookedSession.getCampus());
        holder.mLecturerTv.setText(bookedSession.getLecturerFirstName());
        holder.mAttendanceTv.setText(attended);
        Calendar workshopCal = null;

        try {
            workshopCal = bookedSession.getStartDate();
            holder.mWorkshopDateTv.setText(Utilities.getFormattedDate(workshopCal));
            holder.mWorkshopTimeTv.setText(Utilities.getAppTime(workshopCal));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.mWorkshopDateTv.setText("Problem with Parsing the Data!!! Contact your admin for help");
            return;
        }

        if (bookedSession.getSessionType().equals("")) {
            holder.mWorkshopTypeTv.setVisibility(View.GONE);
        } else {
            holder.mWorkshopTypeTv.setText(bookedSession.getSessionType());
        }
    }

    @Override
    public int getItemCount() {
        return mBookedSession.size();
    }

    /**
     * recyclerview viewholder class
     * linked to the adapter and attach java object with the xml view
     */
    public class recyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView mWorkshopTitleTv;
        private TextView mWorkshopDateTv;
        private TextView mWorkshopTimeTv;
        private TextView mLocationTv;
        private Button mCancelBtn;
        private Button mRemindBtn;
        private TextView mWorkshopTypeTv;
        private TextView mLecturerTv;
        private TextView mAttendanceTv;

        public recyclerViewHolder(View itemView) {
            super(itemView);
            mWorkshopTitleTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_title_textview);
            mWorkshopDateTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_date_textview);
            mWorkshopTimeTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_time_textview);
            mLocationTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_location_textview);
            mCancelBtn = (Button) itemView.findViewById(R.id.adapater_my_bookings_cancel_button);
            mRemindBtn = (Button) itemView.findViewById(R.id.adapater_my_bookings_remind_button);
            mWorkshopTypeTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_type_textview);
            mLecturerTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_lecturer_textview);
            mAttendanceTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_attendance_textview);
            mLecturerTv.setVisibility(View.VISIBLE);
            mAttendanceTv.setVisibility(View.VISIBLE);
            mRemindBtn.setVisibility(View.GONE);
            mCancelBtn.setVisibility(View.GONE);
        }

    }


}