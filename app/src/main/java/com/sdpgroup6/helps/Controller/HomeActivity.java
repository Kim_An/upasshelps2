package com.sdpgroup6.helps.Controller;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sdpgroup6.helps.CalendarHelper.CalendarEventReminder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

/**
 * Is the class of home page activity of the application
 */
public class HomeActivity extends AppCompatActivity
        implements View.OnClickListener {

    //References to UI
    private ImageButton mPersonalDetailImgBtn;
    private ImageButton mMyBookingsImgBtn;
    private ImageButton mMakeBookingImgBtn;
    private ImageButton mLogoutImgBtn;
    private ImageButton mFaqBtn;
    private ImageButton mInfoBtn;
    private TextView mHelpsTv;
    private CoordinatorLayout mCoordinatorLayout;

    private static boolean sBackPressedTwice = false;
    private static int sPressTenTimes = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPersonalDetailImgBtn = (ImageButton) findViewById(R.id.activity_home_my_detials_imgbtn);
        mMyBookingsImgBtn = (ImageButton) findViewById(R.id.activity_home_my_bookings_imgbtn);
        mMakeBookingImgBtn = (ImageButton) findViewById(R.id.activity_home_new_boking_imgbtn);
        mLogoutImgBtn = (ImageButton) findViewById(R.id.activity_home_log_out_imgbtn);
        mFaqBtn = (ImageButton) findViewById(R.id.activity_home_faq_imgbutton);
        mInfoBtn = (ImageButton) findViewById(R.id.activity_home_program_info_imgbtn);
        mHelpsTv = (TextView) findViewById(R.id.activity_home_uts_helps_textview);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_home_coordinator_layout);

        mPersonalDetailImgBtn.setOnClickListener(this);
        mMyBookingsImgBtn.setOnClickListener(this);
        mMakeBookingImgBtn.setOnClickListener(this);
        mLogoutImgBtn.setOnClickListener(this);
        mFaqBtn.setOnClickListener(this);
        mInfoBtn.setOnClickListener(this);
        mHelpsTv.setOnClickListener(this);

        RelativeLayout rootView = (RelativeLayout) findViewById(R.id.activity_home_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);
        //set global app time
        //Utilities.setAppTime(2013, 0, 8, 8, 0);
        //or update with a Task
        Utilities.initiateSyncTimeTask(null);

        Log.i(Utilities.RESPONSETAG, "Current App Time: " + Utilities.getAppTimeString());
        LoginSession.studentId = AppPreferences.getInstance(getApplicationContext()).getStudentId();

    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_home_drawer_layout);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                exitOnBackPressedTwice();
            }
        }
    }

    /**
     * To keep device in portrait mode only
     *
     * @param newConfig
     */
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * onClick functions for the home screen tile options
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.activity_home_my_detials_imgbtn: {
                Intent intent = new Intent(HomeActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.activity_home_my_bookings_imgbtn: {
                //asking for read calendar permission
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Constants.READ_CALENDAR_PERMISSION}, Constants.READ_CALENDAR_PERMISSION_REQUEST);
                } else {
                    startActivity(ActivityManager.intentToMyBookingListActivity(HomeActivity.this));
                }
                break;
            }
            case R.id.activity_home_new_boking_imgbtn: {
                startActivity(ActivityManager.intentToTabbedMakeBookingActivity(HomeActivity.this));
                break;
            }
            case R.id.activity_home_log_out_imgbtn: {
                startActivity(ActivityManager.logout(HomeActivity.this));
                break;
            }
            case R.id.activity_home_faq_imgbutton: {
                startActivity(ActivityManager.intentToFaqActivity(this));
                break;
            }
            case R.id.activity_home_program_info_imgbtn: {
                startActivity(ActivityManager.intentToProgramInfoActivity(this));
                break;
            }
            case R.id.activity_home_uts_helps_textview: {
                showEasterEgg();
            }
        }
    }

    private void exitOnBackPressedTwice() {
        int delayDuration = 2000; //2 seconds

        if (sBackPressedTwice) {
            Intent intent = ActivityManager.exitAppIntent();
            startActivity(intent);
        } else {
            sBackPressedTwice = true;
            Utilities.displayToast(HomeActivity.this, getString(R.string.press_back_again_to_exit));
            //wait for delayDuration time until execute the code inside
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sBackPressedTwice = false;
                }
            }, delayDuration);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.READ_CALENDAR_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.displayToast(this, getString(R.string.permission_granted));
                    startActivity(ActivityManager.intentToMyBookingListActivity(HomeActivity.this));
                } else {
                    Utilities.displayToast(this, getString(R.string.permission_denied));
                }
                break;
            }

        }
    }

    private void showEasterEgg() {
        int delayDuration = 4000; //2 seconds
        if (sPressTenTimes == 10) {
            Snackbar snackbar = Snackbar
                    .make(mCoordinatorLayout, Constants.THE_ONLY_HELP_IS_YOURSELF, Snackbar.LENGTH_LONG)
                    .setAction(Constants.THANKS, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Snackbar anotherSnackbar = Snackbar
                                    .make(mCoordinatorLayout, Constants.IT_HELPS, Snackbar.LENGTH_LONG);
                            anotherSnackbar.show();
                        }
                    });
            snackbar.show();
            sPressTenTimes = 1;
        } else {
            sPressTenTimes += 1;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sPressTenTimes = 1;
                }
            }, delayDuration);
        }
    }
}
