package com.sdpgroup6.helps.Controller;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdpgroup6.helps.CalendarHelper.PermissionHandler;
import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.WaitingCountResponse;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.Model.WorkshopDuplicate;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.view.View.GONE;

/**
 * Takes care of the content within a card of workshop, including buttons and views visibility
 */
public class SingleWorkshopAdapter extends RecyclerView.Adapter<SingleWorkshopAdapter.MyRecyclerViewHolder> {

    private Context mContext;
    private Activity mActivity;
    private PermissionHandler mPermissionHandler;
    private Workshop mClickedWorkshop = null;
    private int mSelectedPos;
    private List<WorkshopDuplicate> mWorkshopDuplicates = new ArrayList<>();

    public SingleWorkshopAdapter(PermissionHandler mPermissionHandler, Activity activity, Context context, List<Workshop> workshops) {
        mContext = context;
        for (Workshop workshop : workshops) {
            mWorkshopDuplicates.add(new WorkshopDuplicate(workshop));
        }
        mActivity = activity;
        this.mPermissionHandler = mPermissionHandler;
    }

    /**
     * Inflate the adapter view and return a view holder with the layout
     */
    @Override
    public MyRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_single_workshop, parent, false);
        return new MyRecyclerViewHolder(v);

    }

    /**
     * set text in the view holder
     */
    @Override
    public void onBindViewHolder(MyRecyclerViewHolder holder, int position) {
        Workshop workshop = mWorkshopDuplicates.get(position).getmWorkshop();
        Calendar workshopCal = null;

        holder.mWorkshopLocationTv.setText(workshop.getCampus());
        holder.mWaitlistTv.setText(String.valueOf(mWorkshopDuplicates.get(position).getWaitingCount()));
        holder.mWorkshopTitle.setText(workshop.getTopic());

        try {
            workshopCal = workshop.getStartDate();
            holder.mWorkshopDate.setText(Utilities.getFormattedDate(workshopCal));
            holder.mWorkshopTimeTv.setText(Utilities.getAppTime(workshopCal));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.mWorkshopDate.setText(Constants.GENERIC_ERROR);
        }

        //shows wait listing if the workshop is full (isClicked)
        if (mWorkshopDuplicates.get(position).isClicked()) {
            showWaiting(holder);
        } else {
            holder.mBookBtn.setVisibility(View.VISIBLE);
            holder.mWaitlistBtn.setVisibility(GONE);
            holder.mWaitlistLayout.setVisibility(GONE);
        }


        //checks if the workshop has waiting count, in case that workshop has requested for its waiting count already

        if (mWorkshopDuplicates.get(position).hasWaitingCount()) {
            showWaiting(holder);
        } else {
            //check if the workshop is full, i.e. the workshop has not been requested for its waiting count yet
            //then if it is, request for the waiting count
            if (mWorkshopDuplicates.get(position).isFull()) {
                setWaitListBeforeClick(holder, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mWorkshopDuplicates.size();
    }

    private void showWaiting(MyRecyclerViewHolder holder) {
        holder.mBookBtn.setVisibility(GONE);
        holder.mWaitlistLayout.setVisibility(View.VISIBLE);
        holder.mWaitlistBtn.setVisibility(View.VISIBLE);
    }

    private void setWaitListBeforeClick(MyRecyclerViewHolder holder, int position) {
        holder.mBookBtn.setVisibility(GONE);
        holder.mWaitlistBtn.setVisibility(View.VISIBLE);
        holder.mWaitlistLayout.setVisibility(View.VISIBLE);
        mSelectedPos = position;
        getAndSetWaitlistCount(mWorkshopDuplicates.get(position).getmWorkshop());
    }

    /**
     * recyclerview viewholder class
     * linked to the adapter and attach java object with the xml view
     */
    public class MyRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, RestResponseHandler {

        private TextView mWorkshopTitle;
        private TextView mWorkshopDate;
        private TextView mWorkshopTimeTv;
        private TextView mWorkshopLocationTv;
        private Button mBookBtn;
        private Button mWaitlistBtn;
        private TextView mWaitlistTv;
        private LinearLayout mWaitlistLayout;

        public MyRecyclerViewHolder(View itemView) {
            super(itemView);
            mWorkshopTitle = (TextView) itemView.findViewById(R.id.adapter_single_workshop_title_textview);
            mWorkshopDate = (TextView) itemView.findViewById(R.id.adapter_single_workshop_start_date_textview);
            mWorkshopTimeTv = (TextView) itemView.findViewById(R.id.adapter_single_workshop_start_time_textview);
            mWorkshopLocationTv = (TextView) itemView.findViewById(R.id.adapter_single_workshop_location_textview);
            mBookBtn = (Button) itemView.findViewById(R.id.adapter_single_workshop_book_button);
            mWaitlistBtn = (Button) itemView.findViewById(R.id.adapter_single_workshop_wailtlist_button);
            mWaitlistTv = (TextView) itemView.findViewById(R.id.adapter_single_workshop_waitlist_textview);
            mWaitlistLayout = (LinearLayout) itemView.findViewById(R.id.adapter_single_workshop_waitlist_layout);
            mBookBtn.setOnClickListener(this);
            mWaitlistBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mSelectedPos = getAdapterPosition();
            int id = v.getId();
            if (id == R.id.adapter_single_workshop_book_button) {
                book();
            } else if (id == R.id.adapter_single_workshop_wailtlist_button) {
                waitlist();
            }
        }

        private void book() {
            try {
                showBookingConfirmationDialog();
            } catch (ParseException e) {
                Utilities.displayError(mContext, e.getMessage());
            }
        }

        private void waitlist() {
            try {
                showWaitlistConfirmationDialog();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        /**
         * send a request for waitlist in a workshop
         */
        private void waitlistWorkshop() {
            try {
                String workshopId = String.valueOf(mWorkshopDuplicates.get(mSelectedPos).getmWorkshop().getWorkshopId());

                MobileRESTClient.getInstance(mContext).getWorkshopController().waitWorkshop(workshopId, true, this);
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            }
        }

        /**
         * send a request to get waiting count for a workshop
         */
        private void getWaitlistCount(Workshop workshop) {
            try {
                MobileRESTClient.getInstance(mContext).getWorkshopController().CountWorkshopWaiting(String.valueOf(workshop.getWorkshopId()), this);
            } catch (NetworkErrorException e) {
                Utilities.displayError(mContext, e.getMessage());
            }
        }

        /**
         * show confirmation dialog with the detail of the workshop clicked
         *
         * @throws ParseException
         */
        private void showWaitlistConfirmationDialog() throws ParseException {
            mClickedWorkshop = mWorkshopDuplicates.get(mSelectedPos).getmWorkshop();
            String information = Constants.DATE + Utilities.getFormattedDate(mClickedWorkshop.getStartDate());
            information += "\n\n" + Constants.TIME + Utilities.getAppTime(mClickedWorkshop.getStartDate());
            information += "\n\n" + Constants.LOCATION + mClickedWorkshop.getCampus();

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.DatePickerTheme);
            builder.setTitle(mClickedWorkshop.getTopic());
            builder.setMessage(information);

            String positiveText = mContext.getString(R.string.waitlist);
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            waitlistWorkshop();
                        }
                    });

            String negativeText = mContext.getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog dialog = builder.create();
            // display dialog
            dialog.show();
        }

        /**
         * show confirmation dialog with the detail of the workshop clicked
         *
         * @throws ParseException
         */
        private void showBookingConfirmationDialog() throws ParseException {
            mClickedWorkshop = mWorkshopDuplicates.get(mSelectedPos).getmWorkshop();
            String information = Constants.DATE + Utilities.getFormattedDate(mClickedWorkshop.getStartDate());
            information += "\n\n" + Constants.TIME + Utilities.getAppTime(mClickedWorkshop.getStartDate());
            information += "\n\n" + Constants.LOCATION + mClickedWorkshop.getCampus();

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.DatePickerTheme);
            builder.setTitle(mClickedWorkshop.getTopic());
            builder.setMessage(information);

            String positiveText = mContext.getString(R.string.book);
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            bookWorkshop(String.valueOf(mClickedWorkshop.getWorkshopId()));
                        }
                    });

            String negativeText = mContext.getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog dialog = builder.create();
            // display dialog
            dialog.show();
        }

        /**
         * send a request to server to book a workshop
         *
         * @param workshopId is the id of the workshop to book
         */
        private void bookWorkshop(String workshopId) {
            try {
                Log.i(Utilities.RESPONSETAG, "Booking A New Workshop, WorkshopID:" +
                        workshopId +
                        " StudentID: " + LoginSession.studentId);

                MobileRESTClient.getInstance(mContext).
                        getWorkshopController().bookWorkshop(workshopId, this);
            } catch (NetworkErrorException e) {
                e.printStackTrace();

                Utilities.displayError(mContext, e.getMessage());
            }
        }

        @Override
        public void onRestResponse(RequestResponse requestResponse) {
            if (requestResponse instanceof BookWorkshopResponse) {
                sendBackToActivityToPromptCalendar(requestResponse);
            } else if (requestResponse instanceof WaitingCountResponse) {
                setWaitingCount(requestResponse);
            } else if (requestResponse != null) {
                displayWaitlistingMessage(requestResponse);
                getWaitlistCount(mWorkshopDuplicates.get(mSelectedPos).getmWorkshop());
            }
        }

        @Override
        public void onResponseFailed(String errorMsg) {
            Log.e(Utilities.ERRORTAG, errorMsg);
            Utilities.displayError(mContext, mContext.getString(R.string.booking_failure) + errorMsg);
            if (errorMsg.equals(Constants.MAXIMUM_WORKSHOP_BOOKINGS_ERROR)) {
                getWaitlistCount(mWorkshopDuplicates.get(mSelectedPos).getmWorkshop());
                mWorkshopDuplicates.get(mSelectedPos).setClicked(true);
                notifyItemChanged(mSelectedPos);
            }
        }

        private void displayWaitlistingMessage(RequestResponse requestResponse) {
            Utilities.displayToast(mContext, Constants.YOU_HAVE_BEEN_WAITLISTED);
        }

        private void sendBackToActivityToPromptCalendar(RequestResponse requestResponse) {
            Utilities.displayToast(mContext, mContext.getString(R.string.booking_success));
            //pass clickedWorkshop for the detail for the calendar event
            if (mClickedWorkshop != null) {
                mPermissionHandler.onBookingSuccess(requestResponse, mClickedWorkshop);
            } else {
                Log.e(Constants.LOG_TAG, "mClickedWorkshop is null");
            }
        }

        private void setWaitingCount(RequestResponse requestResponse) {
            WaitingCountResponse response = (WaitingCountResponse) requestResponse;
            String waitingCount = String.valueOf(response.getResult());
            mWorkshopDuplicates.get(mSelectedPos).setWaitingCount(waitingCount);
            notifyItemChanged(mSelectedPos);
        }
    }

    private void getAndSetWaitlistCount(Workshop workshop) {
        try {
            MobileRESTClient.getInstance(mContext).getWorkshopController().CountWorkshopWaiting(String.valueOf(workshop.getWorkshopId()), new RestResponseHandler() {
                @Override
                public void onRestResponse(RequestResponse requestResponse) {
                    WaitingCountResponse response = (WaitingCountResponse) requestResponse;
                    String waitingCount = String.valueOf(response.getResult());

                    mWorkshopDuplicates.get(mSelectedPos).setWaitingCount(waitingCount);
                    notifyItemChanged(mSelectedPos);
                }

                @Override
                public void onResponseFailed(String errorMsg) {
                    Utilities.displayError(mContext, errorMsg);
                }
            });
        } catch (NetworkErrorException e) {
            Utilities.displayError(mContext, e.getMessage());
        }
    }
}

