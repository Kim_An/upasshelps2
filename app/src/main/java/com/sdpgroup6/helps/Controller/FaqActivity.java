package com.sdpgroup6.helps.Controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

/**
 * Is the class for the activity of the FAQ page
 */
public class FaqActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private DrawerLayout mDrawer;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_faq);

        mToolbar = (Toolbar) findViewById(R.id.activity_faq_toolbar);
        setSupportActionBar(mToolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.nav_faq_drawer_layout);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ScrollView rootView = (ScrollView) findViewById(R.id.activity_faq_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);

        setUpNavigationDrawer();
    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = ActivityManager.getSwitchActivities(this, item.getItemId());

        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }
    /**
     * set up navigation drawer
     * set student id on the nav header, and the profile picture
     */
    private void setUpNavigationDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_faq_nav_view);

        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setCheckedItem(R.id.nav_faq_item);
            View header = navigationView.getHeaderView(0);
            Utilities.setStudentIdOnNav(getApplicationContext(), header, getString(R.string.student_email_suffix));
            ImageView profilePicture = (ImageView) header.findViewById(R.id.nav_header_profile_picture_imageview);
            profilePicture.setOnClickListener(this);
        }
    }

    @Override
    protected void onResume() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_faq_nav_view);

        if (navigationView != null) {
            navigationView.setCheckedItem(R.id.nav_faq_item);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.nav_header_profile_picture_imageview) {
            startActivity(ActivityManager.intentToRegisterActivity(this));
        }
    }

}
