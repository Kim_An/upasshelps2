package com.sdpgroup6.helps.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;

import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.R;

/**
 * Is responsible for making intent for activity to navigate from one activity to another
 * the purpose is so that all the navigation can be tracked
 */
public class ActivityManager {

    //return intent for navigation drawer
    public static Intent getSwitchActivities(Context context, int id){
        switch (id) {
            case R.id.nav_make_booking_item: {
                return intentToTabbedMakeBookingActivity(context);
            }
            case R.id.nav_my_booking_item: {
                return intentToMyBookingListActivity(context);
            }
            case R.id.nav_program_info_item: {
                return intentToProgramInfoActivity(context);
            }
            case R.id.nav_faq_item: {
                return intentToFaqActivity(context);
            }
            case R.id.nav_logout_item: {
                return logout(context);
            }case R.id.nav_contact_staff:{
                toGoEmailIntent(context);
                break;
            }
        }
        return null;
    }

    public static Intent intentToMyBookingListActivity(Context context){
        return new Intent(context, MyBookingsListActivity.class);
    }

    public static Intent logout(Context context) {
        AppPreferences.getInstance(context).removeStudentId();
        return new Intent(context, LoginActivity.class);
    }

    public static Intent intentToLoginActivity(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    public static Intent intentToRegisterActivity(Context context){
        return new Intent(context, RegisterActivity.class);
    }

    public static void setBackgroundImage(Resources res, View view, int drawableId){
        Bitmap bitmap = ImageDecoder.decodeBitmapFromResource(res, drawableId);
        Drawable background = new BitmapDrawable(bitmap);
        view.setBackground(background);
    }

    public static Intent exitAppIntent(){
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return homeIntent;
    }

    public static Intent intentToTabbedMakeBookingActivity(Context context){
        return new Intent(context, TabbedMakeBookingActivity.class);
    }

    private static void toGoEmailIntent(Context context){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                Constants.MAIL_TO, Constants.HELPS_EMAIL, null));
        context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.send_email)));
    }

    public static Intent login(Context appContext, Context context,String id){
        AppPreferences.getInstance(appContext).setupPreferences(id);
        return new Intent(context, HomeActivity.class);
    }

    public static Intent intentToFaqActivity(Context context){
        return new Intent(context, FaqActivity.class);
    }

    public static Intent intentToProgramInfoActivity(Context context){
        return new Intent(context, ProgramInfoActivity.class);
    }
}
