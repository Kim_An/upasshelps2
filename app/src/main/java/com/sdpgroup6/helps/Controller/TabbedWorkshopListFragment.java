package com.sdpgroup6.helps.Controller;

import android.Manifest;
import android.accounts.NetworkErrorException;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.sdpgroup6.helps.CalendarHelper.CalendarChooser;
import com.sdpgroup6.helps.CalendarHelper.CalendarDialogHandler;
import com.sdpgroup6.helps.CalendarHelper.CalendarEventReminder;
import com.sdpgroup6.helps.CalendarHelper.PermissionHandler;
import com.sdpgroup6.helps.CalendarHelper.ReminderChooser;
import com.sdpgroup6.helps.CalendarHelper.ReminderDialogHandler;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.CampusListResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.Model.WorkshopResponse;
import com.sdpgroup6.helps.Model.WorkshopSet;
import com.sdpgroup6.helps.Model.WorkshopSetResponse;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static android.view.View.GONE;

/**
 * takes cares showing workshop list in workshop tab of the make booking activity
 */
public class TabbedWorkshopListFragment extends Fragment implements RestResponseHandler, PermissionHandler, View.OnClickListener,
        DatePickerDialog.OnDateSetListener, CalendarDialogHandler, ReminderDialogHandler, View.OnTouchListener {

    private ProgressBar mProgressBar;
    private RecyclerView mWorkshopRecyclerView;
    private RecyclerView mWorkshopSetRecyclerView;
    private EditText mTopicEt;
    private EditText mDateEt;
    private Spinner mLocationSpinner;
    private Context mContext;
    private Button mSearchBtn;
    private ImageButton mExpandBtn;
    private LinearLayout mSearchFieldLayout;
    private static boolean sExpanded = false;
    private DatePickerDialog mDatePickerDialog;
    private Workshop mBookedWorkshop;
    private String mBookingId;
    private long mCalendarId;
    private TextView mNoResultTv;
    private ImageButton mTopicClearBtn;
    private ImageButton mDateClearBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_make_workshop_booking, container, false);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.fragment_make_workshop_booking_progressbar);
        mWorkshopSetRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_make_workshop_set_booking_recyclerview);
        mWorkshopRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_make_workshop_booking_single_workshop_recyclerview);
        mSearchBtn = (Button) rootView.findViewById(R.id.fragment_make_workshop_detail_search_button);
        mTopicEt = (EditText) rootView.findViewById(R.id.fragment_make_workshop_search_by_topic);
        mDateEt = (EditText) rootView.findViewById(R.id.fragment_make_workshop_search_by_date);
        mLocationSpinner = (Spinner) rootView.findViewById(R.id.fragment_make_workshop_search_by_location);
        mExpandBtn = (ImageButton) rootView.findViewById(R.id.fragment_make_workshop_search_expand_button);
        mSearchFieldLayout = (LinearLayout) rootView.findViewById(R.id.fragment_make_workshop_booking_search_field_layout);
        mNoResultTv = (TextView) rootView.findViewById(R.id.fragment_make_workshop_booking_no_result_textview);
        mDateClearBtn = (ImageButton) rootView.findViewById(R.id.fragment_make_workshop_date_clear_btn);
        mTopicClearBtn = (ImageButton) rootView.findViewById(R.id.fragment_make_workshop_booking_topic_clear_btn);
        mWorkshopRecyclerView.setVisibility(GONE);
        return rootView;
    }

    @Override
    public void onStart() {
        mContext = getContext();

        mSearchBtn.setOnClickListener(this);
        mExpandBtn.setOnClickListener(this);
        mDateEt.setInputType(0x00000000); //hide keyboard, it's an android bug
        mDateEt.setOnTouchListener(this);
        mTopicEt.setOnTouchListener(this);
        mDateClearBtn.setOnClickListener(this);
        mTopicClearBtn.setOnClickListener(this);

        setUpDatePicker();
        AppPreferences.getInstance(mContext.getApplicationContext()).loadStudentId();
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        getWorkshopSet();
    }

    private void setUpLocationSpinnerAdapter(RequestResponse requestResponse) {
        String[] campuses = getCampusList(requestResponse);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.campus_spinner_layout, campuses);

        mLocationSpinner.setAdapter(adapter);
    }

    private void setUpWorkshopSetRecyclerView(List<WorkshopSet> workshopSetList) {
        TabbedWorkshopSetAdapter workshopSetAdapter = null;
        workshopSetAdapter = new TabbedWorkshopSetAdapter(mContext, workshopSetList);

        if (mWorkshopSetRecyclerView != null) {
            mWorkshopSetRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            mWorkshopSetRecyclerView.setAdapter(workshopSetAdapter);
        }
    }

    /**
     * send a request to server to get workshop sets
     */
    private void getWorkshopSet() {
        try {
            mProgressBar.setVisibility(View.VISIBLE);
            MobileRESTClient.getInstance(mContext).getWorkshopController().listWorkshopSet(true, this);
        } catch (NetworkErrorException e) {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
            Utilities.displayError(mContext, e.getMessage());

            e.printStackTrace();

        }
    }

    @Override
    public void onRestResponse(RequestResponse requestResponse) {
        mNoResultTv.setVisibility(GONE);
        mProgressBar.setVisibility(GONE);
        if (requestResponse instanceof WorkshopSetResponse) {
            displayWorkshopSetResult(requestResponse);
        } else if (requestResponse instanceof WorkshopResponse) {
            displaySearchResults(requestResponse);
        } else if (requestResponse instanceof CampusListResponse) {
            setUpLocationSpinnerAdapter(requestResponse);
        }
    }

    public void displayWorkshopSetResult(RequestResponse requestResponse) {
        mWorkshopSetRecyclerView.setVisibility(View.VISIBLE);
        mWorkshopRecyclerView.setVisibility(View.GONE);

        WorkshopSetResponse theResponse = (WorkshopSetResponse) requestResponse;
        List<WorkshopSet> workshopSetList = theResponse.getWorkshops();

        if (workshopSetList != null) {
            setUpWorkshopSetRecyclerView(workshopSetList);
        } else {
            Log.e(Constants.LOG_TAG, "TabbedWorkshopListFragment the response is null");
        }
    }

    @Override
    public void onResponseFailed(String errorMsg) {
        mWorkshopSetRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(GONE);
        Utilities.displayError(mContext, errorMsg);
    }

    private String[] getSearchFields() {
        String searchFields[] = new String[3];
        searchFields[0] = mTopicEt.getText().toString(); //topic
        searchFields[1] = mDateEt.getText().toString(); //date
        searchFields[2] = mLocationSpinner.getSelectedItem().toString(); //campus
        if (searchFields[1].equals("")) {
            searchFields[1] = Utilities.getAppTimeString();
        }
        return searchFields;
    }

    private void search() {
        try {
            mWorkshopSetRecyclerView.setVisibility(View.GONE);
            if (mWorkshopRecyclerView != null) {
                mWorkshopRecyclerView.setVisibility(View.GONE);
            }
            mNoResultTv.setVisibility(GONE);

            hideKeyboard();
            String[] searchInputs = getSearchFields();
            getWorkshopsFromSearch(searchInputs[0], searchInputs[1], searchInputs[2]);
        } catch (Exception e) {
            Utilities.displayError(mContext, e.getMessage());
        }
    }

    /**
     * send a request to the server to get workshop using some criteria
     * @param topic
     * @param date
     * @param location
     */
    private void getWorkshopsFromSearch(@Nullable String topic, @Nullable String date, @Nullable String location) {
        mProgressBar.setVisibility(View.VISIBLE);
        try {
            Calendar startDateBegin = Utilities.getCalendarByString(date);
            Calendar startDateEnd = Calendar.getInstance();
            startDateEnd.add(Calendar.YEAR, 1);
            MobileRESTClient.getInstance(mContext).getWorkshopController().loadWorkshop(null, topic, startDateBegin, startDateEnd, null, null, location, true, null, null, this);
        } catch (NetworkErrorException e) {
            Utilities.displayError(mContext, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    private void setUpWorkshopRecyclerView(List<Workshop> workshopList) {
        if (workshopList != null) {
            mProgressBar.setVisibility(GONE);
            SingleWorkshopAdapter singleWorkshopAdapter = new SingleWorkshopAdapter(this, getActivity(), mContext, workshopList);
            assert mWorkshopRecyclerView != null;
            mWorkshopRecyclerView.setVisibility(View.VISIBLE);
            mWorkshopRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            mWorkshopRecyclerView.setAdapter(singleWorkshopAdapter);
        }
    }

    private void displaySearchResults(RequestResponse requestResponse) {
        toggleSearchLayout();
        mWorkshopSetRecyclerView.setVisibility(GONE);
        mWorkshopRecyclerView.setVisibility(View.VISIBLE);
        WorkshopResponse workshopResponse = (WorkshopResponse) requestResponse;
        if (workshopResponse != null) {
            List<Workshop> workshops = workshopResponse.getWorkShops();
            if (workshops.size() == 0) {
                mNoResultTv.setVisibility(View.VISIBLE);
                mWorkshopRecyclerView.setVisibility(View.GONE);
                mWorkshopSetRecyclerView.setVisibility(View.GONE);
            } else {
                setUpWorkshopRecyclerView(workshops);
                mNoResultTv.setVisibility(GONE);
            }
        } else {
            Utilities.displayError(mContext, "Search could not complete");
        }
    }

    @Override
    public void onBookingSuccess(RequestResponse requestResponse, Workshop workshop) {
        BookWorkshopResponse bookWorkshopResponse = (BookWorkshopResponse) requestResponse;
        mBookingId = String.valueOf(bookWorkshopResponse.getBooking().getBookingId());
        mBookedWorkshop = workshop;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Constants.READ_CALENDAR_PERMISSION}, Constants.READ_CALENDAR_PERMISSION_REQUEST);
        } else {
            showCalendarEventDialog();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.fragment_make_workshop_detail_search_button) {
            search();
        } else if (id == R.id.fragment_make_workshop_search_expand_button) {
            toggleSearchLayout();
        }else if(id == R.id.fragment_make_workshop_date_clear_btn){
            clearEditText(mDateEt);
        }else if(id == R.id.fragment_make_workshop_booking_topic_clear_btn){
            clearEditText(mTopicEt);
        }
    }

    private void toggleSearchLayout() {
        if (sExpanded) {
            mSearchFieldLayout.setVisibility(GONE);
            sExpanded = false;
            mExpandBtn.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
        } else {
            loadCampus();
            mSearchFieldLayout.setVisibility(View.VISIBLE);
            sExpanded = true;
            mExpandBtn.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
        }
    }

    private void setUpDatePicker() {
        int startYear = Utilities.getAppTime().get(Calendar.YEAR);
        int starthMonth = Utilities.getAppTime().get(Calendar.MONTH);
        Calendar dayBefore = Utilities.getAppTime();
        dayBefore.add(Calendar.DAY_OF_MONTH, -1);

        mDatePickerDialog = new DatePickerDialog(mContext, R.style.DatePickerTheme, this, startYear, starthMonth, dayBefore.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMinDate(dayBefore.getTimeInMillis());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = Utilities.getFormattedDate(dayOfMonth, month + 1, year);
        mDateEt.setText(date);
    }

    private void showCalendarEventDialog() {
        CalendarChooser calendarChooser = new CalendarChooser(getActivity(), mContext, mContext.getContentResolver(), this);
        calendarChooser.show();
    }

    @Override
    public void onCalendarDialogResponse(boolean positive, long calId) {
        mCalendarId = calId;
        ReminderChooser reminderChooser = new ReminderChooser(getActivity(), mContext, mContext.getContentResolver(), this);
        reminderChooser.show();
    }

    @Override
    public void onReminderChosen(boolean[] selectedReminder, int arrayResId) {
        try {
            makeEvent(mCalendarId, selectedReminder, arrayResId);
        } catch (ParseException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }

    /**
     * make event in user's calendar by passing the following param
     * @param calId calendar id
     * @param selectedReminders reminder sequence
     * @param arrayResId array reminder
     * @throws ParseException
     */
    private void makeEvent(long calId, boolean[] selectedReminders, int arrayResId) throws ParseException {
        CalendarEventReminder event = new CalendarEventReminder(getActivity(), mContext, mContext.getContentResolver(), mBookingId);

        String title = mBookedWorkshop.getTopic();
        String description = mBookedWorkshop.getDescription();
        String location = mBookedWorkshop.getCampus();
        Calendar beginTime = mBookedWorkshop.getStartDate();
        Calendar endTime = mBookedWorkshop.getEndDate();

        event.makeEvent(calId, title, description, location, beginTime, endTime);
        event.insertEventAndReminders(selectedReminders, arrayResId);
    }

    /**
     * send a request to the server to get a list of campus
     */
    private void loadCampus() {
        try {
            mProgressBar.setVisibility(View.VISIBLE);
            MobileRESTClient.getInstance(mContext).getWorkshopController().listCampusLocation(this);
        } catch (NetworkErrorException e) {
            Utilities.displayError(mContext, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    /**
     * @param requestResponse
     * @return a list of campuses to populate the location spinner
     */
    private String[] getCampusList(RequestResponse requestResponse) {
        CampusListResponse campusListResponse = (CampusListResponse) requestResponse;
        String[] campusList = new String[campusListResponse.getResults().size()+1];
        for (int i = 1; i < campusListResponse.getResults().size()+1; i++) {
            campusList[i] = campusListResponse.getResults().get(i-1).getLocation();
        }
        campusList[0] = "";
        Arrays.sort(campusList);
        return campusList;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.READ_CALENDAR_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.displayToast(mContext, getString(R.string.permission_granted));
                    showCalendarEventDialog();
                } else {
                    Utilities.displayToast(mContext, getString(R.string.permission_denied));
                }
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mContext.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }



    private void clearEditText(View v){
        EditText et = (EditText) v;
        et.setText("");
    }

    /**
     * mainly just to toggle the clear button
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) mContext.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        mDateClearBtn.setVisibility(GONE);
        mTopicClearBtn.setVisibility(GONE);
        int id = v.getId();
        if(id == R.id.fragment_make_workshop_search_by_date) {
            mDatePickerDialog.show();
            mDateClearBtn.setVisibility(View.VISIBLE);
        }
        else if(id == R.id.fragment_make_workshop_search_by_topic){
            mTopicClearBtn.setVisibility(View.VISIBLE);
        }
        return false;
    }
}
