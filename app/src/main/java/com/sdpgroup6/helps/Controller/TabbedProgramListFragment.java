package com.sdpgroup6.helps.Controller;

import android.accounts.NetworkErrorException;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.Program;
import com.sdpgroup6.helps.Model.ProgramResponse;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.util.Calendar;
import java.util.List;

import static android.view.View.GONE;

/**
 * takes cares showing program list in program tab of the make booking activity
 */

public class TabbedProgramListFragment extends Fragment implements RestResponseHandler, View.OnClickListener, View.OnTouchListener {

    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private Context mContext;
    private EditText mTopicEt;
    private EditText mStartDateEt;
    private EditText mEndDateEt;
    private Button mSearchBtn;
    private ImageButton mExpandBtn;
    private LinearLayout mSearchFieldLayout;
    private static boolean sExpanded = false;
    private DatePickerDialog mStartDatePickerDialog;
    private DatePickerDialog mEndDatePickerDialog;
    private TextView mNoResultTv;
    private ImageButton mStartDateClear;
    private ImageButton mEndDateClear;
    private ImageButton mTopicClear;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_make_program_booking, container, false);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.fragment_make_program_booking_progressbar);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_make_program_booking_recyclerview);
        mStartDateEt = (EditText) rootView.findViewById(R.id.fragment_make_program_search_by_start_date);
        mEndDateEt = (EditText) rootView.findViewById(R.id.fragment_make_program_search_by_end_date);
        mSearchBtn = (Button) rootView.findViewById(R.id.fragment_make_program_detail_search_button);
        mTopicEt = (EditText) rootView.findViewById(R.id.fragment_make_program_search_by_topic);
        mExpandBtn = (ImageButton) rootView.findViewById(R.id.fragment_make_program_search_expand_button);
        mSearchFieldLayout = (LinearLayout) rootView.findViewById(R.id.fragment_make_program_booking_search_field_layout);
        mNoResultTv = (TextView) rootView.findViewById(R.id.fragment_make_program_booking_no_result_textview);
        mStartDateClear = (ImageButton) rootView.findViewById(R.id.fragment_make_program_booking_start_date_clear_btn);
        mEndDateClear = (ImageButton) rootView.findViewById(R.id.fragment_make_program_booking_end_date_clear_btn);
        mTopicClear = (ImageButton) rootView.findViewById(R.id.fragment_make_program_booking_topic_clear_btn);
        return rootView;
    }

    @Override
    public void onStart() {
        mContext = getContext();
        mStartDateEt.setInputType(0x00000000); //hide keyboard, it's an android bug
        mEndDateEt.setInputType(0x00000000);
        mTopicEt.setOnTouchListener(this);
        mStartDateEt.setOnTouchListener(this);
        mEndDateEt.setOnTouchListener(this);
        mSearchBtn.setOnClickListener(this);
        mExpandBtn.setOnClickListener(this);
        mEndDateClear.setOnClickListener(this);
        mStartDateClear.setOnClickListener(this);
        mTopicClear.setOnClickListener(this);
        setUpDatePickerDialogs();
        AppPreferences.getInstance(mContext.getApplicationContext()).loadStudentId();
        super.onStart();
    }

    /**
     * get program based on the condition/parameter
     *
     * @param topic     string to search for topic
     * @param startDate start date to search
     * @param endDate   end date to search
     */
    private void getPrograms(String topic, String startDate, String endDate) {
        try {
            if (startDate.equals("")) {
                startDate = Utilities.getAppTimeString();
            }

            if (endDate.equals("")) {
                Calendar temp = (Calendar) Utilities.getAppTime().clone();
                temp.add(Calendar.YEAR, 10);
                endDate = Utilities.getFormattedDate(temp);
            }

            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);

                MobileRESTClient.getInstance(getContext()).getProgramController().loadProgram(topic, null, null, Utilities.getCalendarByString(startDate),
                        Utilities.getCalendarByString(endDate), true, null, null, this);
            }
        } catch (NetworkErrorException e) {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
            e.printStackTrace();

        }
    }

    @Override
    public void onRestResponse(RequestResponse requestResponse) {
        sExpanded = true;
        toggleSearchLayout();
        if (requestResponse instanceof ProgramResponse) {
            mProgressBar.setVisibility(View.GONE);
            setUpRecyclerView(requestResponse);
        }
    }

    @Override
    public void onResponseFailed(String errorMsg) {
        toggleSearchLayout();
        Utilities.displayError(getContext(), errorMsg);
    }

    private void setUpRecyclerView(RequestResponse requestResponse) {
        ProgramResponse programResponse = (ProgramResponse) requestResponse;

        if (programResponse.getProgram() != null) {
            List<Program> programs = programResponse.getProgram();

            if (programs.size() == 0) {
                mNoResultTv.setVisibility(View.VISIBLE);
            } else {
                TabbedProgramAdapter programAdapter = new TabbedProgramAdapter(getContext(), programs);

                assert mRecyclerView != null;
                mRecyclerView.setVisibility(View.VISIBLE);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                mRecyclerView.setAdapter(programAdapter);
                mNoResultTv.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.fragment_make_program_detail_search_button) {
            search();
        } else if (id == R.id.fragment_make_program_search_expand_button) {
            toggleSearchLayout();
        } else if (id == R.id.fragment_make_program_booking_start_date_clear_btn) {
            clearEditText(mStartDateEt);
        } else if (id == R.id.fragment_make_program_booking_end_date_clear_btn) {
            clearEditText(mEndDateEt);
        } else if (id == R.id.fragment_make_program_booking_topic_clear_btn) {
            clearEditText(mTopicEt);
        }
    }

    private String[] getSearchFields() {
        String searchFields[] = new String[3];
        searchFields[0] = mTopicEt.getText().toString(); //topic
        searchFields[1] = mStartDateEt.getText().toString(); //start date
        searchFields[2] = mEndDateEt.getText().toString(); //end date
        return searchFields;
    }

    private void search() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        try {
            mNoResultTv.setVisibility(GONE);
            hideKeyboard();
            String[] searchInputs = getSearchFields();

            getPrograms(searchInputs[0], searchInputs[1], searchInputs[2]);
        } catch (Exception e) {
            Utilities.displayError(mContext, e.getMessage());
        }
    }

    private void toggleSearchLayout() {
        if (sExpanded) {
            mSearchFieldLayout.setVisibility(GONE);
            sExpanded = false;
            mExpandBtn.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
        } else {
            mSearchFieldLayout.setVisibility(View.VISIBLE);
            sExpanded = true;
            mExpandBtn.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
        }
    }

    private void setUpDatePickerDialogs() {
        int startYear = Utilities.getAppTime().get(Calendar.YEAR);
        int starthMonth = Utilities.getAppTime().get(Calendar.MONTH);
        Calendar dayBefore = Utilities.getAppTime();
        dayBefore.add(Calendar.DAY_OF_MONTH, -1);

        mStartDatePickerDialog = new DatePickerDialog(mContext, R.style.DatePickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = Utilities.getFormattedDate(dayOfMonth, month + 1, year);
                mStartDateEt.setText(date);
            }
        }, startYear, starthMonth, dayBefore.get(Calendar.DAY_OF_MONTH));
        mStartDatePickerDialog.getDatePicker().setMinDate(dayBefore.getTimeInMillis());

        mEndDatePickerDialog = new DatePickerDialog(mContext, R.style.DatePickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = Utilities.getFormattedDate(dayOfMonth, month + 1, year);
                mEndDateEt.setText(date);
            }
        }, startYear, starthMonth, dayBefore.get(Calendar.DAY_OF_MONTH));
        mEndDatePickerDialog.getDatePicker().setMinDate(dayBefore.getTimeInMillis());
    }

    /**
     * mainly focus on toggling the 'X', clear button
     * @param v
     * @param event
     * @return
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) mContext.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        int id = v.getId();
        mEndDateClear.setVisibility(GONE);
        mStartDateClear.setVisibility(GONE);
        mTopicClear.setVisibility(GONE);

        if (id == R.id.fragment_make_program_search_by_start_date) {
            mStartDatePickerDialog.show();
            mStartDateClear.setVisibility(View.VISIBLE);
        } else if (id == R.id.fragment_make_program_search_by_end_date) {
            mEndDatePickerDialog.show();
            mEndDateClear.setVisibility(View.VISIBLE);
        } else if (id == R.id.fragment_make_program_search_by_topic) {
            mTopicClear.setVisibility(View.VISIBLE);
        }
        return false;
    }

    public void getProgram() {
        getPrograms(null, "", "");
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mContext.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    private void clearEditText(View v) {
        EditText et = (EditText) v;
        et.setText("");
    }
}
