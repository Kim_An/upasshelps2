package com.sdpgroup6.helps.Controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

/**
 * Is the class for the activity of the Program info page
 */
public class ProgramInfoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private DrawerLayout mDrawer;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_program_info);

        mToolbar = (Toolbar) findViewById(R.id.activity_progrom_info_toolbar);
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        LinearLayout rootView = (LinearLayout) findViewById(R.id.activity_program_info_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);
        setUpNavigationDrawer();
    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = ActivityManager.getSwitchActivities(this, item.getItemId());

        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }

    /**
     * set up navigation drawer
     * set student id on the nav header, and the profile picture
     */
    private void setUpNavigationDrawer() {
        mDrawer = (DrawerLayout) findViewById(R.id.nav_program_info_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_program_info_nav_view);

        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            ImageView profilePicture = (ImageView) header.findViewById(R.id.nav_header_profile_picture_imageview);

            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setCheckedItem(R.id.nav_program_info_item);
            profilePicture.setOnClickListener(this);
            Utilities.setStudentIdOnNav(getApplicationContext(), header, getString(R.string.student_email_suffix));
        }
    }

    @Override
    protected void onResume() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_program_info_nav_view);
        if (navigationView != null) {
            navigationView.setCheckedItem(R.id.nav_program_info_item);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.nav_header_profile_picture_imageview) {
            startActivity(ActivityManager.intentToRegisterActivity(this));
        }
    }
}
