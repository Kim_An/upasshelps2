package com.sdpgroup6.helps.Controller;

import android.Manifest;
import android.accounts.NetworkErrorException;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdpgroup6.helps.CalendarHelper.CalendarChooser;
import com.sdpgroup6.helps.CalendarHelper.CalendarDialogHandler;
import com.sdpgroup6.helps.CalendarHelper.CalendarEventReminder;
import com.sdpgroup6.helps.CalendarHelper.PermissionHandler;
import com.sdpgroup6.helps.CalendarHelper.ReminderChooser;
import com.sdpgroup6.helps.CalendarHelper.ReminderDialogHandler;
import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.Model.WorkshopResponse;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Takes care of displaying a list of workshop of a workshop set, making event and setting reminders
 */
public class TabbedWorkshopSetWorkshopListActivity extends AppCompatActivity implements RestResponseHandler, PermissionHandler,
        CalendarDialogHandler, ReminderDialogHandler {

    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private String mWorkshopSetId;
    private Workshop mBookedWorkshop;
    private String mBookingId;
    private long mCalendarId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_detail);

        CoordinatorLayout rootView = (CoordinatorLayout) findViewById(R.id.activity_workshop_detail_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_workshop_detail_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mProgressBar = (ProgressBar) findViewById(R.id.workshop_detail_progressbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.workshop_detail_single_workshop_recyclerview);

        //get workshop set id and name from the previous activity
        if (getIntent().getStringExtra(Constants.WORKSHOPSET_ID) != null) {
            mWorkshopSetId = getIntent().getStringExtra(Constants.WORKSHOPSET_ID);
            getWorkshops();

            if (getIntent().getStringExtra(Constants.WORKSHOPSET_NAME) != null) {
                String workshopSetName = getIntent().getStringExtra(Constants.WORKSHOPSET_NAME);
                CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) findViewById(R.id.activity_workshop_detail_toolbar_layout);

                if (appBarLayout != null) {
                    int[][] states = ImageDecoder.getColorState();
                    int[] color = ImageDecoder.getColor(getApplicationContext());
                    ColorStateList colorStateList = new ColorStateList(states, color);

                    appBarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    appBarLayout.setExpandedTitleTextColor(colorStateList);
                    appBarLayout.setTitle(workshopSetName);
                }
            }
        }
    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    /**
     * send a request to server to load a workshop
     */
    private void getWorkshops() {
        try {
            Calendar temp = (Calendar) Utilities.getAppTime().clone();
            temp.add(Calendar.YEAR, 10); //change this 10 to a user specified filter later

            MobileRESTClient.getInstance(this).getWorkshopController().
                    loadWorkshop(mWorkshopSetId, null, Utilities.getAppTime(), temp, null, null, null, true, null, null, this);
        } catch (NetworkErrorException e) {
            e.printStackTrace();
            Utilities.displayError(this, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRestResponse(RequestResponse requestResponse) {
        WorkshopResponse workshopResponse = (WorkshopResponse) requestResponse;
        if (workshopResponse.getWorkShops() != null) {
            mProgressBar.setVisibility(View.GONE);
            SingleWorkshopAdapter singleWorkshopAdapter = new SingleWorkshopAdapter(this, this, this, workshopResponse.getWorkShops());
            assert mRecyclerView != null;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setAdapter(singleWorkshopAdapter);
        }
    }

    @Override
    public void onResponseFailed(String errorMsg) {
        Toast.makeText(this, getString(R.string.error) + errorMsg, Toast.LENGTH_LONG).show();
        mProgressBar.setVisibility(View.GONE);
    }

    //this is called when the booking in adapter is made successfully
    @Override
    public void onBookingSuccess(RequestResponse requestResponse, Workshop workshop) {
        BookWorkshopResponse bookWorkshopResponse = (BookWorkshopResponse) requestResponse;
        mBookingId = String.valueOf(bookWorkshopResponse.getBooking().getBookingId());
        mBookedWorkshop = workshop;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Constants.READ_CALENDAR_PERMISSION}, Constants.READ_CALENDAR_PERMISSION_REQUEST);
        } else {
            showCalendarEventDialog();
        }
    }

    private void showCalendarEventDialog() {
        CalendarChooser calendarChooser = new CalendarChooser(this, this, getContentResolver(), this);
        calendarChooser.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.READ_CALENDAR_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.displayToast(this, getString(R.string.permission_granted));
                    showCalendarEventDialog();
                } else {
                    Utilities.displayToast(this, getString(R.string.permission_denied));
                }
                break;
            }
        }
    }

    @Override
    public void onCalendarDialogResponse(boolean positive, long calId) {
        mCalendarId = calId;
        ReminderChooser reminderChooser = new ReminderChooser(this, this, getContentResolver(), this);
        reminderChooser.show();
    }

    /**
     * make event in users' calendar using the following param
     * @param calId calendar id
     * @param selectedReminders sequence of the reminder selected
     * @param arrayResId
     * @throws ParseException
     */
    private void makeEvent(long calId, boolean[] selectedReminders, int arrayResId) throws ParseException {
        CalendarEventReminder event = new CalendarEventReminder(this, this, getContentResolver(), mBookingId);
        String title = mBookedWorkshop.getTopic();
        String description = mBookedWorkshop.getDescription();
        String location = mBookedWorkshop.getCampus();
        Calendar beginTime = mBookedWorkshop.getStartDate();
        Calendar endTime = mBookedWorkshop.getEndDate();

        event.makeEvent(calId, title, description, location, beginTime, endTime);
        event.insertEventAndReminders(selectedReminders, arrayResId);
    }

    @Override
    public void onReminderChosen(boolean[] selectedReminders, int arrayResId) {
        try {
            makeEvent(mCalendarId, selectedReminders, arrayResId);
        } catch (ParseException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }
}
