package com.sdpgroup6.helps.Controller;

import android.Manifest;
import android.accounts.NetworkErrorException;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdpgroup6.helps.CalendarHelper.CalendarChooser;
import com.sdpgroup6.helps.CalendarHelper.CalendarDialogHandler;
import com.sdpgroup6.helps.CalendarHelper.CalendarEventReminder;
import com.sdpgroup6.helps.CalendarHelper.ReminderChooser;
import com.sdpgroup6.helps.CalendarHelper.ReminderDialogHandler;
import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.Model.WorkshopBooking;
import com.sdpgroup6.helps.Model.WorkshopResponse;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestProgramResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Takes care of view program workshops, book program, and set reminders
 */

public class TabbedProgramDetailActivity extends AppCompatActivity implements RestResponseHandler,
        View.OnClickListener, RestProgramResponseHandler, CalendarDialogHandler, ReminderDialogHandler{

    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private String mProgramId;
    private Button mBookBtn;
    private ArrayList<RequestResponse> mRequestResponses;
    private List<Workshop> mProgramWorkshops;
    private long mCalendarId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_detail);

        mProgressBar = (ProgressBar) findViewById(R.id.program_detail_progressbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.program_detail_single_workshop_recyclerview);
        mBookBtn = (Button) findViewById(R.id.activity_program_detail_book_button);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_program_detail_toolbar);

        mBookBtn.setOnClickListener(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        CoordinatorLayout rootView = (CoordinatorLayout) findViewById(R.id.activity_program_detail_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);

        //get the program id from the previous activity
        if (getIntent().getStringExtra(Constants.PROGRAM_ID) != null) {
            mProgramId = getIntent().getStringExtra(Constants.PROGRAM_ID);
            getWorkshops();

            if (getIntent().getStringExtra(Constants.PROGRAM_NAME) != null) {
                String programName = getIntent().getStringExtra(Constants.PROGRAM_NAME);
                CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) findViewById(R.id.activity_program_detail_toolbar_layout);


                if (appBarLayout != null) {
                    //set color to match the theme
                    int[][] states = ImageDecoder.getColorState();
                    int[] color = ImageDecoder.getColor(getApplicationContext());
                    ColorStateList colorStateList = new ColorStateList(states, color);

                    appBarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    appBarLayout.setExpandedTitleTextColor(colorStateList);
                    appBarLayout.setTitle(programName);
                }
            }
        }
    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    /**
     * send a request to the server to get workshops of a program
     */
    private void getWorkshops() {
        try {
            MobileRESTClient.getInstance(this).getProgramController().getProgramWorkshops(mProgramId, this);
        } catch (NetworkErrorException e) {
            e.printStackTrace();

            Utilities.displayError(this, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRestResponse(RequestResponse requestResponse) {
        if(requestResponse instanceof WorkshopResponse){
            setUpRecyclerView(requestResponse);
        }
    }

    @Override
    public void onRestResponse(ArrayList<RequestResponse> requestResponse) {
        Utilities.displayToast(this, getString(R.string.booking_success));
        mRequestResponses = requestResponse;

        //checks for reading calendar permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Constants.READ_CALENDAR_PERMISSION}, Constants.READ_CALENDAR_PERMISSION_REQUEST);
        } else {
            showCalendarEventDialog();
        }

        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponseFailed(String errorMsg) {
        Toast.makeText(this, getString(R.string.error) + errorMsg, Toast.LENGTH_LONG).show();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.READ_CALENDAR_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.displayToast(this, getString(R.string.permission_granted));
                    showCalendarEventDialog();
                } else {
                    Utilities.displayToast(this, getString(R.string.permission_denied));
                }
                break;
            }
        }
    }

    private void setUpRecyclerView(RequestResponse requestResponse){
        WorkshopResponse workshopResponse = (WorkshopResponse) requestResponse;
        if (workshopResponse.getWorkShops() != null) {
            mProgressBar.setVisibility(View.GONE);

            mProgramWorkshops = workshopResponse.getWorkShops();
            ProgramWorkshopAdapter programWorkshopAdapter = new ProgramWorkshopAdapter(workshopResponse.getWorkShops());

            assert mRecyclerView != null;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setAdapter(programWorkshopAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.activity_program_detail_book_button){
            bookProgram(mProgramId);
        }
    }

    /**
     * send a request to the server to book program
     * @param programId is the id of the program to book
     */
    private void bookProgram(String programId){
        mProgressBar.setVisibility(View.VISIBLE);
        try {
            MobileRESTClient.getInstance(this).getProgramController().bookProgram(programId, this);
        } catch (NetworkErrorException e) {
            Utilities.displayError(this, e.getMessage());
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    private void showCalendarEventDialog() {
        CalendarChooser calendarChooser = new CalendarChooser(this, this, getContentResolver(), this);
        calendarChooser.show();
    }

    @Override
    public void onCalendarDialogResponse(boolean positive, long calId) {
        mCalendarId = calId;
        ReminderChooser reminderChooser = new ReminderChooser(this, this, getContentResolver(), this);
        reminderChooser.show();
    }

    @Override
    public void onReminderChosen(boolean[] selectedReminder, int arrayResId) {
        try {
            makeEvent(mCalendarId, selectedReminder, arrayResId);
        } catch (ParseException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
    }

    /**
     * makes event by looping throught the workshops that has been booked within a program
     * @param calId is the calendar id
     * @param selectedReminders is the sequence of the reminder selected
     * @param arrayResId is the reminder array displayed
     * @throws ParseException
     */
    private void makeEvent(long calId, boolean[] selectedReminders, int arrayResId) throws ParseException {
        ArrayList<WorkshopBooking> workshopBookings = getConvertedRequestResponseToBookWorkshopResponse();
        CalendarEventReminder event;
        for (int i = 0; i < workshopBookings.size(); i++) {

            String bookingId = String.valueOf(workshopBookings.get(i).getBookingId());
            event = new CalendarEventReminder(this, this, getContentResolver(), bookingId);

            Workshop mBookedWorkshop = getWorkshopByWorkshopBooking(workshopBookings.get(i));
            if (mBookedWorkshop != null) {
                String title = mBookedWorkshop.getTopic();
                String description = mBookedWorkshop.getDescription();
                String location = mBookedWorkshop.getCampus();
                Calendar beginTime = mBookedWorkshop.getStartDate();
                Calendar endTime = mBookedWorkshop.getEndDate();
                event.makeEvent(calId, title, description, location, beginTime, endTime);
                event.insertEventAndReminders(selectedReminders, arrayResId);
            }
        }
    }

    /**
     * @return workshopBooking in arraylist, extracted from BookWorkshopResponses
     */
    private ArrayList<WorkshopBooking> getConvertedRequestResponseToBookWorkshopResponse(){
        ArrayList<BookWorkshopResponse> bookWorkshopResponses = new ArrayList<>();
        ArrayList<WorkshopBooking> workshopBookings = new ArrayList<>();
        for(RequestResponse response: mRequestResponses){
            BookWorkshopResponse bookWorkshopResponse = (BookWorkshopResponse) response;
            bookWorkshopResponses.add(bookWorkshopResponse);
        }
        for(BookWorkshopResponse response: bookWorkshopResponses){
            workshopBookings.add(response.getBooking());
        }
        return workshopBookings;
    }

    /**
     * @param workshopBooking
     * @return checks if the workshop in program workshop matches the workshop in workshopBooking,
     * returns that working if exists, else return null value
     */
    private Workshop getWorkshopByWorkshopBooking(WorkshopBooking workshopBooking){
        for(Workshop workshop: mProgramWorkshops){
            if(workshop.isWorkshopId(workshopBooking.getWorkshopID())){
                return workshop;
            }
        }
        return null;
    }
}
