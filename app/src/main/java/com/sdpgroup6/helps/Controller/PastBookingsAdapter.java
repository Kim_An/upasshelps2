package com.sdpgroup6.helps.Controller;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sdpgroup6.helps.Model.BookedWorkshop;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

/**
 * Takes care of the content within a card of past booking,
 * no interaction involved because workshop that is passed can't be cancelled or set reminder
 */
public class PastBookingsAdapter extends RecyclerView.Adapter<PastBookingsAdapter.MyViewHolder> {

    List<BookedWorkshop> mBookedWorkshops;
    Context mContext;

    public PastBookingsAdapter(Context context, List<BookedWorkshop> bookedWorkshops) {
        mContext = context;
        mBookedWorkshops = bookedWorkshops;
    }

    /**
     * Inflate the adapter view and return a view holder with the layout
     */
    @Override
    public PastBookingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_bookings, parent, false);

        return new MyViewHolder(v, mBookedWorkshops);
    }

    /**
     * set text in the view holder
     */
    @Override
    public void onBindViewHolder(PastBookingsAdapter.MyViewHolder holder, int position) {
        Calendar workshopCal = null;
        BookedWorkshop bookedWorkshop = mBookedWorkshops.get(position);
        String durationFormat = mContext.getString(R.string.duration_placeholder);
        String duration = String.format(durationFormat, bookedWorkshop.getDuration());
        String attended = bookedWorkshop.getAttended();

        holder.mAttendanceTv.setText(attended);
        holder.mWorkshopTitleTv.setText(bookedWorkshop.getTopic());
        holder.mLocationTv.setText(bookedWorkshop.getCampus());
        holder.mDurationTv.setText(duration);

        try {
            workshopCal = bookedWorkshop.getStarting();
            holder.mWorkshopDateTv.setText(Utilities.getFormattedDate(workshopCal));
            holder.mWorkshopTimeTv.setText(Utilities.getAppTime(workshopCal));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.mWorkshopDateTv.setText(Constants.GENERIC_ERROR);
            return;
        }

        if (bookedWorkshop.getWorkshopType().isEmpty()) {
            holder.mWorkshopTypeTv.setVisibility(View.GONE);
        } else {
            holder.mWorkshopTypeTv.setText(bookedWorkshop.getWorkshopType());
        }
    }

    @Override
    public int getItemCount() {
        return mBookedWorkshops.size();
    }

    /**
     * recyclerview view holder class
     * links to the adapter and attach java object with the xml view
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private List<BookedWorkshop> mBookedWorkshopList;
        private TextView mWorkshopTitleTv;
        private TextView mWorkshopDateTv;
        private TextView mWorkshopTimeTv;
        private TextView mLocationTv;
        private TextView mWorkshopTypeTv;
        private Button mCancelBtn;
        private Button mRemindBtn;
        private TextView mDurationTv;
        private TextView mAttendanceTv;

        public MyViewHolder(View itemView, List<BookedWorkshop> bookedWorkshopList) {
            super(itemView);
            this.mBookedWorkshopList = bookedWorkshopList;
            mWorkshopTitleTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_title_textview);
            mWorkshopDateTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_date_textview);
            mWorkshopTimeTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_time_textview);
            mLocationTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_location_textview);
            mCancelBtn = (Button) itemView.findViewById(R.id.adapater_my_bookings_cancel_button);
            mRemindBtn = (Button) itemView.findViewById(R.id.adapater_my_bookings_remind_button);
            mWorkshopTypeTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_type_textview);
            mDurationTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_duration_textview);
            mAttendanceTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_attendance_textview);

            mDurationTv.setVisibility(View.VISIBLE);
            mAttendanceTv.setVisibility(View.VISIBLE);
            mRemindBtn.setVisibility(View.GONE);
            mCancelBtn.setVisibility(View.GONE);
        }
    }


}