package com.sdpgroup6.helps.Controller;

import android.Manifest;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sdpgroup6.helps.CalendarHelper.CalendarEventReminder;
import com.sdpgroup6.helps.CalendarHelper.EventStorage;
import com.sdpgroup6.helps.CalendarHelper.ReminderHandler;
import com.sdpgroup6.helps.Model.BookedWorkshop;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

/**
 * takes care of the content of the card of upcoming booking
 * interaction available for setting reminder and cancel bookin
 */
public class UpcomingBookingsAdapter extends RecyclerView.Adapter<UpcomingBookingsAdapter.recyclerViewHolder> {

    List<BookedWorkshop> mBookedWorkshops;
    Context mContext;
    ReminderHandler mHandler;
    Activity mActivity;

    public UpcomingBookingsAdapter(Activity activity, ReminderHandler handler, Context context, List<BookedWorkshop> bookedWorkshops) {
        mActivity = activity;
        mHandler = handler;
        mContext = context;
        mBookedWorkshops = bookedWorkshops;
    }

    /**
     * Inflate the adapter view and return a view holder with the layout
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public UpcomingBookingsAdapter.recyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_bookings, parent, false);
        return new recyclerViewHolder(v, mBookedWorkshops);
    }

    /**
     * set text in the view holder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(UpcomingBookingsAdapter.recyclerViewHolder holder, int position) {
        BookedWorkshop bookedWorkshop = mBookedWorkshops.get(position);
        holder.mWorkshopTitleTv.setText(bookedWorkshop.getTopic());
        holder.mLocationTv.setText(bookedWorkshop.getCampus());

        Calendar workshopCal = null;
        try {
            workshopCal = bookedWorkshop.getStarting();
            holder.mWorkshopDateTv.setText(Utilities.getFormattedDate(workshopCal));
            holder.mWorkshopTimeTv.setText(Utilities.getAppTime(workshopCal));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.mWorkshopDateTv.setText("Problem with Parsing the Data!!! Contact your admin for help");
            return;
        }

        //disable the cancel button if the workshop is starting in 2 hours
        if (Utilities.isLessThanTwoHours(workshopCal)) {
            holder.mCancelBtn.setEnabled(false);
            holder.mCancelBtn.setTextColor(mContext.getResources().getColor(R.color.colorButtonCancel));
        }

        //shows the workshop type
        if (bookedWorkshop.getWorkshopType().equals("")) {
            holder.mWorkshopTypeTv.setVisibility(View.GONE);
        } else {
            holder.mWorkshopTypeTv.setText(bookedWorkshop.getWorkshopType());
        }
    }

    @Override
    public int getItemCount() {
        return mBookedWorkshops.size();
    }

    /**
     * recyclerview viewholder class
     * linked to the adapter and attach java object with the xml view
     */
    public class recyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, RestResponseHandler {


        private List<BookedWorkshop> mBookedWorkshopList;
        private TextView mWorkshopTitleTv;
        private TextView mWorkshopDateTv;
        private TextView mWorkshopTimeTv;
        private TextView mLocationTv;
        private Button mCancelBtn;
        private Button mRemindBtn;
        private TextView mWorkshopTypeTv;
        int selectedPos;
        String mBookingId;

        public recyclerViewHolder(View itemView, List<BookedWorkshop> bookedWorkshopList) {
            super(itemView);
            this.mBookedWorkshopList = bookedWorkshopList;
            mWorkshopTitleTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_title_textview);
            mWorkshopDateTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_date_textview);
            mWorkshopTimeTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_time_textview);
            mLocationTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_location_textview);
            mCancelBtn = (Button) itemView.findViewById(R.id.adapater_my_bookings_cancel_button);
            mRemindBtn = (Button) itemView.findViewById(R.id.adapater_my_bookings_remind_button);
            mWorkshopTypeTv = (TextView) itemView.findViewById(R.id.adapter_my_bookings_type_textview);
            mCancelBtn.setOnClickListener(this);
            mRemindBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            selectedPos = getAdapterPosition();
            int workshopId = mBookedWorkshopList.get(selectedPos).getWorkshopID();
            mBookingId = String.valueOf(mBookedWorkshopList.get(selectedPos).getBookingId());
            int id = v.getId();
            if (id == R.id.adapater_my_bookings_cancel_button) {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mActivity,
                            new String[]{Constants.READ_CALENDAR_PERMISSION}, 1234);
                } else {
                    cancelBooking(workshopId);
                }
            } else if (id == R.id.adapater_my_bookings_remind_button) {
                //there is an event related to the booking
//                if (EventStorage.getInstance(mContext).getEventId(mBookingId) != -1) {
//                    Utilities.displayToast(mContext, "Reminders has already been made");
//                } else {
                    mHandler.onRemindClicked(mBookedWorkshopList.get(selectedPos));
//                }
            }
        }

        /**
         * send a request to the server to cancel a booking
         * @param workshopId
         */
        private void cancelBooking(int workshopId) {
            Calendar workshopCal = null;
            try {
                workshopCal = mBookedWorkshopList.get(selectedPos).getStarting();
            } catch (ParseException e) {
                e.printStackTrace();
                //no date ? cant cancel!
                Utilities.displayError(mContext, "Can't cancel the workshop due to unknown time string");
                return;
            }
            try {
                MobileRESTClient.getInstance(mContext).getWorkshopController().cancelBookedWorkshop(String.valueOf(workshopId), workshopCal, this);
            } catch (NetworkErrorException e) {
                Utilities.displayError(mContext, e.getMessage());
            }
        }

        @Override
        public void onRestResponse(RequestResponse requestResponse) {
            Utilities.displayToast(mContext, mContext.getString(R.string.cancel_success));

            removeEvent(mBookingId);

            try {
                mBookedWorkshopList.remove(selectedPos);
                notifyDataSetChanged();
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onResponseFailed(String errorMsg) {
            Utilities.displayError(mContext, errorMsg);
        }
    }

    /**
     * remove event from calendar and from the shared preferences
     * @param workshopBookingId
     */
    public void removeEvent(String workshopBookingId) {
        long eventId = -1;
        if (workshopBookingId != null) {
            eventId = EventStorage.getInstance(mContext).getEventId(workshopBookingId);
        }
        //-1 means there is no event stored
        if (eventId != -1) {
            CalendarEventReminder event = new CalendarEventReminder(mContext.getContentResolver(), mActivity, mContext);
            event.deleteEventWithToast(eventId, workshopBookingId);
        } else {
            Utilities.displayError(mContext, Constants.NO_REMINDER_RELATED_TO_THIS_BOOKING);
        }
    }
}