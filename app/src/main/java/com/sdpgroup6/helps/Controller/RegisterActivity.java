package com.sdpgroup6.helps.Controller;

import android.accounts.NetworkErrorException;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.GetStudentResponse;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.Model.RegisterRequest;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.Student;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Is the class for register page/activity and profile page/activity
 */
public class RegisterActivity extends AppCompatActivity
        implements RestResponseHandler, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener,
        CompoundButton.OnCheckedChangeListener, View.OnTouchListener {

    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private TextView mStudentIdTv;
    private EditText mPrefferedNameEt;
    private TextView mDobTv;
    private Spinner mGenderSpinner;
    private Spinner mStatusSpinner;
    private Spinner mFirstLanguageSpinner;
    private Spinner mCountryOriginSpinner;
    private EditText mAltContactEt;
    private Spinner mDegreeSpinner;
    private CheckBox mHscCb;
    private CheckBox mIeltsCb;
    private CheckBox mToeflcb;
    private CheckBox mTafecb;
    private CheckBox mCultcb;
    private CheckBox mInsearchDeepCb;
    private CheckBox mInsearchDipCb;
    private CheckBox mFoundationCourseCb;
    private EditText mHscEt;
    private EditText mIeltsEt;
    private EditText mToeflEt;
    private EditText mTafeEt;
    private EditText mCultEt;
    private EditText mInsearchDeepEt;
    private EditText mInsearchDipEt;
    private EditText mFoundationCourseEt;
    private TextInputLayout mDobTil;
    private TextInputLayout mHscTil;
    private TextInputLayout mIeltsTil;
    private TextInputLayout mToeflTil;
    private TextInputLayout mTafeTil;
    private TextInputLayout mCultTil;
    private TextInputLayout mInsearchDeepTil;
    private TextInputLayout mInsearchDipTil;
    private TextInputLayout mFoundationCourseTil;
    private DatePickerDialog mDatePickerDialog;
    private Student mStudent;
    private ProgressBar mProgressBar;
    private boolean mNewStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_register);

        initializeViews();
        setListeners();
        setSupportActionBar(mToolbar);
        setUpNavigationDrawer();

        ScrollView rootView = (ScrollView) findViewById(R.id.activity_register_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);

        mFirstLanguageSpinner.setDropDownWidth(700);
        mCountryOriginSpinner.setDropDownWidth(700);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        setUpPreFilled();
        super.onStart();
    }

    /**
     * Generate a Register Request
     *
     * @return the RegisterRequest, ready to send
     */
    private RegisterRequest generateRegisterRequest() {
        Map<String, String> inputs = getInputs();
        if (inputs != null) {
            RegisterRequest registerRequest = new RegisterRequest(inputs.get(Constants.STUDENT_ID),
                    inputs.get(Constants.DEGREE),
                    inputs.get(Constants.STATUS),
                    inputs.get(Constants.FIRST_LANGUAGE),
                    inputs.get(Constants.COUNTRY_ORIGIN),
                    inputs.get(Constants.STUDENT_ID));
            registerRequest.setPreferredName(inputs.get(Constants.PREFFERD_NAME));
            registerRequest.setDateOfBirth(inputs.get(Constants.DOB));
            registerRequest.setGender(inputs.get(Constants.GENDER));
            registerRequest.setAltContact(inputs.get(Constants.ALT_CONTACT));
            registerRequest.setHSC(inputs.get(Constants.HSC));
            registerRequest.setHSCMark(inputs.get(Constants.HSC_MARK));
            registerRequest.setIELTS(inputs.get(Constants.IELTS));
            registerRequest.setIELTSMark(inputs.get(Constants.IELTS_MARK));
            registerRequest.setTOEFL(inputs.get(Constants.TOEFL));
            registerRequest.setTOEFLMark(inputs.get(Constants.TOEFL_MARK));
            registerRequest.setTAFE(inputs.get(Constants.TAFE));
            registerRequest.setTAFEMark(inputs.get(Constants.TAFE_MARK));
            registerRequest.setCULT(inputs.get(Constants.CULT));
            registerRequest.setCULTMark(inputs.get(Constants.CULT_MARK));
            registerRequest.setInsearchDEEP(inputs.get(Constants.INSEARCH_DEEP));
            registerRequest.setInsearchDEEPMark(inputs.get(Constants.INSEARCH_DEEP_MARK));
            registerRequest.setInsearchDiploma(inputs.get(Constants.INSEARCH_DIP));
            registerRequest.setInsearchDiplomaMark(inputs.get(Constants.INSEARCH_DIP_MARK));
            registerRequest.setFoundationCourse(inputs.get(Constants.FOUNDATION_COURSE));
            registerRequest.setFoundationCourseMark(inputs.get(Constants.FOUNDATION_MARK));
            return registerRequest;
        }
        return null;
    }

    @Override
    public void onRestResponse(RequestResponse requestResponse) {
        String id = String.valueOf(mStudentIdTv.getText());
        mProgressBar.setVisibility(View.GONE);
        if (requestResponse instanceof GetStudentResponse) {
            GetStudentResponse getStudentResponse = (GetStudentResponse) requestResponse;
            mStudent = getStudentResponse.getResult();
            try {
                setPreFilledInfo();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            if (mNewStudent) {
                Toast.makeText(RegisterActivity.this,
                        getString(R.string.register_succeed), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(RegisterActivity.this,
                        getString(R.string.save_profile_succeed), Toast.LENGTH_SHORT).show();
            }
            startActivity(ActivityManager.login(getApplicationContext(), RegisterActivity.this, id));
            finish();
        }
    }

    @Override
    public void onResponseFailed(String errorMsg) {
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(RegisterActivity.this,
                getString(R.string.register_failed) + errorMsg, Toast.LENGTH_LONG).show();
    }

    /**
     * Inflating menu layout
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Callback function when options in menu is selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //route up button press to back button press
            onBackPressed();
            return true;
        } else if (id == R.id.action_register_item) {
            register();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = ActivityManager.getSwitchActivities(this, item.getItemId());

        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }

    /**
     * sets up navigation drawer
     * sets student id on the nav header, and the profile picture
     */
    private void setUpNavigationDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_register_nav_view);

        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            ImageView profilePicture = (ImageView) header.findViewById(R.id.nav_header_profile_picture_imageview);

            profilePicture.setOnClickListener(this);
            navigationView.setNavigationItemSelectedListener(this);
            Utilities.setStudentIdOnNav(getApplicationContext(), header, getString(R.string.student_email_suffix));
        }
    }

    /**
     * ensures that the navigation drawer is disabled before user has logged in
     */
    @Override
    protected void onResume() {
        if (LoginSession.studentId.equals(Constants.NULL)) {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        //close nav drawer when the profile picture is selected
        if (id == R.id.nav_header_profile_picture_imageview) {
            if (mDrawer != null) {
                mDrawer.closeDrawer(GravityCompat.START);
            }
        }
    }

    //send a register request to the server, by give a registerRequest object
    private void register() {
        RegisterRequest registerRequest = generateRegisterRequest();

        if (registerRequest != null) {
            try {
                mProgressBar.setVisibility(View.VISIBLE);
                MobileRESTClient.getInstance(RegisterActivity.this)
                        .getStudentController().registerStudent(registerRequest, RegisterActivity.this);
            } catch (NetworkErrorException e) {
                Utilities.displayError(RegisterActivity.this, e.getMessage());

                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        } else {
            Utilities.displayToast(this, Constants.SOME_FIELDS_MUST_BE_COMPLETED);
        }
    }

    /**
     * new student would return true
     * when the intent comes from login activity
     *
     * @return
     */
    public boolean isNewStudent() {
        return getIntent().getStringExtra(Constants.STUDENT_ID) != null;
    }

    /**
     * check if the student is a new student to decide whether pre-filling of information is needed
     */
    public void setUpPreFilled() {
        //pre-fill Student ID
        if (isNewStudent()) {
            mNewStudent = true;
            mStudentIdTv.setText(getIntent().getStringExtra(Constants.STUDENT_ID));
            mProgressBar.setVisibility(View.GONE);
        } else {
            mNewStudent = false;
            try {
                getPrefillers();
            } catch (NetworkErrorException e) {
                Utilities.displayToast(this, e.getMessage());

                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * displays student existing detail onto the screen
     * @throws ParseException
     */
    private void setPreFilledInfo() throws ParseException {
        //placeholder for profile
        mToolbar.setTitle(getString(R.string.title_profile));
        String id = LoginSession.studentId;

        mStudentIdTv.setText(id);

        if (mStudent != null) {
            mPrefferedNameEt.setText(mStudent.getPreferred_name());
            try {
                String dob = Utilities.getDOBString(mStudent.getDob());
                mDobTv.setText(dob);
            } catch (ParseException e) {
                mDobTv.setText("");
            }
            mAltContactEt.setText(mStudent.getAlternative_contact());
            String genderValue = getFullStringFromShortcut(mStudent.getGender());
            setValueOnSpinner(mGenderSpinner, genderValue);
            setValueOnSpinner(mStatusSpinner, mStudent.getStatus());
            setValueOnSpinner(mFirstLanguageSpinner, mStudent.getFirst_language());
            setValueOnSpinner(mCountryOriginSpinner, mStudent.getCountry_origin());
            setValueOnSpinner(mDegreeSpinner, mStudent.getDegree());
            setEducationAndMark(mStudent.getHSC(), mHscCb, mStudent.getHSC_mark(), mHscEt);
            setEducationAndMark(mStudent.getIELTS(), mIeltsCb, mStudent.getIELTS_mark(), mIeltsEt);
            setEducationAndMark(mStudent.getTOEFL(), mToeflcb, mStudent.getTOEFL_mark(), mToeflEt);
            setEducationAndMark(mStudent.getTAFE(), mTafecb, mStudent.getTAFE_mark(), mTafeEt);
            setEducationAndMark(mStudent.getCULT(), mCultcb, mStudent.getCULT_mark(), mCultEt);
            setEducationAndMark(mStudent.getInsearchDEEP(), mInsearchDeepCb, mStudent.getInsearchDEEP_mark(), mInsearchDeepEt);
            setEducationAndMark(mStudent.getInsearchDiploma(), mInsearchDipCb, mStudent.getInsearchDiploma_mark(), mInsearchDipEt);
            setEducationAndMark(mStudent.getFoundationcourse(), mFoundationCourseCb, mStudent.getFoundationcourse_mark(), mFoundationCourseEt);
        }
    }

    /**
     * send a request to server to get information of a student
     * @throws NetworkErrorException
     */
    private void getPrefillers() throws NetworkErrorException {
        mProgressBar.setVisibility(View.VISIBLE);
        if (!AppPreferences.getInstance(getApplicationContext()).getStudentId().equals(Constants.NULL)) {
            MobileRESTClient.getInstance(this).getStudentController().getStudent(AppPreferences.getInstance(getApplicationContext()).getStudentId(), "", this);
            LoginSession.studentId = AppPreferences.getInstance(getApplicationContext()).getStudentId();
        }
    }

    //references view from xml
    public void initializeViews() {
        mToolbar = (Toolbar) findViewById(R.id.activity_register_toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.nav_register_drawer_layout);
        mStudentIdTv = (TextView) findViewById(R.id.activity_register_student_id_textview);
        mGenderSpinner = (Spinner) findViewById(R.id.activity_register_gender_spinner);
        mDegreeSpinner = (Spinner) findViewById(R.id.activity_register_degree_spinner);
        mStatusSpinner = (Spinner) findViewById(R.id.activity_register_status_spinner);
        mFirstLanguageSpinner = (Spinner) findViewById(R.id.activity_register_first_language_spinner);
        mCountryOriginSpinner = (Spinner) findViewById(R.id.activity_register_country_origin_spinner);
        mPrefferedNameEt = (EditText) findViewById(R.id.activity_register_preffered_name_edittext);
        mAltContactEt = (EditText) findViewById(R.id.activity_register_alt_contact_edittext);
        mDobTv = (TextView) findViewById(R.id.activity_register_dob_textview);
        mHscCb = (CheckBox) findViewById(R.id.activity_register_hsc_checkbox);
        mIeltsCb = (CheckBox) findViewById(R.id.activity_register_ielts_checkbox);
        mToeflcb = (CheckBox) findViewById(R.id.activity_register_toefl_checkbox);
        mTafecb = (CheckBox) findViewById(R.id.activity_register_tafe_checkbox);
        mCultcb = (CheckBox) findViewById(R.id.activity_register_cult_checkbox);
        mInsearchDeepCb = (CheckBox) findViewById(R.id.activity_register_insearch_deep_checkbox);
        mInsearchDipCb = (CheckBox) findViewById(R.id.activity_register_insearch_dip_checkbox);
        mFoundationCourseCb = (CheckBox) findViewById(R.id.activity_register_foundation_course_checkbox);
        mHscEt = (EditText) findViewById(R.id.activity_register_hsc_mark_edittext);
        mIeltsEt = (EditText) findViewById(R.id.activity_register_ielts_mark_edittext);
        mToeflEt = (EditText) findViewById(R.id.activity_register_toefl_mark_edittext);
        mTafeEt = (EditText) findViewById(R.id.activity_register_tafe_mark_edittext);
        mCultEt = (EditText) findViewById(R.id.activity_register_cult_mark_edittext);
        mInsearchDeepEt = (EditText) findViewById(R.id.activity_register_insearch_deep_mark_edittext);
        mInsearchDipEt = (EditText) findViewById(R.id.activity_register_insearch_dip_mark_edittext);
        mFoundationCourseEt = (EditText) findViewById(R.id.activity_register_foundation_course_mark_edittext);
        mHscTil = (TextInputLayout) findViewById(R.id.activity_register_hsc_mark_input_layout);
        mIeltsTil = (TextInputLayout) findViewById(R.id.activity_register_ielts_mark_input_layout);
        mToeflTil = (TextInputLayout) findViewById(R.id.activity_register_toefl_mark_input_layout);
        mTafeTil = (TextInputLayout) findViewById(R.id.activity_register_tafe_mark_input_layout);
        mCultTil = (TextInputLayout) findViewById(R.id.activity_register_cult_mark_input_layout);
        mInsearchDeepTil = (TextInputLayout) findViewById(R.id.activity_register_insearch_deep_mark_input_layout);
        mInsearchDipTil = (TextInputLayout) findViewById(R.id.activity_register_insearch_dip_mark_input_layout);
        mFoundationCourseTil = (TextInputLayout) findViewById(R.id.activity_register_foundation_course_mark_input_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.activity_register_progress_bar);
    }

    private void setListeners() {
        mHscCb.setOnCheckedChangeListener(this);
        mIeltsCb.setOnCheckedChangeListener(this);
        mToeflcb.setOnCheckedChangeListener(this);
        mTafecb.setOnCheckedChangeListener(this);
        mCultcb.setOnCheckedChangeListener(this);
        mFoundationCourseCb.setOnCheckedChangeListener(this);
        mInsearchDeepCb.setOnCheckedChangeListener(this);
        mInsearchDipCb.setOnCheckedChangeListener(this);
        mGenderSpinner.setOnTouchListener(this);
        mDegreeSpinner.setOnTouchListener(this);
        mStatusSpinner.setOnTouchListener(this);
        mFirstLanguageSpinner.setOnTouchListener(this);
        mCountryOriginSpinner.setOnTouchListener(this);
    }

    /**
     * gets inputs from the edit text, spinners, and checkboxes
     * @return null if the compulsory fields are empty, and stop the processing
     * otherwise continue to get all the other inputs
     */
    private Map<String, String> getInputs() {
        Map<String, String> inputMap = new HashMap<>();

        inputMap.put(Constants.STATUS, mStatusSpinner.getSelectedItem().toString());
        inputMap.put(Constants.FIRST_LANGUAGE, mFirstLanguageSpinner.getSelectedItem().toString());
        inputMap.put(Constants.COUNTRY_ORIGIN, mCountryOriginSpinner.getSelectedItem().toString());
        inputMap.put(Constants.DEGREE, mDegreeSpinner.getSelectedItem().toString());

        if (isInputsValid(inputMap)) {
            inputMap.put(Constants.DOB, mDobTv.getText().toString());
            String genderValue = getShortcutFromFullString(mGenderSpinner.getSelectedItem().toString());
            inputMap.put(Constants.GENDER, genderValue);
            inputMap.put(Constants.STUDENT_ID, mStudentIdTv.getText().toString());
            inputMap.put(Constants.PREFFERD_NAME, mPrefferedNameEt.getText().toString());
            inputMap.put(Constants.ALT_CONTACT, mAltContactEt.getText().toString());
            inputMap.put(Constants.HSC, String.valueOf(mHscCb.isChecked()));
            inputMap.put(Constants.HSC_MARK, mHscEt.getText().toString());
            inputMap.put(Constants.IELTS, String.valueOf(mIeltsCb.isChecked()));
            inputMap.put(Constants.IELTS_MARK, mIeltsEt.getText().toString());
            inputMap.put(Constants.TOEFL, String.valueOf(mToeflcb.isChecked()));
            inputMap.put(Constants.TOEFL_MARK, mToeflEt.getText().toString());
            inputMap.put(Constants.TAFE, String.valueOf(mTafecb.isChecked()));
            inputMap.put(Constants.TAFE_MARK, mTafeEt.getText().toString());
            inputMap.put(Constants.CULT, String.valueOf(mCultcb.isChecked()));
            inputMap.put(Constants.CULT_MARK, mCultEt.getText().toString());
            inputMap.put(Constants.INSEARCH_DEEP, String.valueOf(mInsearchDeepCb.isChecked()));
            inputMap.put(Constants.INSEARCH_DEEP_MARK, mInsearchDeepEt.getText().toString());
            inputMap.put(Constants.INSEARCH_DIP, String.valueOf(mInsearchDipCb.isChecked()));
            inputMap.put(Constants.INSEARCH_DIP_MARK, mInsearchDipEt.getText().toString());
            inputMap.put(Constants.FOUNDATION_COURSE, String.valueOf(mFoundationCourseCb.isChecked()));
            inputMap.put(Constants.FOUNDATION_MARK, mFoundationCourseEt.getText().toString());

            inputMap = trimValue(inputMap);

            return inputMap;
        }
        return null;
    }

    /**
     * trims the string in the input map
     * @param map the map to trim
     * @return the map that is trimmed
     */
    private Map<String, String> trimValue(Map<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            entry.setValue(entry.getValue().trim());
        }
        return map;
    }

    /**
     * checks if any of the value in the inputMap is empty
     * @param inputMap the map to perform checking
     * @return the true if everything is filled
     */
    private boolean isInputsValid(Map<String, String> inputMap) {
        boolean valid = true;
        for (Map.Entry<String, String> entry : inputMap.entrySet()) {
            if (entry.getValue().equals(Constants.EMPTY_SPINNER)) {
                setSpinnerError(entry.getKey());
                valid = false;
            }
        }
        return valid;
    }


    /**
     * set error on spinner
     * @param spinnerName the name of the spinner to use
     */
    private void setSpinnerError(String spinnerName) {
        Spinner spinner = null;
        switch (spinnerName) {
            case Constants.STATUS:
                spinner = mStatusSpinner;
                break;
            case Constants.FIRST_LANGUAGE:
                spinner = mFirstLanguageSpinner;
                break;
            case Constants.COUNTRY_ORIGIN:
                spinner = mCountryOriginSpinner;
                break;
            case Constants.DEGREE:
                spinner = mDegreeSpinner;
                break;
        }
        if (spinner != null) {
            spinner.requestFocusFromTouch();

            //make textview using the empty selected field and set error to it
            TextView errorText = (TextView) spinner.getSelectedView();
            errorText.setError(Constants.CHOOSE_ONE);
            errorText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));//just to highlight that this is an error
            errorText.setText(getString(R.string.required_field));
            errorText.scrollTo(0, errorText.getTop());
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        buttonView.setFocusable(true);
        buttonView.requestFocus();
        switch (buttonView.getId()) {
            case R.id.activity_register_hsc_checkbox: {
                toggleEditTextVisibility(isChecked, mHscEt, mHscTil);
                break;
            }
            case R.id.activity_register_ielts_checkbox: {
                toggleEditTextVisibility(isChecked, mIeltsEt, mIeltsTil);
                break;
            }
            case R.id.activity_register_toefl_checkbox: {
                toggleEditTextVisibility(isChecked, mToeflEt, mToeflTil);
                break;
            }
            case R.id.activity_register_tafe_checkbox: {
                toggleEditTextVisibility(isChecked, mTafeEt, mTafeTil);
                break;
            }
            case R.id.activity_register_cult_checkbox: {
                toggleEditTextVisibility(isChecked, mCultEt, mCultTil);
                break;
            }
            case R.id.activity_register_insearch_deep_checkbox: {
                toggleEditTextVisibility(isChecked, mInsearchDeepEt, mInsearchDeepTil);
                break;
            }
            case R.id.activity_register_insearch_dip_checkbox: {
                toggleEditTextVisibility(isChecked, mInsearchDipEt, mInsearchDipTil);
                break;
            }
            case R.id.activity_register_foundation_course_checkbox: {
                toggleEditTextVisibility(isChecked, mFoundationCourseEt, mFoundationCourseTil);
                break;
            }
        }
    }

    private void toggleEditTextVisibility(boolean isChecked, EditText et, TextInputLayout til) {
        if (isChecked) {
            et.setVisibility(View.VISIBLE);
            til.setVisibility(View.VISIBLE);
        } else {
            et.setVisibility(View.GONE);
            til.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        v.requestFocus();
        v.requestFocusFromTouch();
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void setValueOnSpinner(Spinner spinner, String value) {
        if (spinner != null && value != null) {
            value = value.trim();
            for (int i = 0; i < spinner.getCount(); i++) {
                if (spinner.getItemAtPosition(i).toString().equals(value)) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }
    }

    private void setEducationAndMark(Boolean hasThisEducation, CheckBox educationCB, String mark, TextView markTv) {
        if (hasThisEducation) {
            educationCB.setChecked(true);
            markTv.setText(mark);
        }
    }

    private String getFullStringFromShortcut(String shortcut) {
        String fullString = "";
        shortcut = shortcut.trim();
        switch (shortcut) {
            case Constants.M:
                fullString = Constants.MALE;
                break;
            case Constants.F:
                fullString = Constants.FEMALE;
                break;
            case Constants.X:
                fullString = Constants.INDETERMINATE_UNSPECIFIED_INTERSEX;
                break;
        }
        return fullString;
    }

    private String getShortcutFromFullString(String fullString) {
        String shortcut = "";
        switch (fullString) {
            case Constants.MALE:
                shortcut = Constants.M;
                break;
            case Constants.FEMALE:
                shortcut = Constants.F;
                break;
            case Constants.INDETERMINATE_UNSPECIFIED_INTERSEX:
                shortcut = Constants.X;
        }
        return shortcut;
    }
}
