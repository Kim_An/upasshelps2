package com.sdpgroup6.helps.Controller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.WorkshopSet;
import com.sdpgroup6.helps.R;

import java.util.List;

/**
 * Created by kiman on 2/10/2016.
 */

public class TabbedWorkshopSetAdapter extends RecyclerView.Adapter<TabbedWorkshopSetAdapter.TabbedWorkshopSetViewHolder>{

    private Context mContext;
    private List<WorkshopSet> mWorkshopSetList;

    public TabbedWorkshopSetAdapter(Context mContext, List<WorkshopSet> mWorkshopSetList) {
        this.mContext = mContext;
        this.mWorkshopSetList = mWorkshopSetList;
    }

    @Override
    public TabbedWorkshopSetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_workshop_set, parent, false);
        return new TabbedWorkshopSetViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(TabbedWorkshopSetViewHolder holder, int position) {
        WorkshopSet workshopSet = mWorkshopSetList.get(position);
        holder.mWorkshopSetNameTv.setText(workshopSet.getName());
    }

    @Override
    public int getItemCount() {
        return mWorkshopSetList.size();
    }


    public class TabbedWorkshopSetViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mWorkshopSetNameTv;
        private LinearLayout mWorkshopSetLayout;

        public TabbedWorkshopSetViewHolder(View itemView) {
            super(itemView);
            mWorkshopSetNameTv = (TextView) itemView.findViewById(R.id.adapter_workshop_set_name_textview);
            mWorkshopSetLayout = (LinearLayout) itemView.findViewById(R.id.adapter_workshop_set_name_textview_layout);
            mWorkshopSetLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int selectedPos = getAdapterPosition();
            WorkshopSet workshopSet = mWorkshopSetList.get(selectedPos);
            Intent intent = new Intent(mContext, TabbedWorkshopSetWorkshopListActivity.class);
            intent.putExtra(Constants.WORKSHOPSET_ID, String.valueOf(workshopSet.getId()));
            intent.putExtra(Constants.WORKSHOPSET_NAME, String.valueOf(workshopSet.getName()));
            Log.i(Constants.LOG_TAG, "Workshop set ID: " + String.valueOf(workshopSet.getId()) + "\n"
                    + "workshop set name: " + workshopSet.getName());
            mContext.startActivity(intent);
        }
    }
}
