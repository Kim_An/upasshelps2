package com.sdpgroup6.helps.Controller;

import android.accounts.NetworkErrorException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.sdpgroup6.helps.Model.CheckStudentResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.MobileRESTClient;

/**
 * Is the class for login page activity
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //References UI
    private ProgressBar mProgressBar;
    private EditText mStudentIdET;
    private EditText mPasswordET;
    private String mStudentId;
    private String mPassword;
    private Button mLoginButton;
    private TextInputLayout mUsernameTIL;
    private TextInputLayout mPasswordTIL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //hide soft keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        ScrollView rootView = (ScrollView) findViewById(R.id.activity_login_root_view);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);

        mProgressBar = (ProgressBar) findViewById((R.id.activity_main_progessbar));
        mStudentIdET = (EditText) findViewById(R.id.activity_main_student_id_edittext);
        mPasswordET = (EditText) findViewById(R.id.activity_main_password_edittext);
        mUsernameTIL = (TextInputLayout) findViewById(R.id.activity_login_username_input_layout);
        mPasswordTIL = (TextInputLayout) findViewById(R.id.activity_login_password_input_layout);
        mLoginButton = (Button) findViewById(R.id.activity_main_login_button);

        mProgressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        addTextWatcher();
        mLoginButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.activity_main_login_button: {
                mStudentId = mStudentIdET.getText().toString();
                mPassword = mPasswordET.getText().toString();
                login(mStudentId, mPassword);
                break;
            }
        }
    }

    //sends a request to sever to check if student exists
    private void login(String id, String password) {
        if (!isFieldEmpty(id, password)) {
            id = id.trim();

            if (isStudentId8Digit(id)) {
                mProgressBar.setVisibility(View.VISIBLE);
                try {
                    MobileRESTClient.getInstance(LoginActivity.this).
                            getStudentController().checkStudent(id, password, responseHandle);
                } catch (NetworkErrorException e) {
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    Utilities.displayError(LoginActivity.this, e.getMessage());
                }
            }
        }
    }

    RestResponseHandler responseHandle = new RestResponseHandler() {
        @Override
        public void onRestResponse(RequestResponse requestResponse) {
            CheckStudentResponse response = (CheckStudentResponse) requestResponse;

            mProgressBar.setVisibility(View.GONE);
            if (response.isExists()) {
                startActivity(ActivityManager.login(getApplicationContext(), LoginActivity.this, mStudentId));
                finish();
            } else {
                Toast.makeText(LoginActivity.this, getString(R.string.welcome_new_user), Toast.LENGTH_SHORT).show();
                Intent intent = ActivityManager.intentToRegisterActivity(LoginActivity.this);
                intent.putExtra(Constants.STUDENT_ID, mStudentId);
                startActivity(intent);
            }
        }

        @Override
        public void onResponseFailed(String errorMsg) {
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(LoginActivity.this, getString(R.string.error) + errorMsg, Toast.LENGTH_SHORT).show();

        }
    };

    /**
     * To keep device in portrait mode only
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    /**
     * Check if the provided parameters are empty, if it is set error text into the Edittext accordingly
     *
     * @param studentId
     * @param password
     * @return
     */
    private boolean isFieldEmpty(String studentId, String password) {
        if (studentId.isEmpty() || password.isEmpty()) {
            if (studentId.isEmpty()) {
                mUsernameTIL.setError(getString(R.string.required_field));
            }
            if (password.isEmpty()) {
                mPasswordTIL.setError(getString(R.string.required_field));
            }
            return true;
        }
        return false;
    }

    /**
     * Take user to home screen if they pressed back on login screen
     */
    @Override
    public void onBackPressed() {
        Intent homeIntent = ActivityManager.exitAppIntent();

        startActivity(homeIntent);
    }

    private boolean isStudentId8Digit(String studentId) {
        if (studentId.length() != Constants.VALID_STUDENT_ID_DIGIT) {
            mUsernameTIL.setError(getString(R.string.invalid_student_id_digit));
            return false;
        }
        return true;
    }

    private void addTextWatcher() {
        mStudentIdET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mUsernameTIL.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPasswordET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPasswordTIL.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
