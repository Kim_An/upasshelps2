package com.sdpgroup6.helps.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.sdpgroup6.helps.ImageDecoder;
import com.sdpgroup6.helps.Model.AppPreferences;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * takes care of interaction and view of the make booking activity
 */
public class TabbedMakeBookingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private DrawerLayout mDrawer;
    private static boolean sShouldGetProgram = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_make_bookings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_tabbed_make_booking_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ImageDecoder.customizeUpButton(getApplicationContext(), actionBar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        CoordinatorLayout rootView = (CoordinatorLayout) findViewById(R.id.activity_tabbed_make_boooking_rootview);
        ActivityManager.setBackgroundImage(getResources(), rootView, R.drawable.stair_background);

        setUpNavigationDrawer();

        mViewPager = (ViewPager) findViewById(R.id.activity_tabbed_make_booking_view_pager);
        setUpViewPager();

        mTabLayout = (TabLayout) findViewById(R.id.activity_tabbed_make_booking_tablayout);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onStart() {
        AppPreferences.getInstance(getApplicationContext()).loadStudentId();
        super.onStart();
    }

    /**
     * sets up tabs to the tab layout
     */
    private void setUpViewPager() {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        final TabbedProgramListFragment programFragment = new TabbedProgramListFragment();

        //add tabs to the adapter
        adapter.addFragment(new TabbedWorkshopListFragment(), Constants.WORKSHOP, null);
        adapter.addFragment(programFragment, Constants.PROGRAM, null);

        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.i("page", "onPageScrolled called \nposition: " + position + ", posOffset: " + positionOffset);
                if (position == 1) {
                    setBufferToGetProgram(programFragment);
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.i("page", "onPageSelected + pos: " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.i("page", "onPageStateChanged + state: " + state);
            }
        });
    }

    /**
     * get program only if 10 seconds has passed after the previous call to get program
     * @param programFragment
     */
    private void setBufferToGetProgram(TabbedProgramListFragment programFragment){
        int delayDuration = 10000; //10 seconds
        if (sShouldGetProgram) {
            programFragment.getProgram();
            sShouldGetProgram = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sShouldGetProgram = true;
                }
            }, delayDuration);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = ActivityManager.getSwitchActivities(this, item.getItemId());

        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }

    /**
     * set up navigation drawer
     * set student id on the nav header, and the profile picture
     */
    private void setUpNavigationDrawer() {
        mDrawer = (DrawerLayout) findViewById(R.id.nav_make_bookings_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_make_bookings_nav_view);

        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);

            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setCheckedItem(R.id.nav_make_booking_item);

            Utilities.setStudentIdOnNav(getApplicationContext(), header, getString(R.string.student_email_suffix));

            ImageView profilePicture = (ImageView) header.findViewById(R.id.nav_header_profile_picture_imageview);
            profilePicture.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.nav_header_profile_picture_imageview) {
            startActivity(ActivityManager.intentToRegisterActivity(this));
        }
    }

    @Override
    protected void onResume() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_make_bookings_nav_view);
        if (navigationView != null) {
            navigationView.setCheckedItem(R.id.nav_make_booking_item);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        super.onPause();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title, @Nullable Bundle bundle) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }
}
