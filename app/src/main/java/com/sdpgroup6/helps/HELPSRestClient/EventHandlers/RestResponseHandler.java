package com.sdpgroup6.helps.HELPSRestClient.EventHandlers;

import com.sdpgroup6.helps.Model.RequestResponse;

/**
 * is for calling back from asynctask class to activity
 */

public interface RestResponseHandler {
      void onRestResponse(RequestResponse requestResponse);
      void onResponseFailed(String errorMsg);
}
