package com.sdpgroup6.helps.HELPSRestClient.Controller;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.Model.ProgramResponse;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.Workshop;
import com.sdpgroup6.helps.Model.WorkshopResponse;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestProgramResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.Rest.RestClient;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Johnny on 20/09/2016.
 * This is the program controller, provide API class relates to Programs.
 */
public class ProgramController {
    private final RestClient mRestClient;
    // caches the last programWorkshops, so if the user wants to book them,
    // the controller does not need to call programworkshops again to obtain all workshops
    private ProgramCache lastProgramCache = new ProgramCache("PlaceHolder", null);

    public ProgramController(RestClient restClient) {
        this.mRestClient = restClient;
    }

    /**
     * returns a ProgramResponse as Data model in onRestResponse
     */
    public void loadProgram(@Nullable String programName, @Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                            @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                            @Nullable Boolean active,
                            @Nullable Integer page, @Nullable Integer pageSize,
                            RestResponseHandler eventHandler) {
        new LoadProgramTask(programName,
                startDateBegin, startDateEnd,
                endDateBegin, endDateEnd,
                active,
                page, pageSize,
                eventHandler).execute(mRestClient);
    }

    /**
     * note the it returns WorkshopResponse as Data Model in onRestResponse
     */
    public void getProgramWorkshops(String programID, RestResponseHandler eventHandler) {
        new GetProgramWorkshopTask(programID, eventHandler).execute(mRestClient);
    }

    /**
     * call this to book Program
     */
    public void bookProgram(String ProgramID, final RestProgramResponseHandler eventHandler) {
        //get all workshops first
        Log.i(Utilities.RESPONSETAG, "Booking Program, ID: " + ProgramID);
        lastProgramCache.getProgramWorkshops(ProgramID, new RestResponseHandler() {
            @Override
            public void onRestResponse(RequestResponse requestResponse) {
                Log.i(Utilities.RESPONSETAG, "Obtained all workshops under booking program\n Initiating booking process:");
                WorkshopResponse workshops = (WorkshopResponse) requestResponse;
                new BookWorkshopsTask(workshops.Results, eventHandler).execute(mRestClient);
            }

            @Override
            public void onResponseFailed(String errorMsg) {
                //should never be called..
                eventHandler.onResponseFailed(errorMsg);
            }
        });
        //book them by write a new AsyncTask

    }

    private class LoadProgramTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RestResponseHandler eventHandler;
        private final String programName;
        private final String startDateBegin;
        private final String startDateEnd;
        private final String endDateBegin;
        private final String endDateEnd;
        private final Boolean active;
        private final Integer page;
        private final Integer pageSize;

        public LoadProgramTask(
                @Nullable String programName, @Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd, @Nullable Boolean active,
                @Nullable Integer page, @Nullable Integer pageSize,
                RestResponseHandler eventHandler) {
            this.programName = programName;
            this.startDateBegin = Utilities.getSQLDateString(startDateBegin);
            this.startDateEnd = Utilities.getSQLDateString(startDateEnd);
            this.endDateBegin = Utilities.getSQLDateString(endDateBegin);
            this.endDateEnd = Utilities.getSQLDateString(endDateEnd);
            this.active = active;
            this.page = page;
            this.pageSize = pageSize;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<ProgramResponse> call = client[0].getmApiService().
                        getPrograms(programName, startDateBegin, startDateEnd,
                                endDateBegin, endDateEnd, active, page, pageSize);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from load programs");
                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());

        }
    }

    private class GetProgramWorkshopTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RestResponseHandler eventHandler;
        private final String programID;

        public GetProgramWorkshopTask(
                String programID, RestResponseHandler eventHandler) {
            this.programID = programID;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<WorkshopResponse> call = client[0].getmApiService().
                        getProgramWorkshops(programID);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from GetProgramWorkshopTask");
                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());

            lastProgramCache = new ProgramCache(programID, (WorkshopResponse) response);


        }
    }


    private class BookWorkshopsTask extends AsyncTask<RestClient, Void, ArrayList<RequestResponse>> {

        private final List<Workshop> workShops;
        private final RestProgramResponseHandler eventHandler;

        public BookWorkshopsTask(List<Workshop> workShops, RestProgramResponseHandler eventHandler) {

            this.workShops = workShops;
            this.eventHandler = eventHandler;
        }

        @Override
        protected ArrayList<RequestResponse> doInBackground(RestClient... client) {
            //make many API calls,
            //if anyone failed,
            //cancel all of made the bookings
            RequestResponse response = null;
            ArrayList<RequestResponse> responses = new ArrayList<>();
            Call<BookWorkshopResponse> call = null;
            ArrayList<Workshop> tobeBooked = new ArrayList<>();
            for (Workshop check : workShops) {
                try {
                    if (Utilities.getAppTime().compareTo(check.getStartDate()) <= 0) {   //book only workshops that start after today
                        tobeBooked.add(check);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.e(Utilities.ERRORTAG, "Error at parsing Time String to Calander during booking program workshops. Failed workshopID: "
                            + check.getWorkshopId());
                    // can not through an error to the user's face atm
                    continue;
                }
            }
            Log.i(Utilities.RESPONSETAG, "Start booking " + tobeBooked.size() + " out of "
                    + workShops.size() + " workshops!");

            for (int i = 0; i < tobeBooked.size(); i++) {
                //find the next workshop that is on

                try {
                    call = client[0].getmApiService().
                            bookWorkshop(String.valueOf(tobeBooked.get(i).getWorkshopId()),
                                    LoginSession.studentId
                                    , LoginSession.studentId);
                    response = call.execute().body();
                    responses.add(response);//add this response
                    Log.i(Utilities.RESPONSETAG, "Book Program Workshop ID: " + tobeBooked.get(i).getWorkshopId() +
                            "\tSuccess: " + response.isSuccess() +
                            "\tDisplayMessage: " + response.getDisplayMessage());
                    if (!response.isSuccess())//roll back process start
                    {
                        Log.e(Utilities.ERRORTAG, "Failed at Booking all workshops under this Program\n" +
                                "Entering Rollback procedure");
                        Call<RequestResponse> cancelCall = null;
                        //set the error message of the failed one.
                        responses.clear();
                        responses.add(response);
                        response.setDisplayMessage("Error at booking Program, Failed WorkshopID: " +
                                tobeBooked.get(i).getWorkshopId() + "\n" + response.getDisplayMessage());
                        for (int j = i - 1; j >= 0; j--)//cancel everything before i
                        {
                            cancelCall = client[0].getmApiService().
                                    cancelWorkshop(String.valueOf(tobeBooked.get(j).getWorkshopId()),
                                            LoginSession.studentId
                                            , LoginSession.studentId);
                            response = cancelCall.execute().body();
                            Log.i(Utilities.RESPONSETAG, "got a response from CancelWorkshop During Rollback\n" +
                                    "Success: " + response.isSuccess() +
                                    "\nDisplayMessage: " + response.getDisplayMessage());
                            if (!response.isSuccess()) {
                                //failed cancel ?
                                Log.e(Utilities.ERRORTAG, "Fetal Error at cancelling workshopID: " + tobeBooked.get(j).getWorkshopId() +
                                        " during rollback, Please contact Admin and provide the WorkshopID\n" +
                                        "Continuing cancelling others");
                            }
                        }
                        Log.i(Utilities.ERRORTAG, "Rollback Finish");
                        break;//stop the booking loop
                    }
                    //what if the server is done?
                    //what if the AppKey is wrong?
                    //what if the database is done?

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
                }

            }
            return responses;
        }

        @Override
        protected void onPostExecute(ArrayList<RequestResponse> responses) {
            if (responses == null || responses.size() < 1) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            // the last one must success.
            if (responses.get(responses.size() - 1).isSuccess()) {
                Log.i(Utilities.RESPONSETAG, "Successfully booked all workshops!");
                eventHandler.onRestResponse(responses);
            } else
                eventHandler.onResponseFailed(responses.get(responses.size() - 1).getDisplayMessage());


        }
    }

    /**
     * cache the last response from getProgramWorkshops
     */
    private class ProgramCache {
        private String lastProgramID;
        private WorkshopResponse cachedWorkshops;

        public ProgramCache(String lastProgramID, WorkshopResponse cachedWorkshops) {
            this.lastProgramID = lastProgramID;
            this.cachedWorkshops = cachedWorkshops;
        }

        public void getProgramWorkshops(String programID, RestResponseHandler programHandler) {
            if (lastProgramID.equals(programID) && cachedWorkshops != null)
                programHandler.onRestResponse(cachedWorkshops);
            else
                new GetProgramWorkshopTask(programID, programHandler).execute(mRestClient);

        }
    }
}
