package com.sdpgroup6.helps.HELPSRestClient;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.HELPSRestClient.Controller.ProgramController;
import com.sdpgroup6.helps.HELPSRestClient.Controller.SessionController;
import com.sdpgroup6.helps.HELPSRestClient.Controller.WorkshopController;
import com.sdpgroup6.helps.HELPSRestClient.Rest.RestClient;
import com.sdpgroup6.helps.HELPSRestClient.Controller.StudentController;

/**
 * Created by Johnny on 13/09/2016.
 * This class implemented singleton pattern,
 * The class returns a REST Client, only if the network is avaiable.
 * The client provide different controllers which is used to make API calls
 */

public class MobileRESTClient {
    private static MobileRESTClient theClient = new MobileRESTClient();

    //Return an MobileRestClient Instance, Only give access if network is available.
    public static MobileRESTClient getInstance(Context context) throws NetworkErrorException {
        if (isNetworkAvailable(context))
            return theClient;
        else {
            throw new NetworkErrorException(context.getString(R.string.network_unavailable));
        }
    }

    private static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    private MobileRESTClient() {
    }

    private final RestClient mRestCLient = new RestClient();
    private final StudentController mStudentController = new StudentController(this.mRestCLient);
    private final SessionController mSessionController = new SessionController(this.mRestCLient);
    private final WorkshopController mWorkshopController = new WorkshopController(this.mRestCLient);
    private final ProgramController mProgramController = new ProgramController(this.mRestCLient);

    public WorkshopController getWorkshopController() {
        return mWorkshopController;
    }

    public StudentController getStudentController() {
        return mStudentController;
    }

    public ProgramController getProgramController() {
        return mProgramController;
    }

    public SessionController getmSessionController() {
        return mSessionController;
    }
}
