package com.sdpgroup6.helps.HELPSRestClient.Controller;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.BookedWorkshopResponse;
import com.sdpgroup6.helps.Model.CampusListResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.WaitingCountResponse;
import com.sdpgroup6.helps.Model.WorkshopResponse;
import com.sdpgroup6.helps.Model.WorkshopSetResponse;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.Rest.RestClient;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;

/**
 * Created by Johnny on 13/09/2016.
 * ALL Nullable Values allow NULL to be placed, if NULL is placed, then the result
 * will NOT consider this filter at all.
 * if yuu use value instead of NULL, then the result will be filtered by your filter.
 */

public class WorkshopController {

    private final RestClient mRestClient;

    public WorkshopController(RestClient restClient) {
        //constructor
        this.mRestClient = restClient;
    }

    /**
     * note, StartDateBegin/StartDateEnd is a pair, and same for endDateBegin/endDateEnd
     */
    public void loadWorkshop(String workShopSetID, @Nullable String topic,
                             @Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                             @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                             @Nullable String compus, @Nullable Boolean active,
                             @Nullable Integer page, @Nullable Integer pageSize,
                             RestResponseHandler eventHandler) {


        new LoadWorkshopTask(workShopSetID, topic == null ? topic : topic.trim(),
                startDateBegin, startDateEnd,
                endDateBegin, endDateEnd,
                compus, active,
                page, pageSize,
                eventHandler).execute(mRestClient);
    }

    /**
     * book a particular workshop
     */
    public void bookWorkshop(String workShopID, RestResponseHandler onResponseHandler) {
        new BookWorkshopTask(workShopID, onResponseHandler).execute(mRestClient);
    }

    /**
     * count the number of waitings in a workshop
     */
    public void CountWorkshopWaiting(String workShopID, RestResponseHandler onResponseHandler) {
        new CountWorkshopWaitingTask(workShopID, onResponseHandler).execute(mRestClient);
    }

    /**
     * return all campus locations (only the buildings)
     */
    public void listCampusLocation(RestResponseHandler onResponseHandler) {
        new ListCampusTask(onResponseHandler).execute(mRestClient);
    }

    /**
     * waitlist a workshop
     */
    public void waitWorkshop(String workShopID, @Nullable Boolean priority, RestResponseHandler onResponseHandler) {
        new WaitWorkshopTask(workShopID, priority, onResponseHandler).execute(mRestClient);
    }

    /**
     * return all workshop sets
     */
    public void listWorkshopSet(@Nullable Boolean active, RestResponseHandler onResponseHandler) {
        new ListWorkshopSetTask(active, onResponseHandler).execute(mRestClient);
    }

    /**
     * return bookings of a student
     */
    public void getWorkshopBooking(@Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                                   @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                                   @Nullable Integer compusId, @Nullable Boolean active,
                                   @Nullable Integer page, @Nullable Integer pageSize,
                                   RestResponseHandler eventHandler) {
        new GetWorkshopBookingTask(startDateBegin, startDateEnd,
                endDateBegin, endDateEnd,
                compusId, active,
                page, pageSize,
                eventHandler).execute(mRestClient);
    }

    /**
     * cancel a workshop boooking
     */
    public void cancelBookedWorkshop(String workshopId, Calendar startTime, RestResponseHandler onResponseHandler) {

        if (Utilities.isLessThanTwoHours(startTime)) {
            onResponseHandler.onResponseFailed("Can not cancel workshop 2 hours before starting!");
            return;
        }
        new CancelWorkshopTask(workshopId, onResponseHandler).execute(mRestClient);
    }


    private class LoadWorkshopTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String workShopSetID;
        private final String topic;
        private final RestResponseHandler eventHandler;
        private final String startDateBegin;
        private final String startDateEnd;
        private final String endDateBegin;
        private final String endDateEnd;
        private final String compus;
        private final Boolean active;
        private final Integer page;
        private final Integer pageSize;

        public LoadWorkshopTask(String workShopSetID, @Nullable String topic,
                                @Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                                @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                                @Nullable String compus, @Nullable Boolean active,
                                @Nullable Integer page, @Nullable Integer pageSize,
                                RestResponseHandler eventHandler) {
            this.startDateBegin = Utilities.getSQLDateString(startDateBegin);
            this.startDateEnd = Utilities.getSQLDateString(startDateEnd);
            this.endDateBegin = Utilities.getSQLDateString(endDateBegin);
            this.endDateEnd = Utilities.getSQLDateString(endDateEnd);
            this.compus = compus;
            this.topic = topic;
            this.active = active;
            this.page = page;
            this.pageSize = pageSize;

            this.workShopSetID = workShopSetID;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<WorkshopResponse> call = client[0].getmApiService().
                        getWorkshops(workShopSetID, topic, startDateBegin, startDateEnd,
                                endDateBegin, endDateEnd, compus, active, page, pageSize);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from getWorkshops");
                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());

        }
    }

    private class BookWorkshopTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String workShopID;
        private final RestResponseHandler eventHandler;

        public BookWorkshopTask(String workShopID, RestResponseHandler eventHandler) {

            this.workShopID = workShopID;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<BookWorkshopResponse> call = client[0].getmApiService().bookWorkshop(workShopID,
                        LoginSession.studentId
                        , LoginSession.studentId);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from BookWorkshop\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());

                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }

    private class ListCampusTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RestResponseHandler eventHandler;

        public ListCampusTask(RestResponseHandler eventHandler) {

            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<CampusListResponse> call = client[0].getmApiService().campusList();
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from CampusList\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());

                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }

    private class CountWorkshopWaitingTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String workShopID;
        private final RestResponseHandler eventHandler;

        public CountWorkshopWaitingTask(String workShopID, RestResponseHandler eventHandler) {

            this.workShopID = workShopID;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<WaitingCountResponse> call = client[0].getmApiService().countWorkshopWaiting(workShopID);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from Count Workshop\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());

                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }

    private class CancelWorkshopTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String workShopID;
        private final RestResponseHandler eventHandler;

        public CancelWorkshopTask(String workShopID, RestResponseHandler eventHandler) {

            this.workShopID = workShopID;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<RequestResponse> call = client[0].getmApiService().cancelWorkshop(workShopID,
                        LoginSession.studentId
                        , LoginSession.studentId);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from CancelWorkshop\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());

                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }

    private class WaitWorkshopTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String workShopID;
        private final RestResponseHandler eventHandler;
        private final Boolean priority;

        public WaitWorkshopTask(String workShopID, @Nullable Boolean priority, RestResponseHandler eventHandler) {

            this.priority = priority;
            this.workShopID = workShopID;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<RequestResponse> call = client[0].getmApiService().waitWorkshop(workShopID,
                        LoginSession.studentId
                        , LoginSession.studentId, priority
                );
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from WaitWorkshop\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());

                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }

    private class ListWorkshopSetTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RestResponseHandler eventHandler;
        private final Boolean active;

        public ListWorkshopSetTask(@Nullable Boolean active, RestResponseHandler eventHandler) {
            this.active = active;
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<WorkshopSetResponse> call = client[0].getmApiService().getWorkshopSets(active);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from WorkshopSets\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());

                //what if the server is down?
                //what if the AppKey is wrong?
                //what if the database is down?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }

    private class GetWorkshopBookingTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RestResponseHandler eventHandler;
        private final String startDateBegin;
        private final String startDateEnd;
        private final String endDateBegin;
        private final String endDateEnd;
        private final Integer compusId;
        private final Boolean active;
        private final Integer page;
        private final Integer pageSize;

        public GetWorkshopBookingTask(@Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                                      @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                                      @Nullable Integer compusId, @Nullable Boolean active,
                                      @Nullable Integer page, @Nullable Integer pageSize,
                                      RestResponseHandler eventHandler) {
            this.startDateBegin = Utilities.getSQLDateString(startDateBegin);
            this.startDateEnd = Utilities.getSQLDateString(startDateEnd);
            this.endDateBegin = Utilities.getSQLDateString(endDateBegin);
            this.endDateEnd = Utilities.getSQLDateString(endDateEnd);
            this.compusId = compusId;
            this.active = active;
            this.page = page;
            this.pageSize = pageSize;
            Log.i(Utilities.RESPONSETAG, "Start Date: " + this.startDateBegin +
                    " End Date: " + this.startDateEnd);
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<BookedWorkshopResponse> call = client[0].getmApiService().
                        getBookedWorkshops(LoginSession.studentId,
                                startDateBegin, startDateEnd, endDateBegin, endDateEnd,
                                compusId, active, page, pageSize);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from getWorkshopBooking\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());
                //what if the server is down?
                //what if the AppKey is wrong?
                //what if the database is down?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }
}
