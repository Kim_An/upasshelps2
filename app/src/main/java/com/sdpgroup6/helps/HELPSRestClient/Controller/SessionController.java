package com.sdpgroup6.helps.HELPSRestClient.Controller;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.LoginSession;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.SessionBookingResponse;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;
import com.sdpgroup6.helps.HELPSRestClient.Rest.RestClient;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;

/**
 * Created by Johnny on 13/09/2016.
 * ALL Nullable Values allow NULL to be placed, if NULL is placed, then the result
 * will NOT consider this filter at all.
 * if you use value instead of NULL, then the result will be filtered by your filter.
 */

public class SessionController {

    private final RestClient mRestClient;

    public SessionController(RestClient restClient) {
        //constructor
        this.mRestClient = restClient;
    }
//returns all booked sessions
    public void getSessionBooking(@Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                                  @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                                  @Nullable String campus, @Nullable Integer lecturerID,
                                  @Nullable Integer sessionTypeID, @Nullable Boolean active,
                                  @Nullable Integer page, @Nullable Integer pageSize,
                                  RestResponseHandler eventHandler) {
        new GetSessionBookingTask( startDateBegin, startDateEnd,
                endDateBegin, endDateEnd,
                campus,lecturerID,sessionTypeID, active,
                page, pageSize,
                eventHandler).execute(mRestClient);
    }

    private class GetSessionBookingTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RestResponseHandler eventHandler;
        private final String startDateBegin;
        private final String startDateEnd;
        private final String endDateBegin;
        private final String endDateEnd;
        private final String compus;
        private final Integer lectuerID;
        private  final Integer sessionTypeID;
        private final Boolean active;
        private final Integer page;
        private final Integer pageSize;

        public GetSessionBookingTask(@Nullable Calendar startDateBegin, @Nullable Calendar startDateEnd,
                                      @Nullable Calendar endDateBegin, @Nullable Calendar endDateEnd,
                                     @Nullable String campus, @Nullable Integer lecturerID,
                                     @Nullable Integer sessionTypeID, @Nullable Boolean active,
                                      @Nullable Integer page, @Nullable Integer pageSize,
                                      RestResponseHandler eventHandler) {
            this.startDateBegin = Utilities.getSQLDateString(startDateBegin);
            this.startDateEnd = Utilities.getSQLDateString(startDateEnd);
            this.endDateBegin = Utilities.getSQLDateString(endDateBegin);
            this.endDateEnd = Utilities.getSQLDateString(endDateEnd);
            this.compus = campus;
            this.lectuerID = lecturerID;
            this.sessionTypeID = sessionTypeID;
            this.active = active;
            this.page = page;
            this.pageSize = pageSize;
            Log.i(Utilities.RESPONSETAG, "Start Date: " + this.startDateBegin +
                    " End Date: " + this.startDateEnd);
            this.eventHandler = eventHandler;
        }

        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<SessionBookingResponse> call = client[0].getmApiService().
                        getBookedSessions(LoginSession.studentId,
                                startDateBegin, startDateEnd, endDateBegin, endDateEnd,
                                compus,lectuerID,sessionTypeID,active, page, pageSize);
                response = call.execute().body();
                 Log.i(Utilities.RESPONSETAG, "got a response from getSessionBooking\n" +
                        "Success: " + response.isSuccess() +
                        "\nDisplayMessage: " + response.getDisplayMessage());
                //what if the server is down?
                //what if the AppKey is wrong?
                //what if the database is down?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());


        }
    }
}
