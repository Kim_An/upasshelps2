package com.sdpgroup6.helps.HELPSRestClient.EventHandlers;

import com.sdpgroup6.helps.Model.RequestResponse;

import java.util.ArrayList;

/**
 * is for calling back from asynctask class to activity
 */
public interface  RestProgramResponseHandler {
      void onRestResponse(ArrayList<RequestResponse> requestResponse);
      void onResponseFailed(String errorMsg);

}
