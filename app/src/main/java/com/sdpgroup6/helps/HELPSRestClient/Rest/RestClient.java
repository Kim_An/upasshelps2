package com.sdpgroup6.helps.HELPSRestClient.Rest;

import com.sdpgroup6.helps.Model.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class is the REST client that makes the connection to the REST API server
 * It has a interceptor for the AppKey and content-type
 */
public class RestClient {
    //API services
    private final APIService mApiService;

    public RestClient() {

        //start building a HttpClient
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //This client is able to intercept request
        httpClient.addInterceptor(new Interceptor() {
            @Override
            //Using Interceptor Filter Pattern , Yeah another pattern,, LOL
            // learn here http://www.tutorialspoint.com/design_pattern/intercepting_filter_pattern.htm
            public Response intercept(Interceptor.Chain chain) throws IOException {

                //get the request
                Request original = chain.request();
                // Add headers from the original request( which got from
                Request request = original.newBuilder()
                        .header(Constants.APP_KEY, Constants.APP_KEY_VALUE)
                        .header(Constants.CONTENT_TYPE, Constants.CONTENT_TYPE_VALUE)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        mApiService = retrofit.create(APIService.class);
    }

    public APIService getmApiService() {
        return mApiService;
    }
}
