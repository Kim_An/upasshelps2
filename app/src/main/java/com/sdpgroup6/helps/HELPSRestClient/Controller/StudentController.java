package com.sdpgroup6.helps.HELPSRestClient.Controller;

import android.os.AsyncTask;
import android.util.Log;

import com.sdpgroup6.helps.Model.CheckStudentResponse;
import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.GetStudentResponse;
import com.sdpgroup6.helps.Model.RegisterRequest;
import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Utilities;
import com.sdpgroup6.helps.HELPSRestClient.Rest.RestClient;
import com.sdpgroup6.helps.HELPSRestClient.EventHandlers.RestResponseHandler;

import java.io.IOException;

import retrofit2.Call;

/**
 * Created by Johnny on 13/09/2016.
 */

public class StudentController {


    private final RestClient mRestClient;

    public StudentController(RestClient mRestClient) {
        //constructor
        this.mRestClient = mRestClient;
    }

    /**
     * checks whether the student exists
     */
    public void checkStudent(String studentId, String password, RestResponseHandler onResponseHandler) {

        new CheckStudentTask(studentId, password, onResponseHandler).execute(mRestClient);
    }

    /**
     * return the student details
     */
    public void getStudent(String studentId, String password, RestResponseHandler onResponseHandler) {

        new GetStudentTask(studentId, password, onResponseHandler).execute(mRestClient);
    }

    /**
     * register a new student
     */
    public void registerStudent(RegisterRequest registerRequest, RestResponseHandler onResponseHandler) {

        new StudentRegisterTask(registerRequest, onResponseHandler).execute(mRestClient);
    }

    private class CheckStudentTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String studentId;
        private final String password;
        private final RestResponseHandler eventHandler;

        public CheckStudentTask(String studentId, String password, RestResponseHandler eventHandler) {
            this.studentId = studentId;
            this.password = password;
            this.eventHandler = eventHandler;
        }

        /**
         * use the rest client to check for student existence, and store in a boolean variable
         *
         * @return
         * @params voids
         */
        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<CheckStudentResponse> call = client[0].getmApiService().isStudentExists(studentId);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from check student exist");
                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        /**
         * this is performed when the doInBackground is finished
         * if the student exists, take them to home screen
         * else take them to the register screen
         *
         * @param response
         */
        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed("Did not receive a valid Response at all," +
                        "this may due to server down, database down. The error may also " +
                        "cause by parsing from JSON to JAVA");
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());

        }
    }

    private class GetStudentTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final String studentId;
        private final String password;
        private final RestResponseHandler eventHandler;

        public GetStudentTask(String studentId, String password, RestResponseHandler eventHandler) {
            this.studentId = studentId;
            this.password = password;
            this.eventHandler = eventHandler;
        }

        /**
         * use the rest client to check for student existence, and store in a boolean variable
         *
         * @return
         * @params voids
         */
        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            RequestResponse response = null;
            try {
                Call<GetStudentResponse> call = client[0].getmApiService().getStudent(studentId);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from check student exist");
                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        /**
         * this is performed when the doInBackground is finished
         * if the student exists, take them to home screen
         * else take them to the register screen
         *
         * @param response
         */
        @Override
        protected void onPostExecute(RequestResponse response) {
            if (response == null) {
                eventHandler.onResponseFailed(Constants.GENERIC_ERROR);
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());

        }
    }

    private class StudentRegisterTask extends AsyncTask<RestClient, Void, RequestResponse> {

        private final RegisterRequest registerRequest;
        private final RestResponseHandler eventHandler;

        public StudentRegisterTask(RegisterRequest registerRequest, RestResponseHandler eventHandler) {
            this.registerRequest = registerRequest;
            this.eventHandler = eventHandler;
        }

        /**
         * use the rest client to check for student existence, and store in a boolean variable
         *
         * @return
         * @params voids
         */
        @Override
        protected RequestResponse doInBackground(RestClient... client) {
            Boolean isExist = false;
            RequestResponse response = null;
            try {
                //later we can optimise by taking the advanatge of polymorphism
                Call<RequestResponse> call = client[0].getmApiService().registerStudent(registerRequest);
                response = call.execute().body();
                Log.i(Utilities.RESPONSETAG, "got a response from register new Student");
                //what if the server is done?
                //what if the AppKey is wrong?
                //what if the database is done?
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(Utilities.ERRORTAG, "You did not pass a correct/valid RestClient instance when you execute the task!");
            }
            return response;
        }

        /**
         * this is performed when the doInBackground is finished
         * if the student exists, take them to home screen
         * else take them to the register screen
         *
         * @param response
         */
        @Override
        protected void onPostExecute(RequestResponse response) {
            //no real response, since this class actually use RequestResponse as the response model
            if (response == null) {
                //need change to String resource later
                eventHandler.onResponseFailed("Did not receive a valid Response at all," +
                        "this may due to server down, database down. The error may also " +
                        "cause by parsing from JSON to JAVA OR invalid Request!!!");
                return;
            }
            if (response.isSuccess())
                eventHandler.onRestResponse(response);
            else
                eventHandler.onResponseFailed(response.getDisplayMessage());
        }
    }
}
