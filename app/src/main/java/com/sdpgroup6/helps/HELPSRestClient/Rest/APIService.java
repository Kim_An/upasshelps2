package com.sdpgroup6.helps.HELPSRestClient.Rest;

import android.support.annotation.Nullable;

import com.sdpgroup6.helps.Model.BookWorkshopResponse;
import com.sdpgroup6.helps.Model.CampusListResponse;
import com.sdpgroup6.helps.Model.CheckStudentResponse;
import com.sdpgroup6.helps.Model.GetStudentResponse;
import com.sdpgroup6.helps.Model.ProgramResponse;
import com.sdpgroup6.helps.Model.RegisterRequest;
import com.sdpgroup6.helps.Model.BookedWorkshopResponse;
import com.sdpgroup6.helps.Model.SessionBookingResponse;
import com.sdpgroup6.helps.Model.WaitingCountResponse;
import com.sdpgroup6.helps.Model.WorkshopResponse;
import com.sdpgroup6.helps.Model.WorkshopSetResponse;
import com.sdpgroup6.helps.Model.RequestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by kiman on 23/08/2016.
 */
//This class defines all API Services That is available to use XD

public interface APIService {

    /**
     * get workshopSet, Takes WorkshopSetResponse Model
     *
     * @return WorkshopSetResponse
     */
    @GET("api/workshop/workshopsets")
    Call<WorkshopSetResponse> getWorkshopSets(@Nullable @Query("active") Boolean active);

    /**
     * register student, send registerRequest object
     *
     * @param registerRequest is an object that it takes to send the request
     * @return a basic RequestResponse object
     */
    @POST("api/student/register")
    Call<RequestResponse> registerStudent(@Body RegisterRequest registerRequest);

    /**
     * check if a student exist in the database
     *
     * @param studentId is used to check
     * @return CheckStudentResponse object
     */
    @GET("api/student/check")
    Call<CheckStudentResponse> isStudentExists(@Query("studentID") String studentId);

    @GET("api/student/get")
    Call<GetStudentResponse> getStudent(@Query("studentID") String studentId);


    /**
     * get a list of available workshops that is associated with the same workshop id
     *
     * @param workshopSetId is used to identify the set of workshop to request for
     * @return WorkshopResponse object
     */
    @GET("api/workshop/search")
    Call<WorkshopResponse> getWorkshops(@Query("workshopSetId") String workshopSetId,
                                        @Nullable @Query("Topic") String topic,
                                        @Nullable @Query("StartingDtBegin") String startDateBegin,
                                        @Nullable @Query("StartingDtEnd") String startDateEnd,
                                        @Nullable @Query("EndingDtBegin") String endDateBegin,
                                        @Nullable @Query("EndingDtEnd") String endDateEnd,
                                        @Nullable @Query("Campus") String compus,
                                        @Nullable @Query("active") Boolean active,
                                        @Nullable @Query("Page") Integer page,
                                        @Nullable @Query("PageSize") Integer pageSize);

    @GET("api/program/search")
    Call<ProgramResponse> getPrograms(@Nullable @Query("name") String programName,
                                      @Nullable @Query("StartingDtBegin") String startDateBegin,
                                      @Nullable @Query("StartingDtEnd") String startDateEnd,
                                      @Nullable @Query("EndingDtBegin") String endDateBegin,
                                      @Nullable @Query("EndingDtEnd") String endDateEnd,
                                      @Nullable @Query("active") Boolean active,
                                      @Nullable @Query("Page") Integer page,
                                      @Nullable @Query("PageSize") Integer pageSize);
    @GET("api/program/programWorkshops")
    Call<WorkshopResponse> getProgramWorkshops( @Query("programId") String programID);

    /**
     * make a request to book a workshop
     *
     * @param workshopId is used to identify the workshop to book
     * @param studentId  is used to identify the student to workshop is booked for
     * @param userId     is used to identify the person who made the booking
     * @return RequestResponse object
     */
    @POST("api/workshop/booking/create")
    Call<BookWorkshopResponse> bookWorkshop(@Query("workshopId") String workshopId,
                                            @Query("studentId") String studentId,
                                            @Query("userId") String userId);

    @POST("api/workshop/wait/create")
    Call<RequestResponse> waitWorkshop(@Query("workshopId") String workshopId,
                                       @Query("studentId") String studentId,
                                       @Query("userId") String userId,
                                       @Nullable @Query("priority") Boolean priority);

    @GET("api/workshop/wait/count")
    Call<WaitingCountResponse> countWorkshopWaiting(@Query("workshopID") String workshopId);

    @POST("api/workshop/booking/cancel")
    Call<RequestResponse> cancelWorkshop(@Query("workshopId") String workshopId,
                                       @Query("studentId") String studentId,
                                       @Query("userId") String userId);
    @GET("api/misc/campus/short")
    Call<CampusListResponse> campusList();

    /**
     * get a list of booked workshop of a student
     * @param studentId is used to identify the student to make request for
     * @return BookedWorkshopResponse object
     */
    @GET("api/workshop/booking/search")
    Call<BookedWorkshopResponse> getBookedWorkshops(@Query("studentID") String studentId,
                                                    @Nullable @Query("StartingDtBegin") String startDateBegin,
                                                    @Nullable @Query("StartingDtEnd") String startDateEnd,
                                                    @Nullable @Query("EndingDtBegin") String endDateBegin,
                                                    @Nullable @Query("EndingDtEnd") String endDateEnd,
                                                    @Nullable @Query("CampusId") Integer compusId,
                                                    @Nullable @Query("active") Boolean active,
                                                    @Nullable @Query("Page") Integer page,
                                                    @Nullable @Query("PageSize") Integer pageSize);

    @GET("api/session/booking/search")
    Call<SessionBookingResponse> getBookedSessions(@Query("studentID") String studentId,
                                                   @Nullable @Query("StartingDtBegin") String startDateBegin,
                                                   @Nullable @Query("StartingDtEnd") String startDateEnd,
                                                   @Nullable @Query("EndingDtBegin") String endDateBegin,
                                                   @Nullable @Query("EndingDtEnd") String endDateEnd,
                                                   @Nullable @Query("Campus") String compus,
                                                   @Nullable @Query("LecturerID") Integer lecturerID,
                                                   @Nullable @Query("SessionTypeId") Integer sessionTypeId,
                                                   @Nullable @Query("active") Boolean active,
                                                   @Nullable @Query("Page") Integer page,
                                                   @Nullable @Query("PageSize") Integer pageSize);


}




