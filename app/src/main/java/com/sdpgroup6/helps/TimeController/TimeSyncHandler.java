package com.sdpgroup6.helps.TimeController;

import java.util.Calendar;

/**
 * Created by Johnny on 1/10/2016.
 */

public interface TimeSyncHandler {
    void onGetTimeResponse(Boolean Success,Calendar newTime);
}
