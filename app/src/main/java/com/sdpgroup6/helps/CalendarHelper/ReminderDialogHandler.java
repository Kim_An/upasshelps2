package com.sdpgroup6.helps.CalendarHelper;

/**
 * Is responsible for calling back once user has selected their reminder choices
 */

public interface ReminderDialogHandler {
    void onReminderChosen(boolean[] selectedReminder, int arrayResId);
}
