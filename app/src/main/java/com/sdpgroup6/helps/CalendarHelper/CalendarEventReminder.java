package com.sdpgroup6.helps.CalendarHelper;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Reminders;
import android.provider.CalendarContract.Events;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.sdpgroup6.helps.Model.Constants;
import com.sdpgroup6.helps.Model.EmailCalendar;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Is responsible for make mEvent and reminders in the user's calendar app
 */
public class CalendarEventReminder {

    private ContentResolver mContentResolver = null;
    private Context mContext = null;
    private Activity mActivity = null;
    private ContentValues mEvent;
    private long mEventID;
    private String mWorkshopBookingId;

    public CalendarEventReminder(ContentResolver mContentResolver, Activity mActivity, Context context) {
        this.mContentResolver = mContentResolver;
        this.mActivity = mActivity;
        this.mContext = context;
    }

    public CalendarEventReminder(Activity applicationActivity, Context activityContext, ContentResolver mContentResolver, String mWorkshopBookingId) {
        this.mContentResolver = mContentResolver;
        this.mContext = activityContext;
        this.mActivity = applicationActivity;
        this.mWorkshopBookingId = mWorkshopBookingId;
    }

    /**
     * makes an mEvent containing all of its attributes and store in mEvent, which is a ContentValue object
     * @param calId
     * @param title
     * @param description
     * @param location
     * @param beginTime
     * @param endTime
     */
    public void makeEvent(long calId, String title, String description, String location, Calendar beginTime, Calendar endTime) {
        mEvent = new ContentValues();
        mEvent.put(Constants.CALENDAR_ID, calId);
        mEvent.put(Constants.TITLE, title);
        mEvent.put(Constants.DESCRIPTION, description);
        mEvent.put(Constants.EVENT_LOCATION, location);
        mEvent.put(Constants.EVENT_TIMEZONE, Constants.TIMEZONE);
        mEvent.put(Constants.DATETIME_START, beginTime.getTimeInMillis());
        mEvent.put(Constants.DATETIME_TEND, endTime.getTimeInMillis());
    }

    /**
     * makes an event and reminders using the mEvent for the event, after checking for permission to write to calendar
     * @param selectedReimders
     * @param arrayResId
     */
    public void insertEventAndReminders(boolean[] selectedReimders, int arrayResId) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Constants.WRITE_CALENDAR_PERMISSION}, Constants.WRITE_CALENDAR_PERMISSION_REQUEST);

        } else {
            try {
                Uri eventUri = mContentResolver.insert(Events.CONTENT_URI, mEvent);

                assert eventUri != null;
                //getting the event id after inserting the event into a calendar
                mEventID = Long.parseLong(eventUri.getLastPathSegment());
                Log.i(Constants.LOG_TAG, "mEventID = " + mEventID);
                long[] reminderTimes = getReminderTimes(selectedReimders, arrayResId);
                for (long time : reminderTimes) {
                    mContentResolver.insert(Reminders.CONTENT_URI, createReminder(time, mEventID));
                }
                //for the booking id and its event id in an sharedPreference
                EventStorage.getInstance(mContext).store(mWorkshopBookingId, mEventID);
                Utilities.displayToast(mContext, mContext.getString(R.string.reminder_added_success_msg));
            } catch (IllegalArgumentException e) {
                Log.e(Constants.LOG_TAG, e.getMessage());
            }
        }
    }

    /**
     * deletes an event from the calendar and show a toast
     * removes the event id and its booking id from the sharedPreference
     * @param eventId
     * @param workshopBookingId
     */
    public void deleteEventWithToast(long eventId, String workshopBookingId) {
        Uri deleteUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventId);
        int rows = mContentResolver.delete(deleteUri, null, null);

        Log.i(Constants.LOG_TAG, "Event row deleted: " + rows);
        Utilities.displayToast(mContext, mContext.getString(R.string.reminder_remove_success_msg));
        EventStorage.getInstance(mContext).remove(workshopBookingId);
    }

    /**
     * deletes an event from the calendar without showing a toast
     * removes the event id and its booking id from the sharedPreference
     * @param eventId
     * @param workshopBookingId
     */
    public void deleteEventWithoutToast(long eventId, String workshopBookingId) {
        Uri deleteUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventId);
        int rows = mContentResolver.delete(deleteUri, null, null);

        Log.i(Constants.LOG_TAG, "Event row deleted: " + rows);
        EventStorage.getInstance(mContext).remove(workshopBookingId);
    }

    /**
     * initializes a reminder and return a ContentValues object for making reminder
     * @param minute
     * @param eventId
     * @return
     */
    private ContentValues createReminder(long minute, long eventId) {
        ContentValues reminder = new ContentValues();
        reminder.put(Reminders.MINUTES, minute);
        reminder.put(Reminders.EVENT_ID, eventId);
        reminder.put(Reminders.METHOD, Reminders.METHOD_EMAIL);
//        reminder.put(Reminders.METHOD, Reminders.METHOD_ALERT);
        return reminder;
    }

    /**
     * converts hour to minutes and return the value
     * @param hour
     * @return
     */
    private long minOfHour(int hour) {
        return hour * 60;
    }

    /**
     * converts week to minutes and return to value
     * @param week
     * @return
     */
    private long minOfWeek(int week) {
        return week * 10080;
    }

    /**
     * retrieves calendar id that are store in the phone
     * @return
     */
    public ArrayList<EmailCalendar> getGoogleCalendarId() {
        ArrayList<EmailCalendar> calendars = new ArrayList<>();
        //the columns we want from the calendar database
        String[] resultColumns =
                new String[]{
                        CalendarContract.Calendars._ID,
                        CalendarContract.Calendars.NAME,
                        CalendarContract.Calendars.ACCOUNT_NAME,
                        CalendarContract.Calendars.ACCOUNT_TYPE};

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Constants.READ_CALENDAR_PERMISSION}, Constants.READ_CALENDAR_PERMISSION_REQUEST);

        } else {
            //the query we make to get data from the calendar database
            Cursor queryCursor = mContentResolver.query(CalendarContract.Calendars.CONTENT_URI,
                    resultColumns,
                    CalendarContract.Calendars.VISIBLE + " = 1",
                    null,
                    CalendarContract.Calendars._ID + " ASC");
            assert queryCursor != null;
            if (queryCursor.moveToFirst()) {
                long tempCalId;
                do {
                    String tempCalName = queryCursor.getString(1);
                    tempCalId = queryCursor.getLong(0);
                    Log.i(Constants.LOG_TAG, "cal id = " + tempCalId);
                    Log.i(Constants.LOG_TAG, "cal name = " + tempCalName);
                    if (tempCalName != null) {
                        if (isEmail(tempCalName)) {
                            calendars.add(new EmailCalendar(tempCalId, tempCalName));
                        }
                    }
                } while (queryCursor.moveToNext());
            }
            queryCursor.close();
        }
        return calendars;
    }

    /**
     * checks if a string is an email using the @ symbol
     * @param calendar
     * @return
     */
    private static boolean isEmail(String calendar) {
        return calendar.contains("@");
    }

    /**
     * makes the reminder time based on the inputs that user picks
     * returns them in an array
     * @param selectedReminders
     * @param arrayResId
     * @return
     */
    private long[] getReminderTimes(boolean[] selectedReminders, int arrayResId){
        ArrayList<Long> reminderList = new ArrayList<>();
        if(selectedReminders[0]){
            reminderList.add(getFirstReminderFromArray(arrayResId));
        }
        if(selectedReminders[1]){
            reminderList.add(minOfHour(24));
        }
        if(selectedReminders[2]){
            reminderList.add(minOfWeek(1));
        }
        long[] reminderTimes = new long[reminderList.size()];
        for (int i = 0; i < reminderList.size(); i++) {
            reminderTimes[i] = reminderList.get(i);
        }
        return reminderTimes;
    }

    /**
     * checks if the input that user picks was 10 minutes or others and return the value accordingly
     * @param arrayResId
     * @return
     */
    private long getFirstReminderFromArray(int arrayResId){
        String[] reminderArray = mContext.getResources().getStringArray(arrayResId);

        if(reminderArray[0].contains(Constants.TEN_MIN)){
            return 10;
        }
        return 30;
    }
}