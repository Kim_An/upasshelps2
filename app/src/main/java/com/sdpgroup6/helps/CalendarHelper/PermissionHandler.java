package com.sdpgroup6.helps.CalendarHelper;

import com.sdpgroup6.helps.Model.RequestResponse;
import com.sdpgroup6.helps.Model.Workshop;

/**
 * Is responsible for calling back to activity class once a booking is made successfully
 * so that the activity can then start asking runtime permission for reading calendar
 */

public interface PermissionHandler {
    void onBookingSuccess(RequestResponse requestResponse, Workshop workshop);
}
