package com.sdpgroup6.helps.CalendarHelper;

/**
 * Is for calling back on calendar dialog action made by the user
 * it is used usually in adapter to call back to activity
 */
public interface CalendarDialogHandler {
    void onCalendarDialogResponse(boolean positive, long calId);
}
