package com.sdpgroup6.helps.CalendarHelper;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.sdpgroup6.helps.Model.EmailCalendar;
import com.sdpgroup6.helps.R;
import com.sdpgroup6.helps.Utilities;

import java.util.ArrayList;

/**
 * Is responsible for reminder chooser dialog
 */
public class ReminderChooser {

    private AlertDialog mAlertDialog;

    public ReminderChooser(Activity activity, final Context context, ContentResolver contentResolver, final ReminderDialogHandler handler) {
        int size = context.getResources().getStringArray(R.array.reminder_array_1).length;
        String positiveText = context.getString(android.R.string.ok);
        String negativeText = context.getString(android.R.string.cancel);
        final boolean[] selectedItem = new boolean[size];

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DatePickerTheme);
        builder.setTitle(R.string.reminder_chooser_title);

        //allow users to choose mutliple reminders/options
        builder.setMultiChoiceItems(R.array.reminder_array_1, selectedItem, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                selectedItem[which] = isChecked;
            }
        });

        //positive button handling
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.onReminderChosen(selectedItem, R.array.reminder_array_1);
            }
        });

        //negative button handling
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        mAlertDialog = builder.create();
    }

    public void show(){
        mAlertDialog.show();
    }

}
