package com.sdpgroup6.helps.CalendarHelper;

import com.sdpgroup6.helps.Model.BookedWorkshop;

/**
 * Is used in upcomingBookingAdapter to call back to the activity to invoke the reminder dialog
 */

public interface ReminderHandler {
    void onRemindClicked(BookedWorkshop bookedWorkshop);
}
