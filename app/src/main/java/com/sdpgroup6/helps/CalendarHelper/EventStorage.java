package com.sdpgroup6.helps.CalendarHelper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.sdpgroup6.helps.Model.Constants;


/**
 * Is responsible for storing booking id that is matched with a event id in the calendar
 * It uses shared preference, and uses Singleton pattern
 */
public class EventStorage {
    private static EventStorage theEventStorage = null;
    private static SharedPreferences sharedpreferences;
    private static SharedPreferences.Editor editor;

    public static EventStorage getInstance(Context context) {
        if(theEventStorage != null) {
            return theEventStorage;
        }else{
            theEventStorage= new EventStorage(context);
            return theEventStorage;
        }
    }

    private EventStorage(Context context) {
        sharedpreferences = context.getSharedPreferences(Constants.EVENT_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void store(String bookingId, long eventId){
        editor = sharedpreferences.edit();
        editor.putLong(bookingId, eventId);
        editor.commit();
        Log.i(Constants.LOG_TAG, "Stored in the EventStorage: " + bookingId + " - " + eventId);
    }

    public long getEventId(String bookingId){
        long defaultValue = -1;

        return sharedpreferences.getLong(bookingId, defaultValue);
    }

    public void remove(String bookingId){
        editor = sharedpreferences.edit();
        editor.remove(bookingId);
        editor.commit();
    }
}
