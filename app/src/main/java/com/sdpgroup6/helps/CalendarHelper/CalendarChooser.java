package com.sdpgroup6.helps.CalendarHelper;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.sdpgroup6.helps.Model.EmailCalendar;
import com.sdpgroup6.helps.R;

import java.util.ArrayList;

/**
 * Is responsible for creating a calendar dialog
 */
public class CalendarChooser {

    private AlertDialog mAlertDialog;

    public CalendarChooser(Activity activity, Context context, ContentResolver contentResolver, final CalendarDialogHandler handler) {
        final long[] calId = new long[1];
        CalendarEventReminder event = new CalendarEventReminder(contentResolver, activity, context);
        final ArrayList<EmailCalendar> calendars = event.getGoogleCalendarId();
        final String[] items = new String[calendars.size()];

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DatePickerTheme);
        builder.setTitle(R.string.calendar_chooser_title);

        for (int i = 0; i < calendars.size(); i++) {
            items[i] = calendars.get(i).getName();
        }

        builder.setSingleChoiceItems(items, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String calNameClicked = items[which];
                        for (EmailCalendar calendar : calendars) {
                            if (calendar.isCalendarName(calNameClicked)) {
                                calId[0] = calendar.getId();
                            }
                        }
                    }
                });

        String positiveText = context.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        HomeActivity.this.onCalendarDialogResponse(true, calId);
                        if (which == -1) {
                            for (EmailCalendar calendar : calendars) {
                                String calNameClicked = items[0];
                                if (calendar.isCalendarName(calNameClicked)) {
                                    calId[0] = calendar.getId();
                                }
                            }
                            handler.onCalendarDialogResponse(true, calId[0]);
                        }else{
                            handler.onCalendarDialogResponse(true, calId[0]);
                        }
                    }
                });

        String negativeText = context.getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        mAlertDialog = builder.create();
    }

    public void show(){
        mAlertDialog.show();
    }
}
