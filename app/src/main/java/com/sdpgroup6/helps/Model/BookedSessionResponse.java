package com.sdpgroup6.helps.Model;

/**
 * Created by Johnny on 16/09/2016.
 * Is the response model for BookedSession
 */
public class BookedSessionResponse extends RequestResponse{

    public BookedSessionResponse(Boolean isSuccess, String displayMessage) {
        super(isSuccess, displayMessage);
    }
}
