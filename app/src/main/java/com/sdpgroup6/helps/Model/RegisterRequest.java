package com.sdpgroup6.helps.Model;

/**
 * This class is the model of the json format of the register request for a student
 */
public class RegisterRequest {
    private String StudentId;
    private String DateOfBirth;
    private String Gender;
    private String Degree;
    private String Status;
    private String FirstLanguage;
    private String CountryOrigin;
    private String Background;
    private String DegreeDetails;
    private String AltContact;
    private String PreferredName;
    private String HSC;
    private String HSCMark;
    private String IELTS;
    private String IELTSMark;
    private String TOEFL;
    private String TOEFLMark;
    private String TAFE;
    private String TAFEMark;
    private String CULT;
    private String CULTMark;
    private String InsearchDEEP;
    private String InsearchDEEPMark;
    private String InsearchDiploma;
    private String InsearchDiplomaMark;
    private String FoundationCourse;
    private String FoundationCourseMark;
    private String CreatorId;

    public RegisterRequest(String studentId, String degree, String status, String firstLanguage, String countryOrigin, String creatorId) {
        this.StudentId = studentId;
        this.Degree = degree;
        this.Status = status;
        this.FirstLanguage = firstLanguage;
        this.CountryOrigin = countryOrigin;
        this.CreatorId = creatorId;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getFirstLanguage() {
        return FirstLanguage;
    }

    public void setFirstLanguage(String firstLanguage) {
        FirstLanguage = firstLanguage;
    }

    public String getCountryOrigin() {
        return CountryOrigin;
    }

    public void setCountryOrigin(String countryOrigin) {
        CountryOrigin = countryOrigin;
    }

    public String getBackground() {
        return Background;
    }

    public void setBackground(String background) {
        Background = background;
    }

    public String getDegreeDetails() {
        return DegreeDetails;
    }

    public void setDegreeDetails(String degreeDetails) {
        DegreeDetails = degreeDetails;
    }

    public String getAltContact() {
        return AltContact;
    }

    public void setAltContact(String altContact) {
        AltContact = altContact;
    }

    public String getPreferredName() {
        return PreferredName;
    }

    public void setPreferredName(String preferredName) {
        PreferredName = preferredName;
    }

    public String getHSC() {
        return HSC;
    }

    public void setHSC(String HSC) {
        this.HSC = HSC;
    }

    public String getHSCMark() {
        return HSCMark;
    }

    public void setHSCMark(String HSCMark) {
        this.HSCMark = HSCMark;
    }

    public String getIELTS() {
        return IELTS;
    }

    public void setIELTS(String IELTS) {
        this.IELTS = IELTS;
    }

    public String getIELTSMark() {
        return IELTSMark;
    }

    public void setIELTSMark(String IELTSMark) {
        this.IELTSMark = IELTSMark;
    }

    public String getTOEFL() {
        return TOEFL;
    }

    public void setTOEFL(String TOEFL) {
        this.TOEFL = TOEFL;
    }

    public String getTOEFLMark() {
        return TOEFLMark;
    }

    public void setTOEFLMark(String TOEFLMark) {
        this.TOEFLMark = TOEFLMark;
    }

    public String getTAFE() {
        return TAFE;
    }

    public void setTAFE(String TAFE) {
        this.TAFE = TAFE;
    }

    public String getTAFEMark() {
        return TAFEMark;
    }

    public void setTAFEMark(String TAFEMark) {
        this.TAFEMark = TAFEMark;
    }

    public String getCULT() {
        return CULT;
    }

    public void setCULT(String CULT) {
        this.CULT = CULT;
    }

    public String getCULTMark() {
        return CULTMark;
    }

    public void setCULTMark(String CULTMark) {
        this.CULTMark = CULTMark;
    }

    public String getInsearchDEEP() {
        return InsearchDEEP;
    }

    public void setInsearchDEEP(String insearchDEEP) {
        InsearchDEEP = insearchDEEP;
    }

    public String getInsearchDEEPMark() {
        return InsearchDEEPMark;
    }

    public void setInsearchDEEPMark(String insearchDEEPMark) {
        InsearchDEEPMark = insearchDEEPMark;
    }

    public String getInsearchDiploma() {
        return InsearchDiploma;
    }

    public void setInsearchDiploma(String insearchDiploma) {
        InsearchDiploma = insearchDiploma;
    }

    public String getInsearchDiplomaMark() {
        return InsearchDiplomaMark;
    }

    public void setInsearchDiplomaMark(String insearchDiplomaMark) {
        InsearchDiplomaMark = insearchDiplomaMark;
    }

    public String getFoundationCourse() {
        return FoundationCourse;
    }

    public void setFoundationCourse(String foundationCourse) {
        FoundationCourse = foundationCourse;
    }

    public String getFoundationCourseMark() {
        return FoundationCourseMark;
    }

    public void setFoundationCourseMark(String foundationCourseMark) {
        FoundationCourseMark = foundationCourseMark;
    }

    public String getCreatorId() {
        return CreatorId;
    }

    public void setCreatorId(String creatorId) {
        CreatorId = creatorId;
    }
}
