package com.sdpgroup6.helps.Model;

/**
 * This class is the model of a basic response of the API
 */
public class RequestResponse {
    private Boolean IsSuccess;
    private String DisplayMessage;

    public RequestResponse(Boolean isSuccess, String displayMessage) {
        this.IsSuccess = isSuccess;
        this.DisplayMessage = displayMessage;
    }

    public Boolean isSuccess() {
        return IsSuccess;
    }

    public String getDisplayMessage() {
        return DisplayMessage;
    }

    public void setDisplayMessage(String displayMessage) {
        this.DisplayMessage = displayMessage;
    }
}
