package com.sdpgroup6.helps.Model;

/**
 * This class is the model of the response of the isStudentExist of the API
 */

public class CheckStudentResponse extends RequestResponse{
    private Boolean IsExists;;

    public CheckStudentResponse(Boolean IsSuccess, String DisplayMessage,Boolean IsExists) {
        super(IsSuccess,DisplayMessage);
        this.IsExists = IsExists;
    }
    public Boolean isExists() {
        return IsExists;
    }
}
