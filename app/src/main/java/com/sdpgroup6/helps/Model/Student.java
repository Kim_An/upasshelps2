package com.sdpgroup6.helps.Model;

import android.support.annotation.Nullable;

import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Johnny on 27/09/2016.
 * Is the model for student that reflects the model on the server
 */
public class Student {

    private String studentID;
    private
    @Nullable
    String dob;
    private String gender;
    private String degree;
    private String status;
    private String first_language;
    private String country_origin;
    private String background;
    private
    @Nullable
    Boolean HSC;
    private String HSC_mark;
    private
    @Nullable
    Boolean IELTS;
    private String IELTS_mark;
    private
    @Nullable
    Boolean TOEFL;
    private String TOEFL_mark;
    private
    @Nullable
    Boolean TAFE;
    private String TAFE_mark;
    private
    @Nullable
    Boolean CULT;
    private String CULT_mark;
    private
    @Nullable
    Boolean InsearchDEEP;
    private String InsearchDEEP_mark;
    private
    @Nullable
    Boolean InsearchDiploma;
    private String InsearchDiploma_mark;
    private
    @Nullable
    Boolean foundationcourse;
    private String foundationcourse_mark;
    private
    @Nullable
    String created;
    private
    @Nullable
    Integer creatorID;
    private String degree_details;
    private String alternative_contact;
    private String preferred_name;

    public String getStudentID() {
        return studentID.trim();
    }

    @Nullable
    public java.util.Calendar getDob() throws ParseException {
        if (dob == null)
            throw new ParseException("No DOB Data", 0);
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(dob));
        return cal;
    }

    public String getGender() {
        return gender == null ? "" : gender.trim();
    }

    public String getDegree() {
        return degree.trim();
    }

    public String getStatus() {
        return status.trim();
    }

    public String getFirst_language() {
        return first_language.trim();
    }

    public String getCountry_origin() {
        return country_origin.trim();
    }

    public String getBackground() {
        return background.trim();
    }

    @Nullable
    public Boolean getHSC() {
        return HSC == null ? false : HSC;
    }

    public String getHSC_mark() {
        return HSC_mark.trim();
    }

    @Nullable
    public Boolean getIELTS() {
        return IELTS == null ? false : IELTS;
    }

    public String getIELTS_mark() {
        return IELTS_mark.trim();
    }

    @Nullable
    public Boolean getTOEFL() {
        return TOEFL == null ? false : TOEFL;
    }

    public String getTOEFL_mark() {
        return TOEFL_mark.trim();
    }

    @Nullable
    public Boolean getTAFE() {
        return TAFE == null ? false : TAFE;
    }

    public String getTAFE_mark() {
        return TAFE_mark.trim();
    }

    @Nullable
    public Boolean getCULT() {
        return CULT == null ? false : CULT;
    }

    public String getCULT_mark() {
        return CULT_mark.trim();
    }

    @Nullable
    public Boolean getInsearchDEEP() {
        return InsearchDEEP == null ? false : InsearchDEEP;
    }

    public String getInsearchDEEP_mark() {
        return InsearchDEEP_mark.trim();
    }

    @Nullable
    public Boolean getInsearchDiploma() {
        return InsearchDiploma == null ? false : InsearchDiploma;
    }

    public String getInsearchDiploma_mark() {
        return InsearchDiploma_mark.trim();
    }

    @Nullable
    public Boolean getFoundationcourse() {
        return foundationcourse == null ? false : foundationcourse;
    }

    public String getFoundationcourse_mark() {
        return foundationcourse_mark.trim();
    }

    @Nullable
    public Calendar getCreated() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(created));
        return cal;
    }

    @Nullable
    public Integer getCreatorID() {
        return creatorID == null ? -1 : creatorID;
    }

    public String getDegree_details() {
        return degree_details;
    }

    public String getAlternative_contact() {
        return alternative_contact.trim();
    }

    public String getPreferred_name() {
        return preferred_name.trim();
    }
}
