package com.sdpgroup6.helps.Model;

import android.support.annotation.Nullable;

import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;

/**
 * This class is the model of the single workshop within a workshop set
 */
public class Program {

    private Integer id;
    private String name;
    private String days;
    @Nullable private Integer numOfWeeks;
    @Nullable  private String startDate;
    @Nullable private String endDate;
    @Nullable private Integer maximum;
    @Nullable private Integer cutoff;
    @Nullable private String created;
    @Nullable private Integer creatorId;
    @Nullable private String modified;
    @Nullable private Integer modifierId;
    @Nullable private String archived;
    @Nullable private Integer  archiverId;
    @Nullable private Integer  reminder_num;
    @Nullable private Integer  reminder_sent;

    public Program(Integer id, String name, String days, Integer numOfWeeks, String startDate, String endDate, Integer maximum, Integer cutoff, String created, Integer creatorId, String modified, Integer modifierId, String archived, Integer archiverId, Integer reminder_num, Integer reminder_sent) {
        this.id = id;
        this.name = name;
        this.days = days;
        this.numOfWeeks = numOfWeeks;
        this.startDate = startDate;
        this.endDate = endDate;
        this.maximum = maximum;
        this.cutoff = cutoff;
        this.created = created;
        this.creatorId = creatorId;
        this.modified = modified;
        this.modifierId = modifierId;
        this.archived = archived;
        this.archiverId = archiverId;
        this.reminder_num = reminder_num;
        this.reminder_sent = reminder_sent;
    }

    public Calendar getStartDate() throws ParseException {
       // Log.i(Utilities.RESPONSETAG,"DateTime from SQL server(Workshop): "+StartDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(startDate));
        return cal;
    }

    public Calendar getEndDate() throws ParseException {
        //Log.i(Utilities.RESPONSETAG,"DateTime from SQL server(Workshop): "+EndDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(endDate));
        return cal;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDays() {
        return days;
    }

    @Nullable
    public Integer getNumOfWeeks() {
        return numOfWeeks;
    }

    @Nullable
    public Integer getMaximum() {
        return maximum;
    }

    @Nullable
    public Integer getCutoff() {
        return cutoff;
    }
}