package com.sdpgroup6.helps.Model;

/**
 * Created by 46693_000 on 7/10/2016.
 */
public class Campus {
    private int id ;
    private String campus;
    private String archived;

    public Campus(int id, String campus, String archived) {
        this.id = id;
        this.campus = campus;
        this.archived = archived;
    }

    public int getId() {
        return id;
    }

    public String getCampus() {
        return campus;
    }
}
