package com.sdpgroup6.helps.Model;

/**
 * Is a model used only for single workshop adapter, for temporarily storing clicked status and waiting count
 */
public class WorkshopDuplicate{

    private Workshop mWorkshop;
    private boolean mClicked;
    private String mWaitingCount;

    public WorkshopDuplicate(Workshop workshop) {
        mWorkshop = workshop;
        mClicked = false;
        mWaitingCount = "";
    }

    public String getWaitingCount() {
        return mWaitingCount.equals("null") ? "0" : mWaitingCount;
    }

    public void setWaitingCount(String mWaitingCount) {
        this.mWaitingCount = mWaitingCount;
    }

    public Workshop getmWorkshop() {
        return mWorkshop;
    }

    public boolean isClicked() {
        return mClicked;
    }

    public void setClicked(boolean mClicked) {
        this.mClicked = mClicked;
    }

    public boolean isFull(){
        return mWorkshop.isFull();
    }

    public boolean hasWaitingCount(){
        return !mWaitingCount.equals("");
    }
}