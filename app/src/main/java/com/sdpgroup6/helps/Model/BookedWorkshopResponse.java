package com.sdpgroup6.helps.Model;

import android.util.Log;

import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This class is the model of the response of the getBookedWorkshops of the API
 * the name "Results" of the BookedWorkshop list must not be modified to anything else
 * or the parsing from json to java object would not work
 */
public class BookedWorkshopResponse extends RequestResponse {

    private List<BookedWorkshop> Results = new ArrayList<>();

    public BookedWorkshopResponse(List<BookedWorkshop> results, Boolean isSuccess, String displayMessage) {
        super(isSuccess, displayMessage);
        Results = results;
    }
    //modified by JH, at 19/09, you can now return past workshops/future workshops based on time set at Utilities class

    public List<BookedWorkshop> getPastWorkshops() {
        List<BookedWorkshop> pastWorkshops = new ArrayList<>();
        Calendar currentTime = Utilities.getAppTime();
        for(BookedWorkshop thisWorkshop : Results)
        {
            try {
                if(currentTime.getTimeInMillis()> thisWorkshop.getStarting().getTimeInMillis())
                {
                    //add to past results
                    pastWorkshops.add(thisWorkshop);
                }
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(Utilities.ERRORTAG,"Problem with Parsing the DateTime from RESTAPI!!! Contact your admin for help");
                continue;
            }
        }


        return pastWorkshops;
    }

    public List<BookedWorkshop> getFutureWorkshops() {
        List<BookedWorkshop> futureWorkshops = new ArrayList<>();
        Calendar currentTime = Utilities.getAppTime();
        for(BookedWorkshop thisWorkshop : Results)
        {
            try {
                if(currentTime.getTimeInMillis() <= thisWorkshop.getStarting().getTimeInMillis())
                {
                    //add to future results
                    futureWorkshops.add(thisWorkshop);
                }
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(Utilities.ERRORTAG,"Problem with Parsing the DateTime from RESTAPI!!! Contact your admin for help");
                continue;
            }
        }
        return futureWorkshops;
    }

    public List<BookedWorkshop> getAllWorkshops() {
        return Results;
    }

}