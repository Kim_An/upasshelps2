package com.sdpgroup6.helps.Model;

/**
 * This class is the model of a basic response of the API
 */
public class WaitingCountResponse extends RequestResponse{
    private Integer Result;

    public WaitingCountResponse(Boolean isSuccess, String displayMessage, Integer result) {
        super(isSuccess, displayMessage);
        Result = result;
    }

    public Integer getResult() {
        return Result;
    }
}
