package com.sdpgroup6.helps.Model;

import android.support.annotation.Nullable;

/**
 * Created by 46693_000 on 5/10/2016.
 * Is the model for workshop booking is used in BookWorkshopResponse
 */
public class WorkshopBooking {
    private Integer id;
    private Integer workshopID;
    private String studentID;
    @Nullable
    private Boolean canceled;
    @Nullable
    private Boolean attended;
    @Nullable
    private String archived;

    public Integer getBookingId() {
        return id;
    }

    public Integer getWorkshopID() {
        return workshopID;
    }
}
