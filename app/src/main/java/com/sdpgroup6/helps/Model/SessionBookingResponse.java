package com.sdpgroup6.helps.Model;

import java.util.List;

/**
 * Created by 46693_000 on 7/10/2016.
 * Is the response model for getBookedSession from the api
 */
public class SessionBookingResponse extends RequestResponse{

    private List<SessionBooking> Results;

    public SessionBookingResponse(Boolean isSuccess, String displayMessage, List<SessionBooking> results) {
        super(isSuccess, displayMessage);
        Results = results;
    }

    public List<SessionBooking> getResults() {
        return Results;
    }
}
