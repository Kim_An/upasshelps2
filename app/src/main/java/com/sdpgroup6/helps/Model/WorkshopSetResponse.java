package com.sdpgroup6.helps.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the model of the response of the getWorkshopSets of the API
 * the name "Results" of the WorkshopSet list must not be modified to anything else
 * or the parsing from json to java object would not work
 */
public class WorkshopSetResponse extends RequestResponse {

    //The variable name Results must not be changed to anything else,
    //otherwise it will not work when converting to/from JSON
    private List<WorkshopSet> Results = new ArrayList<WorkshopSet>();

    public WorkshopSetResponse(List<WorkshopSet> workshopSets, Boolean isSuccess, String displayMessage) {
        super(isSuccess, displayMessage);
        this.Results = workshopSets;
    }

    /**
     * @return The WorkshopSetResponse
     */
    public List<WorkshopSet> getWorkshops() {
        return Results;
    }
}