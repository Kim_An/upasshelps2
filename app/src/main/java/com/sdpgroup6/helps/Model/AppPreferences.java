package com.sdpgroup6.helps.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by kiman on 20/09/2016.
 * Takes care of store student id, remove student id, returning student id from the shared preference
 */
public class AppPreferences {
    private static AppPreferences theAppPreferences = null;
    private static SharedPreferences sharedpreferences;
    private static SharedPreferences.Editor editor;

    public static AppPreferences getInstance(Context context) {
        if(theAppPreferences != null) {
            return theAppPreferences;
        }else{
            theAppPreferences= new AppPreferences(context);
            return theAppPreferences;
        }
    }

    private AppPreferences(Context context) {
        sharedpreferences = context.getSharedPreferences(Constants.STUDENT_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setupPreferences(String studentId) {
        editor = sharedpreferences.edit();
        editor.putString(Constants.STUDENT_ID, studentId);
        editor.commit();
        LoginSession.studentId = studentId;
    }

    public String getStudentId(){
        String defaultValue = Constants.NULL;
        return sharedpreferences.getString(Constants.STUDENT_ID, defaultValue);
    }

    public void removeStudentId(){
        editor = sharedpreferences.edit();
        editor.remove(Constants.STUDENT_ID);
        editor.commit();
        LoginSession.studentId = Constants.NULL;
    }

    public void loadStudentId(){
        if(LoginSession.studentId.equals(Constants.NULL) && !getStudentId().equals(Constants.NULL)){
            LoginSession.studentId = getStudentId();
        }
    }
}
