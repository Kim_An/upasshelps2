package com.sdpgroup6.helps.Model;

import android.util.Log;

import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * This class is the model of the single workshop within a workshop set
 */
public class Workshop {

    private Integer WorkshopId;
    private String topic;
    private String description;
    private String targetingGroup;
    private String campus;
    private String StartDate;
    private String EndDate;
    private Integer maximum;
    private Integer WorkShopSetID;
    private String cutoff;
    private String type;
    private Integer reminderNum;
    private Integer reminderSent;
    private String DaysOfWeek;
    private Integer BookingCount;
    private String archived;

    public Workshop(Integer workshopId, String topic, String description, String targetingGroup, String campus, String startDate, String endDate, Integer maximum, Integer workShopSetID, String cutoff, String type, Integer reminderNum, Integer reminderSent, String daysOfWeek, Integer bookingCount, String archived) {
        WorkshopId = workshopId;
        this.topic = topic;
        this.description = description;
        this.targetingGroup = targetingGroup;
        this.campus = campus;
        StartDate = startDate;
        EndDate = endDate;
        this.maximum = maximum;
        WorkShopSetID = workShopSetID;
        this.cutoff = cutoff;
        this.type = type;
        this.reminderNum = reminderNum;
        this.reminderSent = reminderSent;
        DaysOfWeek = daysOfWeek;
        BookingCount = bookingCount;
        this.archived = archived;
    }

    public Workshop(Integer workshopId, String topic, String description, String startDate, String endDate, String campus) {
        this.topic = topic;
        this.description = description;
        StartDate = startDate;
        EndDate = endDate;
        this.campus = campus;
        WorkshopId = workshopId;
    }

    public Integer getWorkshopId() {
        return WorkshopId;
    }

    public String getTopic() {
        return topic;
    }

    public String getDescription() {
        return description;
    }

    public String getTargetingGroup() {
        return targetingGroup;
    }

    public String getCampus() {
        return campus;
    }

    public Calendar getStartDate() throws ParseException {
        // Log.i(Utilities.RESPONSETAG,"DateTime from SQL server(Workshop): "+StartDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(StartDate));
        return cal;
    }

    public Calendar getEndDate() throws ParseException {
        //Log.i(Utilities.RESPONSETAG,"DateTime from SQL server(Workshop): "+EndDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(EndDate));
        return cal;
    }

    public Integer getMaximum() {
        return maximum;
    }

    public Integer getWorkShopSetID() {
        return WorkShopSetID;
    }

    public String getCutoff() {
        return cutoff;
    }

    public String getType() {
        return type;
    }

    public Integer getReminderNum() {
        return reminderNum;
    }

    public Integer getReminderSent() {
        return reminderSent;
    }

    public String getDaysOfWeek() {
        return DaysOfWeek;
    }

    public Integer getBookingCount() {
        return BookingCount;
    }

    public String getArchived() {
        return archived;
    }

    public boolean isWorkshopId(Integer workshopId) {
        return WorkshopId.equals(workshopId);
    }

    public boolean isFull(){
        int cutOffs = Integer.parseInt(cutoff);
        return cutOffs <= BookingCount;
    }
}