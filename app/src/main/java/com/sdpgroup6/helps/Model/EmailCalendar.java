package com.sdpgroup6.helps.Model;

/**
 * Created by kiman on 1/10/2016.
 * Is a data structure for calendar of user
 */
public class EmailCalendar {
    private long id;
    private String name;

    public EmailCalendar(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isCalendarName(String calName){
        return calName.equals(name);
    }
}
