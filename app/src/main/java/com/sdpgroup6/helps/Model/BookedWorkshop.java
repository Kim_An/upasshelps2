package com.sdpgroup6.helps.Model;

import android.util.Log;

import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * This class is the model of booked workshop
 * There is a collection of this objects in BookedWorkshopResponse
 */
public class BookedWorkshop {

    private Integer BookingId;
    private Integer workshopID;
    private String studentID;
    private String topic;
    private String description;
    private String targetingGroup;
    private String campus;
    private String starting;
    private String ending;
    private Integer maximum;
    private String cutoff;
    private String canceled;
    private String attended;
    private Integer WorkShopSetID;
    private String type;
    private Integer reminder_num;
    private Integer reminder_sent;
    private String WorkshopArchived;
    private String BookingArchived;

    public BookedWorkshop(Integer bookingId, Integer workshopID, String studentID, String topic, String description, String targetingGroup, String campus, String starting, String ending, Integer maximum, String cutoff, String canceled, String attended, Integer workShopSetID, String type, Integer reminder_num, Integer reminder_sent, String workshopArchived, String bookingArchived) {
        BookingId = bookingId;
        this.workshopID = workshopID;
        this.studentID = studentID;
        this.topic = topic;
        this.description = description;
        this.targetingGroup = targetingGroup;
        this.campus = campus;
        this.starting = starting;
        this.ending = ending;
        this.maximum = maximum;
        this.cutoff = cutoff;
        this.canceled = canceled;
        this.attended = attended;
        WorkShopSetID = workShopSetID;
        this.type = type;
        this.reminder_num = reminder_num;
        this.reminder_sent = reminder_sent;
        WorkshopArchived = workshopArchived;
        BookingArchived = bookingArchived;
    }

    public Integer getBookingId() {
        return BookingId;
    }


    public Integer getWorkshopID() {
        return workshopID;
    }


    public String getStudentID() {
        return studentID;
    }


    public String getTopic() {
        return topic;
    }


    public String getDescription() {
        return description;
    }


    public String getTargetingGroup() {
        return targetingGroup;
    }


    public String getCampus() {
        return campus;
    }


    public Calendar getStarting() throws ParseException {
        //Log.i(Utilities.RESPONSETAG,"DateTime from SQL server: "+starting);
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(starting));
        return cal;
    }


    public Calendar getEnding() throws ParseException {
        //Log.i(Utilities.RESPONSETAG,"DateTime from SQL server: "+ending);
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(ending));
        return cal;
    }

    public String getDuration(){
        long duration = 0;
        try {
            duration = Utilities.getDateDiff(getStarting().getTime(), getEnding().getTime(), TimeUnit.MINUTES);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return String.valueOf(duration);
    }


    public Integer getMaximum() {
        return maximum;
    }


    public String getCutoff() {
        return cutoff;
    }

    public void setCutoff(String cutoff) {
        this.cutoff = cutoff;
    }

    public String getCanceled() {
        return canceled;
    }

    public void setCanceled(String canceled) {
        this.canceled = canceled;
    }

    public String getAttended() {
        if(attended != null) {
            switch (attended) {
                case "0":
                    return "Absent";
                case "1":
                    return "Attended";
                default:
                    return "Not marked";
            }
        }else {
            return "Not marked";
        }
    }

    public void setAttended(String attended) {
        this.attended = attended;
    }

    public Integer getWorkShopSetID() {
        return WorkShopSetID;
    }

    public void setWorkShopSetID(Integer workShopSetID) {
        WorkShopSetID = workShopSetID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWorkshopType() {
        if (type.equals("single")) {
            return "Workshop";
        } else if (type.equals("multiple")) {
            return "Program";
        } else {
            return "";
        }
    }

    public Integer getReminder_num() {
        return reminder_num;
    }

    public void setReminder_num(Integer reminder_num) {
        this.reminder_num = reminder_num;
    }

    public Integer getReminder_sent() {
        return reminder_sent;
    }

    public void setReminder_sent(Integer reminder_sent) {
        this.reminder_sent = reminder_sent;
    }

    public String getWorkshopArchived() {
        return WorkshopArchived;
    }

    public void setWorkshopArchived(String workshopArchived) {
        WorkshopArchived = workshopArchived;
    }

    public String getBookingArchived() {
        return BookingArchived;
    }

    public void setBookingArchived(String bookingArchived) {
        BookingArchived = bookingArchived;
    }

    public Workshop getWorkshop(){
        return new Workshop(workshopID, topic, description, starting, ending, campus);
    }
}