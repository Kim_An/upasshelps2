package com.sdpgroup6.helps.Model;

import java.util.List;

/**
 * Created by 46693_000 on 7/10/2016.
 */
public class CampusListResponse extends RequestResponse{
    private List<Location> Results;

    public CampusListResponse(Boolean isSuccess, String displayMessage, List<Location> results) {
        super(isSuccess, displayMessage);
        Results = results;
    }

    public List<Location> getResults() {
        return Results;
    }
}
