package com.sdpgroup6.helps.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the model of the response of the getWorkshop of the API
 * the name "Results" of the Workshop list must not be modified to anything else
 * or the parsing from json to java object would not work
 */
public class ProgramResponse extends RequestResponse {

    public List<Program> Results = new ArrayList<>();

    public ProgramResponse(List<Program> results, Boolean isSuccess, String displayMessage) {
        super(isSuccess, displayMessage);
        Results = results;
    }

    public List<Program> getProgram() {
        return Results;
    }
}