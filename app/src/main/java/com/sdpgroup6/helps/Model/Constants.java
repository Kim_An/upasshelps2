package com.sdpgroup6.helps.Model;

/**
 * This class stores the value of constant variables
 */
public class Constants {
    public static final String BASE_URL_ADDRESS = "http://elssa-booking.azurewebsites.net/";
    public static final String APP_KEY = "Appkey";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APP_KEY_VALUE = "H0ldth3Ap12016#";
    public static final String CONTENT_TYPE_VALUE = "Application/json";
    public static final String STUDENT_ID = "studentID";
    public static final String PASSWORD = "PASSWORD";
    public static final int VALID_STUDENT_ID_DIGIT = 8;

    public static final String LOG_TAG = "sdp6";

    public static final String ARG_ITEM_ID = "item_id";

    public static final String NULL = "null";
    public static final String EMPTY_SPINNER = "--";

    public static final String WORKSHOPSET_ID = "workshopset_id";
    public static final String WORKSHOPSET_NAME = "workshopset_name";
    public static final String PROGRAM_ID = "program_id";
    public static final String PROGRAM_NAME = "program_name";

    public static final String UPCOMING_WORKSHOP = "upcoming_workshop";
    public static final String PAST_WORKSHOP = "past_workshop";
    public static final String WORKSHOP_ID = "workshop_id";
    public static final String WORKSHOP_NAME = "workshop_name";
    public static final String WORKSHOP_DESCRIPTION = "description";
    public static final String WORKSHOP_START_DATE = "workshop_start_date";
    public static final String WORKSHOP_START_TIME = "workshop_start_time";
    public static final String WORKSHOP_END_DATE = "workshop_start_date";
    public static final String WORKSHOP_END_TIME = "workshop_start_time";

    public static final String WORKSHOP_LOCATION = "workshop_location";

    public static final String MAXIMUM_WORKSHOP_BOOKINGS_ERROR = "Error creating workshop booking: Workshop has reached cut-off for bookings.";
    public static final String CREATE_WORKSHOP_BOOKING_EXISTS_ERROR = "Error creating workshop booking: Booking already exists.";

    public static final String CREATE_WORKSHOP_WAITING_BOOKING_EXISTS_ERROR = "Error creating workshop wait-list entry: Student has booking for this workshop.";
    public static final String CREATE_WORKSHOP_WAITING_EXISTS_ERROR = "Error creating workshop wait-list entry: Student is already on the wait-list.";
    public static final String CREATE_WORKSHOP_WAITING_ERROR = "Error creating workshop wait-list entry: {0}";

    public static final String CANCEL_WORKSHOP_BOOKING_ERROR = "Error canceling workshop booking: {0}";
    public static final String UPDATE_SESSION_BOOKING_ERROR = "Error updating session booking: {0}";
    public static final String UPDATE_WORKSHOP_BOOKING_ERROR = "Error updating workshop booking: {0}";

    public static final String GENERIC_ERROR = "Error, Please try again later.";

    public static final String DOB = "date_of_birth";
    public static final String GENDER = "gender";
    public static final String DEGREE = "degree ";
    public static final String STATUS = "status ";
    public static final String FIRST_LANGUAGE = "first language ";
    public static final String COUNTRY_ORIGIN = "country origin ";
    public static final String BACKGROUND = "background";
    public static final String DEGREE_DETAILS = "degree_details";
    public static final String ALT_CONTACT = "alt_contact";
    public static final String PREFFERD_NAME = "preferred_name";
    public static final String HSC = "hsc";
    public static final String HSC_MARK = "hsc_mark";
    public static final String IELTS = "ielts";
    public static final String IELTS_MARK = "ielts_mark";
    public static final String TOEFL = "toefl";
    public static final String TOEFL_MARK = "toefl_mark";
    public static final String TAFE = "tafe";
    public static final String TAFE_MARK = "tafe_mark";
    public static final String CULT = "cult";
    public static final String CULT_MARK = "cult_mark";
    public static final String INSEARCH_DEEP = "insearch_deep";
    public static final String INSEARCH_DEEP_MARK = "insearch_deep_mark";
    public static final String INSEARCH_DIP = "insearch_diploma";
    public static final String INSEARCH_DIP_MARK = "insearch_diploma_mark";
    public static final String FOUNDATION_COURSE = "foundation_course";
    public static final String FOUNDATION_MARK = "foundation_course_mark";

    public static final String WRITE_CALENDAR_PERMISSION = "android.permission.WRITE_CALENDAR";
    public static final int WRITE_CALENDAR_PERMISSION_REQUEST = 19;
    public static final String READ_CALENDAR_PERMISSION = "android.permission.READ_CALENDAR";
    public static final int READ_CALENDAR_PERMISSION_REQUEST = 29;

    public static final String MAIL_TO = "mailto";
    public static final String HELPS_EMAIL = "helps@uts.edu.au";

    public static final String CALENDAR_ID = "calendar_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String EVENT_LOCATION = "eventLocation";
    public static final String EVENT_TIMEZONE = "eventTimezone";
    public static final String TIMEZONE = "Australia/Sydney";
    public static final String DATETIME_START = "dtstart";
    public static final String DATETIME_TEND = "dtend";
    public static final String TEN_MIN = "10 minutes";

    public static final String EVENT_PREFERENCES = "com.sdpgroup6.helps.preferences.event";
    public static final String STUDENT_PREFERENCES = "com.sdpgroup6.helps.preferences";

    public static final String IT_HELPS = "IT HELPS";
    public static final String THE_ONLY_HELP_IS_YOURSELF = "THE ONLY HELP IS YOURSELF";
    public static final String THANKS = "Thanks";

    public static final String NO_REMINDER_RELATED_TO_THIS_BOOKING = "No reminder related to this booking";
    public static final String SOME_FIELDS_MUST_BE_COMPLETED = "Some fields must be completed";
    public static final String CHOOSE_ONE = "Choose one";

    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final String INDETERMINATE_UNSPECIFIED_INTERSEX = "Indeterminate / Unspecified / Intersex";
    public static final String M = "M";
    public static final String F = "F";
    public static final String X = "X";

    public static final String DATE = "Date: ";
    public static final String TIME = "Time: ";
    public static final String LOCATION = "Location: ";
    public static final String YOU_HAVE_BEEN_WAITLISTED = "You have been waitlisted";
    public static final String WORKSHOP = "WORKSHOP";
    public static final String PROGRAM = "PROGRAM";
}
