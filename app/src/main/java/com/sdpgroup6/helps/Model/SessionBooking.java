package com.sdpgroup6.helps.Model;

import android.support.annotation.Nullable;

import com.sdpgroup6.helps.Utilities;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by 46693_000 on 7/10/2016.
 * Is the model for response of session booking
 */
public class SessionBooking {
    private String LecturerFirstName;
    private String LecturerLastName;
    private String LecturerEmail;
    private String SessionTypeAbb;
    private String SessionType;
    private String AssignmentType;
    private String AppointmentType;
    private int BookingId;
    private String StartDate;
    private String EndDate;
    private String Campus;
    private Boolean Cancel;
    private String Assistance;
    private String Reason;
    private @Nullable Integer Attended;
    private @Nullable Integer WaitingID;
    private @Nullable Integer IsGroup;
    private String NumPeople;
    private String LecturerComment;
    private String LearningIssues;
    private @Nullable Integer IsLocked;
    private String AssignTypeOther;
    private String Subject;
    private String AppointmentsOther;
    private String AssistanceText;
    private int SessionId;
    private @Nullable String archived;

    public String getLecturerFirstName() {
        return LecturerFirstName;
    }

    public String getLecturerLastName() {
        return LecturerLastName;
    }

    public String getLecturerEmail() {
        return LecturerEmail;
    }

    public String getSessionTypeAbb() {
        return SessionTypeAbb;
    }

    public String getSessionType() {
        return SessionType;
    }

    public String getAssignmentType() {
        return AssignmentType;
    }

    public String getAppointmentType() {
        return AppointmentType;
    }

    public int getBookingId() {
        return BookingId;
    }

    public Calendar getStartDate() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(StartDate));
        return cal;
    }

    public Calendar getEndDate() throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(Utilities.getDatefromString(EndDate));
        return cal;
    }

    public String getCampus() {
        return Campus;
    }

    public Boolean getCancel() {
        return Cancel;
    }

    public String getAssistance() {
        return Assistance;
    }

    public String getReason() {
        return Reason;
    }

    @Nullable
    public String getAttended() {
        if(Attended != null) {
            switch (Attended) {
                case 0:
                    return "Absent";
                case 1:
                    return "Attended";
                default:
                    return "Not marked";
            }
        }else{
            return "Not marked";
        }
    }

    @Nullable
    public Integer getWaitingID() {
        return WaitingID;
    }

    @Nullable
    public Integer getIsGroup() {
        return IsGroup;
    }

    public String getNumPeople() {
        return NumPeople;
    }

    public String getLecturerComment() {
        return LecturerComment;
    }

    public String getLearningIssues() {
        return LearningIssues;
    }

    @Nullable
    public Integer getIsLocked() {
        return IsLocked;
    }

    public String getAssignTypeOther() {
        return AssignTypeOther;
    }

    public String getSubject() {
        return Subject;
    }

    public String getAppointmentsOther() {
        return AppointmentsOther;
    }

    public String getAssistanceText() {
        return AssistanceText;
    }

    public int getSessionId() {
        return SessionId;
    }

    @Nullable
    public String getArchived() {
        return archived;
    }
}
