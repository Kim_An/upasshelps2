package com.sdpgroup6.helps.Model;

/**
 * This is the model of the workshopset item
 */
public class WorkshopSet {

    private int id;
    private String name;
    private String archived;

    public WorkshopSet(Integer id, String name, String archived) {
        this.id = id;
        this.name = name;
        this.archived = archived;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getArchived() {
        return archived;
    }

}