package com.sdpgroup6.helps.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is the model for the workshop list and detail activity
 */
public class WorkshopSets {

    public static final Map<String, WorkshopSet> ITEM_MAP = new HashMap<>();

    public static List<WorkshopSet> WORKSHOPS = new ArrayList<>();

    public WorkshopSets(List<WorkshopSet> workshopSets) {
        WORKSHOPS = workshopSets;
        for (WorkshopSet workshopSet : workshopSets) {
            addItem(workshopSet);
        }
        String s = ITEM_MAP.toString();
        System.out.print(s);
    }

    /**
     * This function adds item to the map
     *
     * @param workshopSet
     */
    private static void addItem(WorkshopSet workshopSet) {
        ITEM_MAP.put(String.valueOf(workshopSet.getId()), workshopSet);
    }

}
