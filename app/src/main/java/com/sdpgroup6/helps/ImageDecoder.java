package com.sdpgroup6.helps;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;

/**
 * This class is responsible for generating images, generating color state, color
 */
public class ImageDecoder {
    /**
     * compress the background the image to 1/5th of the original size, so that
     * the app can supports devices with low memory
     * @param res
     * @param resId
     * @return
     */
    public static Bitmap decodeBitmapFromResource(Resources res, int resId) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(res, resId, options);

        //reduce the size to 1/5 of the original
        options.inSampleSize = 5;

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);

    }

    /**
     * return a color state, so that it can be used in conjunction with the getColor to set the
     * color for CollapsingToolbarLayout
     * @return
     */
    public static int[][] getColorState() {
        return new int[][]{
                new int[]{android.R.attr.state_enabled} // pressed
        };
    }


    /**
     * return a color, so that it can be used in conjunction with the getColorState to set the
     * color for CollapsingToolbarLayout
     * @return
     */
    public static int[] getColor(Context appContext) {
        return new int[]{
                ContextCompat.getColor(appContext, R.color.colorTitle)
        };
    }

    /**
     * this is another way to customize up the color of the up button on the toolbars
     * @param appContext
     * @param actionBar
     */
    public static void customizeUpButton(Context appContext, ActionBar actionBar) {
        final Drawable upArrow = ContextCompat.getDrawable(appContext, R.drawable.ic_arrow_up_24dp);
        upArrow.setColorFilter(ContextCompat.getColor(appContext, R.color.colorTitle), PorterDuff.Mode.SRC_ATOP);
        actionBar.setHomeAsUpIndicator(upArrow);
    }
}
